<?php
/*
*/

class Proyecto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		  //$this->db_rrhh = $this->load->database('db_rrhh', TRUE); 
		  //$this->db_siape = $this->load->database('db_siape', TRUE);    
		  //$this->tabla = 'persona.funcionario';
	}
///inicio reportes graficos
    public function listaDeptos()
    {        
        $query = $this->db->query("select distinct departamento from datos_proyecto.proyecto_datos order by departamento asc");	
        return $query->result();        
    } 
    /*//lista carteras por departamentos
	public function countCartera($depto)
    {        
        $query = $this->db->query("select distinct cartera, count(*) 
        	                         from datos_proyecto.proyecto_datos 
        	                         where departamento ilike('%$depto%')
                                     group by cartera
                                     order by cartera asc");	
        return $query->result();        
    }
    */
    public function countCarteraFunc($depto) //por usuario
    {        
        $idus = $this->session->userdata('per');
        if ($depto=='') $consulta = "where v.id_us=$idus and 1=1";
        else
        $consulta = "where v.id_us=$idus and departamento ilike(''%$depto%'')";

        $query = $this->db->query("select * from crosstab(
'select distinct p.departamento, p.cartera, count(p.cartera) as catidad 
from datos_proyecto.proyecto_datos p
join datos_proyecto.view_asignados v on p.id=v.id_proy
$consulta--''ORURO'' -- and tipo_proy = ''RIEGO''
group by departamento, cartera order by departamento, cartera asc', 'SELECT distinct cartera from datos_proyecto.proyecto_datos order by cartera asc')
as ct(depto text, cartera1 int, cartera2 int)");
//var_dump($query);	
        return $query->result();        
    }
    public function countTipoProyectosusu($depto)
    {        
        $idus = $this->session->userdata('per');
        if ($depto=='') $consulta = "and 1=1";
        else
        $consulta = "and departamento ilike('%$depto%')";
        $query = $this->db->query("select distinct p.tipo_proy, count(*) as cantidad,
                                            round(count(*)*100/(select sum(a.cantidad) from 
                                                       (select count(p1.tipo_proy) as cantidad  
                                                        from 
                            datos_proyecto.proyecto_datos  p1
                            join datos_proyecto.view_asignados v1 on p1.id=v1.id_proy
                            where v1.id_us=$idus $consulta 
                            ) 
                                               as a),2)as porcentaje--con dos decimales
from datos_proyecto.proyecto_datos p
join datos_proyecto.view_asignados v on p.id=v.id_proy
where v.id_us=$idus $consulta
group by tipo_proy
order by tipo_proy asc");    
        return $query->result();       
    }

///fin reportes graficos    
	public function getproyectoficha($idproy) //2022
    {        
        $query = $this->db->query("select a.*, b.*, c.*, 
vct.costo_total,
pe.estado, pe.fecha_inicio, pe.plazo_ejec_convenio,pe.acta_provisional_fecha,pe.acta_definitiva_fecha,pe.porcentaje_ejec_fisica,
vat.ampliaciones_totales,
vfpe.fecha_plazo_ejec_final,
vpef.porcentaje_ejec_financiera,
vda.desembolso_acumulado_fdi
from 
datos_proyecto.proyecto_datos a
join datos_financiera.desembolsos b on a.id=b.id_proy
join datos_financiera.estructuta_financiamiento c on a.id=c.id
join datos_financiera.view_costo_total vct on a.id=vct.id_proy
join datos_proyecto.plazos_ejecucion pe on a.id=pe.id_proy 
join datos_proyecto.view_ampliaciones_totales vat on a.id=vat.id_proy
join datos_proyecto.view_fecha_plazo_ejec_final vfpe on a.id=vfpe.id_proy
join datos_proyecto.view_porcentaje_ejec_financiera vpef on a.id=vpef.id_proy
join datos_financiera.view_desembolso_acumulado_fdi vda on a.id=vda.id_proy
where a.id='".$idproy."'");	
        return $query->row();        
    }
	public function getproyectos() /*ticosas*/
    {        
        $query = $this->db->query("select * from datos_proyecto.proyecto_datos p
									join datos_proyecto.view_asignados v on p.id=v.id_proy
									where id_us='".$this->session->userdata('per')."'");	
        return $query->result();        
    }
    function listaDepartamento(){
    	$query = $this->db->query("select * from geografia.departamento where pais_iso = 'BO'");	
        return $query->result(); 	
    }
    function listaProvicnia($idD){
    	$query = $this->db->query("select * from geografia.provincia where iddepartamento='".$idD."' order by descripcion");	
        return $query->result(); 	
    }
    function listaMunicipio($idP){
    	$query = $this->db->query("select * from geografia.municipio where idprovincia='".$idP."' order by descripcion");	
        return $query->result(); 	
    }













    function listaIndicador(){
    	$query = $this->db->query("select * from datos_proyecto.indicador order by id_indicador asc");	
        return $query->result(); 	
    }
    function listaUmedida(){
    	$query = $this->db->query("select * from datos_proyecto.unidad_medida order by id_unidad asc");	
        return $query->result(); 	
    }
    function listaCartera(){
    	$query = $this->db->query("select * from datos_proyecto.cartera order by id_cartera asc");	
        return $query->result(); 	
    }
    function listaTipoProy(){
    	$query = $this->db->query("select * from datos_proyecto.tipo_proyecto order by id_tipop asc");	
        return $query->result(); 	
    }
    function listaTipoInv(){
    	$query = $this->db->query("select * from datos_proyecto.tipo_inversion order by id_inversion asc");	
        return $query->result(); 	
    }

     function listaDepartamento2($abuscar){
    	$query = $this->db->query("select descripcion from geografia.departamento where pais_iso = 'BO' and descripcion ILIKE '%" . $abuscar . "%'LIMIT 5");	
        return $query->result(); 	
    }

    function verifTipo($tipo){
		$query= $this->db->query("select * from datos_convenio.entidad_ejecutora where descripcion ilike('%".$tipo."%') "); 
	   return $query->num_rows(); 
	}

	function guardarEntidadE($enti,$sigl){
		$data = array(
			'descripcion'=>$enti,
			'sigla'=>$sigl
			);
		$this->db->insert('datos_convenio.entidad_ejecutora',$data);
		return $this->db->insert_id();
	}

	function listaEjecutora(){
    	$query = $this->db->query("select * from datos_convenio.entidad_ejecutora order by id_ejecutora asc");	
        return $query->result(); 	
    }

    function verifConvenio($conv)
	{
		$query = $this->db->query("select * from datos_convenio.convenio where nro_convenio ilike('%".$conv."%')");	
		return $query->result();  	
	}
	public function getproyectosborrador()
    {        
        $query = $this->db->query("select * from datos_proyecto.borrador order by id asc");	
        return $query->result();        
    }
//INICIO DESEMBOLSOS
public function getproyectosDesembolsos() //1 ticosas
    {        
        $query = $this->db->query("select p.id, p.cartera,p.departamento, p.municipio,  p.nombre_proy, p.num_convenio, e.*, d.*, vct.*, vda.*, vsf.* 
from datos_proyecto.proyecto_datos p
join datos_financiera.estructuta_financiamiento e on p.id=e.id
join datos_financiera.desembolsos d on p.id=d.id_proy
join datos_financiera.view_costo_total vct on p.id=vct.id_proy
join datos_financiera.view_desembolso_acumulado_fdi vda on p.id=vda.id_proy
join datos_financiera.view_saldo_fdi vsf on p.id=vsf.id_proy
join datos_proyecto.view_asignados va on p.id=va.id_proy
where va.id_us='".$this->session->userdata('per')."' 
order by p.id asc");	
        return $query->result();        
    } 
public function EditProyecto(){//tico
    $rownumber=$this->input->post('row');
    $val=$this->input->post('valuei');//valor del campo
    if($val == '') $val = null;
    $table = $this->input->post('tabl');
    $split_data = explode(':', $rownumber);
    $rownumber_id = $split_data[1];//ID
    $rownumber_name = $split_data[0];//NOMBRE CAMPO
     $data = array(
			$rownumber_name => $val,
			'campo' => $rownumber_name,
			'id_usuario' => $this->session->userdata('per'),
			'ip' => $this->funciones->getip(),
			'datos'=> $val,
			'datos_anterior' => $this->funciones->valorAnterior($rownumber_id, $table, $rownumber_name)
		 );

		$this->db->where('id_proy', $rownumber_id);
		$this->db->update($table,$data);
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
    }   
  public function getHistproyectos($tabla) //tico
    {        
        $query = $this->db->query("
 select * from seguridad.bitacora b
join datos_proyecto.proyecto_datos p on b.id_proy=p.id
where b.id_usuario = '".$this->session->userdata('per')."' and tabla = '".$tabla."'
order by b.id desc limit 10");
       //var_dump($this->db->last_query());
        return $query->result();        
    }  

    public function getAlertas() //tico
    {        
        $query = $this->db->query("
 select p.id, p.cartera, p.departamento, p.municipio, p.nombre_proy, va.* from datos_proyecto.view_alertas_plazos va
join datos_proyecto.proyecto_datos p on va.id_proy = p.id
order by alerta
");
       //var_dump($this->db->last_query());
        return $query->result();        
    }       
//FIN DESEMBOLSOS    	
//INICIO PLAZOS
/*    
public function editarPlazos(){//tico
    $rownumber=$this->input->post('row');
    $val=$this->input->post('valuei');
    $split_data = explode(':', $rownumber);
    $rownumber_id = $split_data[1];//ID
    $rownumber_name = $split_data[0];//NOMBRE CAMPO
     $data = array(
			$rownumber_name => $val,
			'campo' => $rownumber_name,
			'id_usuario' => $this->session->userdata('per'),
			'ip' => $this->funciones->getip(),
			'datos'=> $val,
			'datos_anterior' => $this->funciones->valorAnterior($rownumber_id, 'datos_proyecto.plazos_ejecucion', $rownumber_name)
		 );

		$this->db->where('id_proy', $rownumber_id);
		$this->db->update('datos_proyecto.plazos_ejecucion',$data);
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
    }
*/
public function getproyectosPlazos() //ticosas
    {        
        $query = $this->db->query("select e.*, p.id, p.cartera,p.departamento, p.municipio,  p.nombre_proy, p.num_convenio, vp.porcentaje_ejec_financiera, vap.alerta 
                    from datos_proyecto.plazos_ejecucion e
                                    join datos_proyecto.proyecto_datos p on e.id_proy=p.id
                                    join datos_proyecto.view_porcentaje_ejec_financiera vp on p.id=vp.id_proy 
                                    join datos_proyecto.view_alertas_plazos vap on p.id=vap.id_proy 
                                    join datos_proyecto.view_asignados va on p.id=va.id_proy
                                    --where e.vigencia_plazo_ejecucion != 'NO VIGENTE' or e.vigencia_plazo_ejecucion is null
                                    where va.id_us='".$this->session->userdata('per')."' and (e.vigencia_plazo_ejecucion != 'NO VIGENTE' or e.vigencia_plazo_ejecucion is null)
                                    order by id_proy asc");	
        return $query->result();        
    }
 ///FIN PLAZOS DE EJECUCION   
///////////////////////INICIO SITUACION
public function getproyectosSituaciones()/*ticosas*/
    {        
        $query = $this->db->query("select s.*, p.id, p.cartera,p.departamento, p.municipio,  p.nombre_proy, p.num_convenio
from datos_proyecto.situacion_general_inspeccion s
join datos_proyecto.proyecto_datos p on s.id_proy=p.id 
join datos_proyecto.view_asignados va on p.id=va.id_proy
where va.id_us='".$this->session->userdata('per')."'
order by id_proy asc");	
        return $query->result();        
    }
 public function getUsuariosTec()
    {        
        $query = $this->db->query("select id_usuario, nombre||' '||apellido_pat||' '||apellido_mat as usuario from seguridad.usuarios where tipo_usu=1");	
        return $query->result();        
    }   
/*    
public function editarSituacion(){//tico
    $rownumber=$this->input->post('row');
    $val=$this->input->post('valuei');
    $split_data = explode(':', $rownumber);
    $rownumber_id = $split_data[1];//ID
    $rownumber_name = $split_data[0];//NOMBRE CAMPO
     $data = array(
			$rownumber_name => $val,
			'campo' => $rownumber_name,
			'id_usuario' => $this->session->userdata('per'),
			'ip' => $this->funciones->getip(),
			'datos'=> $val,
			'datos_anterior' => $this->funciones->valorAnterior($rownumber_id, 'datos_proyecto.situaciones_inspeccion', $rownumber_name)
		 );

		$this->db->where('id_proy', $rownumber_id);
		$this->db->update('datos_proyecto.situaciones_inspeccion',$data);
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
    }
 */     
///////////////////// FIN SITUACION   

////////////////////////////////////// INICIO ACUERDO
public function getFechaLimite($id)
    {        
        $query = $this->db->query("select * from datos_proyecto.view_fecha_limite_segun_ac_conc where id_proy='".$id."'");	
        return $query->row()->fecha_limite_segun_ac_conc;        
    }   
public function getAlertaPlazosEjec($id)
    {        
        $query = $this->db->query("select * from datos_proyecto.view_alertas_plazos where id_proy='".$id."'");  
        return $query->row()->alerta;        
    }        
function getEstadosVenc(){
    	$query = $this->db->query("select * from datos_proyecto.estado_venc order by id_estadov asc");	
        return $query->result(); 	
    }    
function getVigenciaPl(){
    	$query = $this->db->query("select * from datos_proyecto.vigencia_plazo order by id_vigenciap asc");	
        return $query->result(); 	
    }        
public function getproyectosAcuerdos() /*ticosas*/
    {        
        $query = $this->db->query("select a.*, p.id, p.cartera,p.departamento, p.municipio,  p.nombre_proy, p.num_convenio, vfl.*, vap.alerta, pe.vigencia_plazo_ejecucion   
from datos_proyecto.acuerdo_conciliatorio a
join datos_proyecto.proyecto_datos p on a.id_proy=p.id 
join datos_proyecto.view_fecha_limite_segun_ac_conc vfl on a.id_proy=vfl.id_proy 
join datos_proyecto.view_alertas_plazos vap on p.id=vap.id_proy
join datos_proyecto.plazos_ejecucion pe on p.id=pe.id_proy
join datos_proyecto.view_asignados va on p.id=va.id_proy
where pe.vigencia_plazo_ejecucion = 'NO VIGENTE' and va.id_us='".$this->session->userdata('per')."' 
order by a.id_proy asc");	
        return $query->result();        
    }
/*    
public function editarVencimiento(){//tico
    $rownumber=$this->input->post('row');
    $val=$this->input->post('valuei');
    $split_data = explode(':', $rownumber);
    $rownumber_id = $split_data[1];//ID
    $rownumber_name = $split_data[0];//NOMBRE CAMPO
     $data = array(
			$rownumber_name => $val,
			'campo' => $rownumber_name,
			'id_usuario' => $this->session->userdata('per'),
			'ip' => $this->funciones->getip(),
			'datos'=> $val,
			'datos_anterior' => $this->funciones->valorAnterior($rownumber_id, 'datos_proyecto.vencimiento_plazos', $rownumber_name)
		 );

		$this->db->where('id_proy', $rownumber_id);
		$this->db->update('datos_proyecto.vencimiento_plazos',$data);
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
    }

*/
/////////////////////////////////////// FIN ACUERDO


    public function editarProy(){
    $rownumber=$this->input->post('row');
    $val=$this->input->post('valuei');
    $split_data = explode(':', $rownumber);
    $rownumber_id = $split_data[1];
    $rownumber_name = $split_data[0];

        $data = array(
			$rownumber_name => $val
		 );

		$this->db->where('id', $rownumber_id);
		$this->db->update('datos_proyecto.borrador',$data);
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
    }






/*

	function getusuario()
	{
		$query = $this->db_rrhh->query("select * from persona.tipodocumento");	
        return $query->result();   	
	}
	
	public function getfuncionarios()
    {        
        $query = $this->db_siape->query("select fun.id, fun.apellido_pat, fun.apellido_mat, fun.nombres, fun.ci, tra.cargo_comision, it.puesto, unid.nombre as unidad, dir.nombre as direccion 
										from siape.t_funcionario fun, siape.t_trabajo tra, siape.t_item it, siape.t_direccion dir, siape.t_unidad unid 
										where fun.ci = tra.ci and tra.nitem = it.nitem and dir.idd = it.idd and unid.idu=it.idu and fun.estado = true and it.estd = true and tra.est = true
										order by fun.id");	
        return $query->result();        
    }
	
    public function getfuncionarios()
    {        
        $query = $this->db_rrhh->query("select f.id_funcionario, f.nombres, f.paterno, f.materno, f.nro_documento, ex.departamento as expedido, f.domicilio 
										from persona.funcionario f 
										join persona.estadocivil ec on ec.id_estadocivil = f.id_estadocivil
										join persona.expedido ex on ex.id_expedido = f.id_expedido
										join persona.tipodocumento td on td.id_tipodocumento =  f.id_tipodocumento
										order by f.id_funcionario asc");	
        return $query->result();        
    }
    public function selec_datosfun($idfunc)
    {        
        $query = $this->db_rrhh->query("select *
										from persona.funcionario f 
										join persona.estadocivil ec on ec.id_estadocivil = f.id_estadocivil
										join persona.expedido ex on ex.id_expedido = f.id_expedido
										join persona.tipodocumento td on td.id_tipodocumento =  f.id_tipodocumento
										where f.id_funcionario = '".$idfunc."'");	
        return $query->result();        
    }
    function listaTipoDoc()
	{
		$query = $this->db_rrhh->query("select * from persona.tipodocumento order by id_tipodocumento asc ");	
		return $query->result();  	
	}
	function listaExped()
	{
		$query = $this->db_rrhh->query("select * from persona.expedido order by id_expedido asc ");	
		return $query->result();  	
	}
	function listaEstCivil()
	{
		$query = $this->db_rrhh->query("select * from persona.estadocivil order by id_estadocivil asc ");	
		return $query->result();  	
	}
	function listaAfp()
	{
		$query = $this->db_rrhh->query("select * from persona.afp order by id_afp asc ");	
		return $query->result();  	
	}
	function verifnumdocum($num)
	{
		$query = $this->db_rrhh->query("select * from persona.funcionario where nro_documento = '".$num."'");	
		return $query->result();  	
	}
	function verifnumbiom($biom)
	{
		$query = $this->db_rrhh->query("select * from persona.funcionario where codigo_biometrico = '".$biom."'");	
		return $query->result();  	
	}
	function verifnumnua($nua)
	{
		$query = $this->db_rrhh->query("select * from persona.funcionario where nua_cua = '".$nua."'");	
		return $query->result();  	
	}
	function verifnumcuenta($cuenta)
	{
		$query = $this->db_rrhh->query("select * from persona.funcionario where numerocuenta = '".$cuenta."'");	
		return $query->result();  	
	}


function guardarfun()
	{
		$telef = $this->input->get('telefono');
		$celu = $this->input->get('celular');
		if($telef == '') $telef = null;
		if($celu == '') $celu = null;
		$data = array(
			'id_tipodocumento' => $this->input->get('tipoDoc'),
			'id_expedido' => $this->input->get('expedido'),
			'id_estadocivil' => $this->input->get('estCivil'),
			'nombres' => $this->input->get('nombre'),
			'paterno' => $this->input->get('paternoFun'),
			'materno' => $this->input->get('maternoFun'),
			'nro_documento' => $this->input->get('ndocumento'),
			'fecha_nacimiento' => $this->input->get('fecha'),
			'lugar_nacimiento' => $this->input->get('lugarnac'),
			'genero' => $this->input->get('genero'),
			'fono' => $telef,
			'cel' => $celu,
			'domicilio' => $this->input->get('domicilio'),
			'email' => $this->input->get('email'),
			'profesion' => $this->input->get('profesion'),
			'codigo_biometrico' => $this->input->get('codbiometrico'),
			'numerocuenta' => $this->input->get('ncuenta'),
			'id_afp' => $this->input->get('afpseg'),
			'nua_cua' => $this->input->get('nua')
			
		 );
		$this->db_rrhh->insert('persona.funcionario',$data);
		if($this->db_rrhh->affected_rows() > 0) return TRUE;
		else return FALSE;

	}
	function editarfun()
	{
		$telef = $this->input->get('telefono');
		$celu = $this->input->get('celular');
		if($telef == '') $telef = null;
		if($celu == '') $celu = null;
		$data = array(
			'id_tipodocumento' => $this->input->get('tipoDoc'),
			'id_expedido' => $this->input->get('expedido'),
			'id_estadocivil' => $this->input->get('estCivil'),
			'nombres' => $this->input->get('nombre'),
			'paterno' => $this->input->get('paternoFun'),
			'materno' => $this->input->get('maternoFun'),
			'nro_documento' => $this->input->get('ndocumento'),
			'fecha_nacimiento' => $this->input->get('fecha'),
			'lugar_nacimiento' => $this->input->get('lugarnac'),
			'genero' => $this->input->get('genero'),
			'fono' => $telef,
			'cel' => $celu,
			'domicilio' => $this->input->get('domicilio'),
			'email' => $this->input->get('email'),
			'profesion' => $this->input->get('profesion'),
			'codigo_biometrico' => $this->input->get('codbiometrico'),
			'numerocuenta' => $this->input->get('ncuenta'),
			'id_afp' => $this->input->get('afpseg'),
			'nua_cua' => $this->input->get('nua')
			
		 );

		$this->db_rrhh->where('id_funcionario', $this->input->get('idfunc'));
		$this->db_rrhh->update('persona.funcionario',$data);
		if($this->db_rrhh->affected_rows() > 0) return TRUE;
		else return FALSE;

	}

*/











	



	
	
	
	
	
	

	
	


	

	



}
?>