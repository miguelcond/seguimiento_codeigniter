<?php
/*
*/

class Administrador_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		  //$this->db_rrhh = $this->load->database('db_rrhh', TRUE); 
		  //$this->db_siape = $this->load->database('db_siape', TRUE);    
		  //$this->tabla = 'persona.funcionario';
	}
	public function countTipoProyectosAdmin($depto)
    {        
        if ($depto=='') $consulta = "where 1=1";
        else
        $consulta = "where departamento ilike('%$depto%')";
        $query = $this->db->query("select distinct tipo_proy, count(*) as cantidad,
                                            round(count(*)*100/(select sum(a.cantidad) from (select count(tipo_proy) as cantidad  from datos_proyecto.proyecto_datos $consulta) 
                                               as a),2)as porcentaje--con dos decimales
                                   from datos_proyecto.proyecto_datos $consulta
                                   group by tipo_proy
                                   order by tipo_proy asc");	
        return $query->result();        
    }
	public function countCartera($depto) //general
    {        
        if ($depto=='') $consulta = "where 1=1";
        else
        $consulta = "where departamento ilike(''%$depto%'')";

        $query = $this->db->query("select * from crosstab(
'select distinct departamento, cartera, count(cartera) as catidad 
from datos_proyecto.proyecto_datos 
$consulta
--where departamento = ''ORURO'' -- and tipo_proy = ''RIEGO''
group by departamento, cartera order by departamento, cartera asc', 'SELECT distinct cartera from datos_proyecto.proyecto_datos order by cartera asc')
as ct(depto text, cartera1 int, cartera2 int)");
//var_dump($query);	
        return $query->result();        
    }
	 public function listaDeptos()
    {        
        $query = $this->db->query("select distinct departamento from datos_proyecto.proyecto_datos order by departamento asc");	
        return $query->result();        
    } 
	public function getUsuarios() //2022
    {        
        $query = $this->db->query("select u.*, tu.nombre as tipousuario from seguridad.usuarios u
join seguridad.tipo_usuarios tu on u.tipo_usu=tu.id_tipo_usuario order by id_usuario asc");	
        return $query->result();        
    }
    public function cant_usuarios(){
    	$query = $this->db->query("select count(*) as numero from seguridad.usuarios");
    	return $query->row();
    }
    public function cantUsuariosTecnicos(){
    	$query = $this->db->query("select count(*) as numero from seguridad.usuarios where tipo_usu=1");
    	return $query->row();
    }
    public function activo($id) {
        $query = $this->db->get_where('seguridad.usuarios', array('id_usuario' => $id));
        if ($query->num_rows() > 0) {
            if ($query->row()->activo == 't') {
                $this->db->update('seguridad.usuarios', array('activo' => false), array('id_usuario' => $id));
                
            } else {
                $this->db->update('seguridad.usuarios', array('activo' => true), array('id_usuario' => $id));
            	
            }
        }
    }
 	public function selec_datosUs($idUs)
    {        
        $query = $this->db->query("select * from seguridad.usuarios where id_usuario = '".$idUs."' ");	
        return $query->row();        
    }

 function listaTipoUs()
	{
		$query = $this->db->query("select * from seguridad.tipo_usuarios order by id_tipo_usuario asc ");	
		return $query->result();  	
	}




function nombreUs($nom,$pat){
	//$nomMinusculas = strtolower($nom);
	$armar= explode(' ',strtolower($nom));
	$armar1=$armar[0];
	$i=0;
	$num=1;
	//$us=$armar[0].'.'.strtolower($pat);
	
	while ($i<1){
		$us=$armar[0].'.'.strtolower($pat);
		
		$query1=$this->db->query("select * from seguridad.usuarios where usuario ilike('%$us%')");
		if($query1->result()){
			$armar[0]=$armar1.$num;
			$num++;
		}
		else
		$i=2;	

	}
	return $us;
}


	
function guardarUs()
	{
		//$telef = $this->input->get('telefono');
		//$celu = $this->input->get('celular');
		//if($telef == '') $telef = null;
		//if($celu == '') $celu = null;
		$nombreUsuario = $this->nombreUs($this->input->get('nombre'),$this->input->get('apellidoPat'));

		$data = array(
			'usuario'=>$nombreUsuario,
			'contrasena'=>md5($this->input->get('ci')),
			'nombre' => $this->input->get('nombre'),
			'apellido_pat' => $this->input->get('apellidoPat'),
			'apellido_mat' => $this->input->get('apellidoMat'),
			'ci' => $this->input->get('ci'),
			'unidad_org' => $this->input->get('org'),
			'activo'=> true,
			'tipo_usu' => $this->input->get('tipoUs')
		);
		$this->db->insert('seguridad.usuarios',$data);
		$sqldato = $this->db->last_query();

		$sqldato = str_replace("'", '"', $this->db->last_query()); //$this->db->insert_id()//devuelve el ultimo id de la tabla //$this->db->last_query()//ultima consulta
		$this->funciones->bitacoragral('seguridad.usuarios','insert',json_encode($data),$this->session->userdata('per'),$this->db->insert_id(),$sqldato,$this->funciones->getip());
		
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}


function editarUs()
	{
		//$nombreUsuario = $this->nombreUs(trim($this->input->get('nombre')),trim($this->input->get('apellidoPat')));
		$data = array(
			//'usuario'=>$nombreUsuario,
			'nombre' => $this->input->get('nombre'),
			'apellido_pat' => $this->input->get('apellidoPat'),
			'apellido_mat' => $this->input->get('apellidoMat'),
			'ci' => $this->input->get('ci'),
			'unidad_org' => $this->input->get('org'),
			'tipo_usu' => $this->input->get('tipoUs')
			
		 );

		$this->db->where('id_usuario', $this->input->get('idUsc'));//$this->input->get('idUsc') es el id que se esta modificando
		$this->db->update('seguridad.usuarios',$data);
		$sqldato = $this->db->last_query();

		$sqldato = str_replace("'", '"', $this->db->last_query());
		$this->funciones->bitacoragral('seguridad.usuarios','update',json_encode($data),$this->session->userdata('per'),$this->input->get('idUsc'),$sqldato,$this->funciones->getip());
		
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}
function verifnumci($num)
	{
		$query = $this->db->query("select * from seguridad.usuarios where ci = '".$num."'");	
		return $query->result();  	
	}

public function getAsignadosUs($idus) 
    {        
        $query = $this->db->query("select * from datos_proyecto.view_asignados where id_us='".$idus."'");	
        return $query->result();        
    }
public function getPorAsignadar() 
    {        
        $query = $this->db->query("select * from datos_proyecto.view_porAsignar");	
        return $query->result();        
    }
    public function cant_asignados(){
    	$query = $this->db->query("select count(*) as numero from datos_proyecto.view_asignados");
    	return $query->row();
    }
    public function cant_porAsignar(){
    	$query = $this->db->query("select count(*) as numero from datos_proyecto.view_porAsignar");
    	return $query->row();
    }
    public function cant_asignUs($idus){
    	$query = $this->db->query("select count(*) as numero from datos_proyecto.view_asignados where id_us='".$idus."'");
    	return $query->row();
    }
    public function total_proy(){
    	$query = $this->db->query("select count(*) as numero from datos_proyecto.proyecto_datos");
    	return $query->row();
    }
    public function datos_usuario($idus) //2022
    {        
        $query = $this->db->query("select u.*, tu.nombre as tipousuario from seguridad.usuarios u
join seguridad.tipo_usuarios tu on u.tipo_usu=tu.id_tipo_usuario where id_usuario='".$idus."'");	
        return $query->row();        
    }
    function guardarAsignacion()
	{
		$data = array(
			'id_proy'=>$this->input->get('idproyecto'),
			'id_us'=>$this->input->get('idusuario'),
			'fecha_asign' => date('Y-m-d'),
			'estado' => true
			
		);
		$this->db->insert('datos_proyecto.asignacion',$data);

		$sqldato = str_replace("'", '"', $this->db->last_query()); //$this->db->insert_id()//devuelve el ultimo id de la tabla //$this->db->last_query() ultima consulta
		$this->funciones->bitacoragral('datos_proyecto.asignacion','insert',json_encode($data),$this->session->userdata('per'),$this->db->insert_id(),$sqldato,$this->funciones->getip());

		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}
	function guardarDesasignarProy()
	{
		
		$idasign = $this->input->get('idasignacion');

		$data = array(
			'fecha_dev' => date('Y-m-d'),
			'estado' => false
			
		);
		//$this->db->insert('datos_proyecto.asignacion',$data);

		$this->db->where('id_asig', $this->input->get('idasignacion'));
		$this->db->update('datos_proyecto.asignacion',$data);

		$sqldato = str_replace("'", '"', $this->db->last_query());
		$this->funciones->bitacoragral('datos_proyecto.asignacion','update',json_encode($data),$this->session->userdata('per'),$this->input->get('idasignacion'),$sqldato,$this->funciones->getip());
		
		if($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}



}
?>