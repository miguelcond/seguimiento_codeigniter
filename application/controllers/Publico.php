<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /**
 * 
 */
 class Publico extends CI_Controller
 {
 	
 	public function __construct()
 	{
 		parent::__construct();		
 	}
 	public function index(){

 		$data=array();
 		$data['page']='Autenticacion';
 		$this->load->view('Publico',$data);
 	}

 	function login() {
 		$rules = array(
			array(
				'field'   => 'usuario',
				'label'   => 'usuario',
				'rules'   => 'required|trim|min_length[6]'
			),
			array(
				'field'   => 'password',
				'label'   => 'contrase&ntilde;a',
				'rules'   => 'required|trim|min_length[5]'
			)
			
		);
		
		$this->form_validation->set_rules($rules);
		$this->form_validation->set_message('required', '- Ingrese el campo %s');
		$this->form_validation->set_message('min_length', '- El campo %s debe contener minimo %s caracteres');
		//$this->form_validation->set_message('valid_email', 'Formato de E-mail invalido');
		//$this->form_validation->set_message('matches', 'El campo %s debe coincidir con el campo %s');
		$data = array();
		$data['page'] = 'SEGUIMIENTO';
		if($this->form_validation->run()) {
			$resp = $this->logeo->login(trim($this->input->post('usuario')), trim($this->input->post('password')));
			if(!isset($resp['error'])) 
				
				{
					$nivel = $this->session->userdata('controlador');
					redirect($nivel);
						
					//redirect('Proyecto');		
				}
			
			else $data['error'] = $resp['error'];
			//$data['cap_img'] = $this->get_captcha();
		} //else $data['cap_img'] = $this->get_captcha();
		$this->load->view('Publico', $data);
	}
	function cerrarses() {//
		$this->logeo->logout();
		//redirect('Publico');
	}

 }

?>