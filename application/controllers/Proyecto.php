<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* This is Example Controller
*/
class Proyecto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Proyecto_model');
		$this->load->library('pdf4');
		$this->template->write_view('sidenavs', 'template/usuario1', true);
		$this->template->write_view('navs', 'template/default_topnavs_proy.php', true);
	}

	function index() {
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Inicio');

		$valor['deptos'] = $this->Proyecto_model->listaDeptos();
		$this->template->write_view('content', 'tes/dashboard',$valor, true);

		$this->template->render();
	}
	public function cantCArtera(){
	   $depto = $this->input->post('dep');
	   //var_dump($depto);
       //$datos = $this->Proyecto_model->countCartera($depto);
       $datos = $this->Proyecto_model->countCarteraFunc($depto);
       $json = json_encode($datos); 
       echo $json;
	}
	public function cantTipoProy(){
	   $depto = $this->input->post('dep');
	   //var_dump($depto);
       $datos = $this->Proyecto_model->countTipoProyectosusu($depto);
       $json = json_encode($datos); 
       echo $json;
	}
	public function detailsficha(){//fdi
			$idval=$this->input->post('id1');
			$dato1['filas1'] = $this->Proyecto_model->getproyectoficha($idval);
			$this->load->view('proy/ficha', $dato1);
		
	}
	public function details(){
		if($this->uri->segment(3))
		{
			$customer_id = $this->uri->segment(3);
			$dato1['filas1'] = $this->Proyecto_model->getproyectoficha($customer_id);
			//$data['customer_details'] = $this->htmltopdf_model->fetch_single_details($customer_id);
			$this->load->view('proy/ficha', $dato1);
		}
	}
	public function pdfdetails()
	{
		if($this->uri->segment(3))
		{

			$customer_id = $this->uri->segment(3);
			$dato1['filas1'] = $this->Proyecto_model->getproyectoficha($customer_id);
		//$this->template->write_view('content', 'proy/listado', $dato, true);

			ob_start();
			$customer_id = $this->uri->segment(3);
			$this->load->view('proy/ficha',$dato1);
			//$html_content=$this->output->get_output();
			//$html_content = ob_get_contents();
			//ob_end_clean();
			//$html_content .= $this->htmltopdf_model->fetch_single_details($customer_id);
			//$dompdf = new PDF();
			$this->pdf4->set_paper('letter', 'landscape');
			$this->pdf4->loadHtml(ob_get_clean());
			$this->pdf4->render();
			$pdf = $this->pdf4->output();
			$filename = "ejemplo1.pdf";
			file_put_contents($filename,$pdf);
			$this->pdf4->stream($filename);
			//$this->pdf4->stream("".$customer_id.".pdf", array("Attachment"=>0));
		}
	}
	function simple_template() {
		$this->template->set_template('default');
		$this->template->write('header', 'This is Header');
		$this->template->write('title', 'My Simple Template', TRUE);
		$this->template->write_view('content', 'tes/mypage', '', true);

		$this->template->render();
	}

	function dashboard() {
		$this->template->write('title', 'Dashboard', TRUE);
		$this->template->write('header', 'Dashboard');
		$this->template->write_view('content', 'tes/dashboard', '', true);

		$this->template->render();
	}

	function form_ex() {
		$this->template->write('title', 'Gentelella Template', TRUE);
		$this->template->write('header', 'This is Header');
		$this->template->write_view('content', 'tes/form', '', true);

		$this->template->render();
	}

	function table_ex() {
		$this->template->write('title', 'Gentelella Template', TRUE);
		$this->template->write('header', 'Table Dynamics<small>Some examples</small>');
		$this->template->write_view('content', 'tes/table', '', true);

		$this->template->render();

	}
	function table_dyn_ex() {
		$this->template->write('title', 'Gentelella Template', TRUE);
		$this->template->write('header', 'Table <small>Some examples</small>');
		$this->template->write_view('content', 'tes/table_dynamic', '', true);

		$this->template->render();

	}


	//desde aqui
	function lista_proy() {
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Listado/registro <small>de Proyectos</small>');
		$dato['filas'] = $this->Proyecto_model->getproyectos();
		$this->template->write_view('content', 'proy/listado', $dato, true);
		$this->template->render();
	}
	function getDepartamento()
	{
		$filas = $this->Proyecto_model->listaDepartamento();
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}
	function getProv()
	{
		$idDep=$this->input->post('departam');
		$filas = $this->Proyecto_model->listaProvicnia($idDep);
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}
	function getMun()
	{
		$idProv=$this->input->post('prov');
		$filas = $this->Proyecto_model->listaMunicipio($idProv);
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}












 function getEjecutora()
	{
		$filas = $this->Proyecto_model->listaEjecutora();
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id_ejecutora."'>".$fila->sigla."</option>";		
		}
		echo $option;

	}
 function getIndicador()
	{
		$filas = $this->Proyecto_model->listaIndicador();
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id_indicador."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}
    function getUmedida()
	{
		$filas = $this->Proyecto_model->listaUmedida();
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id_unidad."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}
	function getCartera()
	{
		$filas = $this->Proyecto_model->listaCartera();
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id_cartera."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}

	function getTipoProyecto()
	{
		$filas = $this->Proyecto_model->listaTipoProy();
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id_tipop."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}
	function getTipoInversion()
	{
		$filas = $this->Proyecto_model->listaTipoInv();
		$option = "<option value=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id_inversion."'>".$fila->descripcion."</option>";		
		}
		echo $option;

	}
	function getDepartamento2()
	{ $abuscar = $this->input->post('term');
	//echo var_dump($abuscar);
		$filas = $this->Proyecto_model->listaDepartamento2($abuscar);
		 foreach ($filas as $key => $entidad) {
            $row_set[] = $entidad->descripcion;
        }
        echo json_encode($row_set);

	}
	function saveTipo()
	{ //$abuscarEnt = $this->funciones->toSQL($this->input->post('ent'));
	  $abuscarEnt = $this->input->post('ent'); 	
	  $sigla = $this->funciones->toSQL($this->input->post('sigl')); 	
	//echo var_dump($abuscar);
		$filas = $this->Proyecto_model->verifTipo($abuscarEnt);
		if($filas > 0 )
		echo 1;
		else{
			$resp=$this->Proyecto_model->guardarEntidadE($abuscarEnt,$sigla);
			$filas=$this->Proyecto_model->listaEjecutora();
			$option = "<option value=''>Seleccione ....</option>";
				foreach ($filas as $fila) 
				{
					if ($fila->id_ejecutora == $resp) $select="selected"; else $select="";
					$option.="<option value = '".$fila->id_ejecutora."' $select >".$fila->sigla."</option>";	
				}
				echo $option;
				} 

	}
	function validaConv()
	{
		$convenio = trim($this->input->post('conv'));
		$datos = $this->Proyecto_model->verifConvenio($convenio);
		if($datos) echo 1;
		else echo 0; 
	}

	function registrar(){
		//// 1
		$tipoP = $this->funciones->toSQL($this->input->post('tipoProyecto'));
		$carteraP = $this->funciones->toSQL($this->input->post('carteraProyecto'));
		$familiasB = $this->funciones->toSQL($this->input->post('familiasBeneficiadas'));
		$tipoI = $this->funciones->toSQL($this->input->post('tipoInversion'));
		$codigoS = $this->funciones->toSQL($this->input->post('codigoSisin'));
		$nombreP = $this->funciones->toSQL($this->input->post('nombreProyecto'));
		
		//// 2
		$deptp = $this->funciones->toSQL($this->input->post('departamento'));
		$prov = $this->funciones->toSQL($this->input->post('cbprovincia'));
		$mun = $this->funciones->toSQL($this->input->post('cbmunicipio'));
		$lat = $this->funciones->toSQL($this->input->post('txtLatitud'));
		$long = $this->funciones->toSQL($this->input->post('txtLongitud'));
		
		////// 3
		$numeriC = $this->funciones->toSQL($this->input->post('numeroConvenio'));
		$fechaC = $this->funciones->toSQL($this->input->post('fechaConvenio'));
		$plazoI = $this->funciones->toSQL($this->input->post('plazoInicial'));
		$montoF = $this->funciones->toSQL($this->input->post('montoFinanciamiento'));
		//$fechaInC = $this->funciones->toSQL($this->input->post('fechaInicConvenio'));
		//$subirC = $this->funciones->toSQL($this->input->post('subirConvenio')); 
		$inversionF = $this->funciones->toSQL($this->input->post('inversionFdi')); 
		$inversionG = $this->funciones->toSQL($this->input->post('inversionGam')); 
		$inersionB = $this->funciones->toSQL($this->input->post('inversionBeneficiarios')); 
		$inversionO = $this->funciones->toSQL($this->input->post('inversionOtros')); 
		$emtidadEj = $this->funciones->toSQL($this->input->post('entidadEjecutora')); 
		
		////// 4
		$indicadorD = $this->funciones->toSQL($this->input->post('indicadorDesglosado'));
		$unidadM = $this->funciones->toSQL($this->input->post('unidadMedida'));
		$metaP = $this->funciones->toSQL($this->input->post('metaProyecto'));
		$this->input->post('nombreProyecto');
	}

function editarPlazosEjec() {
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Edicion de Proyectos');
		$dato['filas'] = $this->Proyecto_model->getproyectosPlazos();
		//$dato['departamentos'] = $this->Proyecto_model->listaDepartamento();
		$this->template->write_view('content', 'proy/plazosEjec', $dato, true);
		$this->template->render();
		//echo $this->router->class."<br>";
		//echo $this->router->method;
	}

//////////////INCIO SITUACIONES
/*	
function procesoSituaciones(){
$this->db->trans_begin();
		$this->Proyecto_model->editarSituacion();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;	
			}	
}
*/
function editarSituaciones() {
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Edicion de Proyectos Situaciones/inspecciones');
		$this->template->write_view('content', 'proy/situaciones', true);
		$this->template->render();
	}

	
public function listaSituaciones(){ 
	$datos = $this->Proyecto_model->getproyectosSituaciones();
	$datosUsuariosT = $this->Proyecto_model->getUsuariosTec();
			if($datos){
				foreach($datos as $key=>$fila){
							$option1 = "<option value=''>Seleccione ....</option>";
							foreach ($datosUsuariosT as $fila1) 
							{
								if ($fila->tecnico_inspeccion1 == $fila1->id_usuario)
								$option1.="<option value = '".$fila1->id_usuario."' selected>".$fila1->usuario."</option>";		
								else
								$option1.="<option value = '".$fila1->id_usuario."'>".$fila1->usuario."</option>";	
							}
							$option2 = "<option value=''>Seleccione ....</option>";
							foreach ($datosUsuariosT as $fila2) 
							{
								if ($fila->tecnico_inspeccion2 == $fila2->id_usuario)
								$option2.="<option value = '".$fila2->id_usuario."' selected>".$fila2->usuario."</option>";		
								else
								$option2.="<option value = '".$fila2->id_usuario."'>".$fila2->usuario."</option>";	
							}
							$option3 = "<option value=''>Seleccione ....</option>";
							foreach ($datosUsuariosT as $fila3) 
							{
								if ($fila->tecnico_inspeccion3 == $fila3->id_usuario)
								$option3.="<option value = '".$fila3->id_usuario."' selected>".$fila3->usuario."</option>";		
								else
								$option3.="<option value = '".$fila3->id_usuario."'>".$fila3->usuario."</option>";	
							}


						$num = $key+1;
						$nom = $fila->nombre_proy;
						$data[] = array(
							"nro"=>"<label>".$num."</label>",
							"edit"=>"<a href='#' onclick='editarProyectoAjax();'><i class='fa fa-toggle-off fa-2x' style='color:#1abb9c'></i><span style='display:none'>0</></a>",
							"cart"=>$fila->cartera,
							"depa"=>$fila->departamento,
							"munc"=>$fila->municipio,
							"nombre"=>"<label ondblclick='marcarFiladbl();'>".$nom."</label>",
							"nConv"=>$fila->num_convenio,

							"otrassituaciones"=>"<span class='validation iconok'></span>
                                        <textarea disabled style='width:100%' class='inputborder editableSituaciones' name='otras_situaciones:".$fila->id."' id='otras_situaciones:".$fila->id."'>".$fila->otras_situaciones."</textarea>",             

                            "inspeccion1"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableSituaciones' type='date' name='fecha_inspeccion_campo1:".$fila->id."' id='fecha_inspeccion_campo1:".$fila->id."' value='".$fila->fecha_inspeccion_campo1."'>", 
                            "tecnicoins1"=>"<span class='validation iconok'></span>
                            		   <select disabled class='inputborder editableSituaciones' name='tecnico_inspeccion1:".$fila->id."' id='tecnico_inspeccion1:".$fila->id."'>
                                       ".$option1."                 
                                       </select>",                         
                            "inspeccion2"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableSituaciones' type='date' name='fecha_inspeccion_campo2:".$fila->id."' id='fecha_inspeccion_campo2:".$fila->id."' value='".$fila->fecha_inspeccion_campo2."'>",            
                            "tecnicoins2"=>"<span class='validation iconok'></span>
                            		   <select disabled class='inputborder editableSituaciones' name='tecnico_inspeccion2:".$fila->id."' id='tecnico_inspeccion2:".$fila->id."'>
                                       ".$option2."                 
                                       </select>",
                            "inspeccion3"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableSituaciones' type='date' name='fecha_inspeccion_campo3:".$fila->id."' id='fecha_inspeccion_campo3:".$fila->id."' value='".$fila->fecha_inspeccion_campo3."'>",
                            "tecnicoins3"=>"<span class='validation iconok'></span>
                            		   <select disabled class='inputborder editableSituaciones' name='tecnico_inspeccion3:".$fila->id."' id='tecnico_inspeccion3:".$fila->id."'>
                                       ".$option3."                 
                                       </select>",            



							"opc"=>"<a data-title='Ver' href='#' onclick='filaProyecto1()' data-toggle='modal'>opciones</a>",            


						);
						/*
						$row = [];
						$row[] = $key+1;
						$row[] = $fila->nombre;
						$output['aaData'][] = $row;
						*/
					}	
			}else{
						/*
						$row = [];
						$row[] = '';
						$row[] = '';
						$output['aaData'][] = $row;
						*/
						$data[] = array("nro"=>'',"nombre"=>'');
			}	
			$data=array("data"=>$data);		
		echo json_encode($data);

	}
///////////FIN SITUIACIONES	


//////INICIO Acuerdo
function actualizarAcuerdo(){//5 tico
	$id_proyecto = $this->input->post('id_val');
	$fecha_limite = $this->funciones->fecha($this->Proyecto_model->getFechaLimite($id_proyecto));
	$alerta_venc_plazos_ejec = $this->Proyecto_model->getAlertaPlazosEjec($id_proyecto);
	$datosArray = array("fecha_lim"=>$fecha_limite, "alertaA"=>(int)$alerta_venc_plazos_ejec." <i style='color: ".$this->funciones->alertacolores($alerta_venc_plazos_ejec).";' class='fa fa-warning fa-2x'></i>");
	echo json_encode($datosArray);

	//echo $fecha_limite."*".(int)$alerta_venc_plazos_ejec." <i style='color: ".$this->funciones->alertacolores($alerta_venc_plazos_ejec).";' class='fa fa-warning fa-2x'></i>";
}
function editarAcuerdo() {
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Edicion de Proyectos Acuerdo Conciliatorio - Ejecución');
		$this->template->write_view('content', 'proy/acuerdo', true);
		$this->template->render();
	}
	
public function listaacuerdo(){ 
	//$value = $this->input->post('func');	
	$datos = $this->Proyecto_model->getproyectosAcuerdos();
	$datosVigencia = $this->Proyecto_model->getVigenciaPl();
	
			if($datos){
				foreach($datos as $key=>$fila){
						    
						   	$option1 = '';
							$option1 = "<option value=''>Seleccione ....</option>";
							 if ($fila->acuerdo_conciliatorio_no == 'NO')
								$option1.="<option value = '".$fila->acuerdo_conciliatorio_no."' selected>".$fila->acuerdo_conciliatorio_no."</option>";
								else
								$option1.="<option value = 'NO'>NO</option>";	
							
							$option2 = "<option value=''>Seleccione ....</option>";
							foreach ($datosVigencia as $fila2) 
							{
								if ($fila->vigencia_plazo_ejecucion == $fila2->descripcion)
								$option2.="<option value = '".$fila2->descripcion."' selected>".$fila2->descripcion."</option>";		
								else
								$option2.="<option value = '".$fila2->descripcion."'>".$fila2->descripcion."</option>";	
							}


															
						$num = $key+1;
						$nom = $fila->nombre_proy;
						$data[] = array(
							"nro"=>"<label>".$num."</label>",
							"edit"=>"<a href='#' onclick='editarProyectoAjax();'><i class='fa fa-toggle-off fa-2x' style='color:#1abb9c'></i><span style='display:none'>0</></a>",
							"cart"=>$fila->cartera,
							"depa"=>$fila->departamento,
							"munc"=>$fila->municipio,
							"nombre"=>"<label ondblclick='marcarFiladbl();'>".$nom."</label>",
							"nConv"=>$fila->num_convenio,

							/*
							"notaintencion"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableVencimiento' type='date' name='nota_intencion_resolucion_fecha:".$fila->id."' id='nota_intencion_resolucion_fecha:".$fila->id."' value='".$fila->nota_intencion_resolucion_fecha."' onkeyup='this.value=Numeros(this.value)'>",
							"respgam"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableVencimiento' type='date' name='respuesta_gam_intencion_fecha:".$fila->id."' id='respuesta_gam_intencion_fecha:".$fila->id."' value='".$fila->respuesta_gam_intencion_fecha."'>",
                            "infcontinuidadfdi"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableVencimiento' type='date' name='informe_continuidad_fdi_fecha:".$fila->id."' id='informe_continuidad_fdi_fecha:".$fila->id."' value='".$fila->informe_continuidad_fdi_fecha."' onkeyup='this.value=Numeros(this.value)'>",
							*/

							"acuerdoconc"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableAcuerdo' type='date' name='acuerdo_conciliatorio_fecha:".$fila->id."' id='acuerdo_conciliatorio_fecha:".$fila->id."' value='".$fila->acuerdo_conciliatorio_fecha."'>",
                            "acuerdoconcno"=>"<span class='validation iconok'></span>
                            		   <select disabled class='inputborder editableAcuerdo' name='acuerdo_conciliatorio_no:".$fila->id."' id='acuerdo_conciliatorio_no:".$fila->id."'>
                                       ".$option1."                 
                                       </select>", 
                            "plazoconciliatorio"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableAcuerdo' type='text' name='plazo_acuerdo_conciliatorio:".$fila->id."' id='plazo_acuerdo_conciliatorio:".$fila->id."' value='".$fila->plazo_acuerdo_conciliatorio."' onkeyup='this.value=Numeros(this.value)'>",
                            "fechalimite"=>"<span id='editFechaLimite_".$fila->id."'>".$this->funciones->fecha($fila->fecha_limite_segun_ac_conc)."</span>",
                            
                            "vigenciaplazos"=>"<span class='validation iconok'></span>
                            		   <select disabled class='inputborder editablePlazos' name='vigencia_plazo_ejecucion:".$fila->id."' id='vigencia_plazo_ejecucion:".$fila->id."'>
                                       ".$option2."                 
                                       </select>",

                            "alertaPlazos"=>"<span id='editAlertaPlazos_".$fila->id."'>".(int)$fila->alerta." <i style='color: ".$this->funciones->alertacolores($fila->alerta).";' class='fa fa-warning fa-2x'></i>"."</span>",
                           
                           "opc"=>"<a data-title='Ver' href='#' onclick='filaProyecto1()' data-toggle='modal'>opciones</a>",            


						);
						/*
						$row = [];
						$row[] = $key+1;
						$row[] = $fila->nombre;
						$output['aaData'][] = $row;
						*/
					}	
			}else{
						/*
						$row = [];
						$row[] = '';
						$row[] = '';
						$output['aaData'][] = $row;
						*/
						$data[] = array("nro"=>'',"nombre"=>'');
			}	
			$data=array("data"=>$data);		
		echo json_encode($data);

	}
//////////FIN acuerdo

//////////INICIO PLAZOS	
function actualizarPlazosAjax(){
	$id_proyecto = $this->input->post('id_val');
	$alerta_venc_plazos_ejec = $this->Proyecto_model->getAlertaPlazosEjec($id_proyecto);
	$datosArray = array("ampliaciones_t"=>$this->funciones->ampliacionesTotal($id_proyecto), 
		                "plazo_ejec_fin"=> $this->funciones->fecha($this->funciones->plazoEjecFinal($id_proyecto)),
		                "alertaplazos"=>(int)$alerta_venc_plazos_ejec." <i style='color: ".$this->funciones->alertacolores($alerta_venc_plazos_ejec).";' class='fa fa-warning fa-2x'></i>");
	echo json_encode($datosArray);
	//echo $this->funciones->ampliacionesTotal($id_proyecto)."*".$this->funciones->fecha($this->funciones->plazoEjecFinal($id_proyecto))."*".(int)$alerta_venc_plazos_ejec." <i style='color: ".$this->funciones->alertacolores($alerta_venc_plazos_ejec).";' class='fa fa-warning fa-2x'></i>";
}

function editarAjaxPlazos() { //1
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Edicion de Proyectos Plazos Ejecucion');
		$this->template->write_view('content', 'proy/plazosEjecucion', true);
		$this->template->render();
		
	}

public function listaPlazosEjecucion(){ 
	//$value = $this->input->post('func');	
	$datos = $this->Proyecto_model->getproyectosPlazos();
	$datosEstado = $this->Proyecto_model->getEstadosVenc();
	$datosVigencia = $this->Proyecto_model->getVigenciaPl();
			if($datos){
				foreach($datos as $key=>$fila){
					///
					 $option1 = "<option value=''>Seleccione ....</option>";
							foreach ($datosEstado as $fila1) 
							{
								if ($fila->estado == $fila1->descripcion)
								$option1.="<option value = '".$fila1->descripcion."' selected>".$fila1->descripcion."</option>";		
								else
								$option1.="<option value = '".$fila1->descripcion."'>".$fila1->descripcion."</option>";	
							}
							 $option2 = "<option value=''>Seleccione ....</option>";
							foreach ($datosVigencia as $fila2) 
							{
								if ($fila->vigencia_plazo_ejecucion == $fila2->descripcion)
								$option2.="<option value = '".$fila2->descripcion."' selected>".$fila2->descripcion."</option>";		
								else
								$option2.="<option value = '".$fila2->descripcion."'>".$fila2->descripcion."</option>";	
							}

					///
						$num = $key+1;
						$nom = $fila->nombre_proy;
						$data[] = array(
							"nro"=>"<label>".$num."</label>",
							"edit"=>"<a href='#' onclick='editarProyectoAjax();'><i class='fa fa-toggle-off fa-2x' style='color:#1abb9c'></i><span style='display:none'>0</></a>",
							"cart"=>$fila->cartera,
							"depa"=>$fila->departamento,
							"munc"=>$fila->municipio,
							"nombre"=>"<label ondblclick='marcarFiladbl();'>".$nom."</label>",
							"nConv"=>$fila->num_convenio,

							/*
							"fechai"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder' type='date' name='fecha_inicio:".$fila->id."' id='fecha_inicio:".$fila->id."' value='".$fila->fecha_inicio."'>",
							*/
                            "fechai"=>$this->funciones->fecha($fila->fecha_inicio),            
							"plazoe"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='plazo_ejec_convenio:". $fila->id."' id='plazo_ejec_convenio:".$fila->id."' value='".$fila->plazo_ejec_convenio."' onkeyup='this.value=Numeros(this.value)'>",
							"adenda1"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='adenda_conv1:".$fila->id."' id='adenda_conv1:".$fila->id."' value='".$fila->adenda_conv1."' onkeyup='this.value=Numeros(this.value)'>",
							"fechaadenda1"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='date' name='adenda_conv1_fecha:".$fila->id."' id='adenda_conv1_fecha:".$fila->id."' value='".$fila->adenda_conv1_fecha."'>",
                            "adenda2"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='adenda_conv2:".$fila->id."' id='adenda_conv2:".$fila->id."' value='".$fila->adenda_conv2."' onkeyup='this.value=Numeros(this.value)'>",
							"fechaadenda2"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='date' name='adenda_conv2_fecha:".$fila->id."' id='adenda_conv2_fecha:".$fila->id."' value='".$fila->adenda_conv2_fecha."'>",
                            "adenda3"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='adenda_conv3:".$fila->id."' id='adenda_conv3:".$fila->id."' value='".$fila->adenda_conv3."' onkeyup='this.value=Numeros(this.value)'>",
							"fechaadenda3"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='date' name='adenda_conv3_fecha:".$fila->id."' id='adenda_conv3_fecha:".$fila->id."' value='".$fila->adenda_conv3_fecha."'>",
                            "adenda4"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='adenda_conv4:".$fila->id."' id='adenda_conv4:".$fila->id."' value='".$fila->adenda_conv4."' onkeyup='this.value=Numeros(this.value)'>",
							"fechaadenda4"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='date' name='adenda_conv4_fecha:".$fila->id."' id='adenda_conv4_fecha:".$fila->id."' value='".$fila->adenda_conv4_fecha."'>",
                            "adenda5"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='adenda_conv5:".$fila->id."' id='adenda_conv5:".$fila->id."' value='".$fila->adenda_conv5."' onkeyup='this.value=Numeros(this.value)'>",
							"fechaadenda5"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='date' name='adenda_conv5_fecha:".$fila->id."' id='adenda_conv5_fecha:".$fila->id."' value='".$fila->adenda_conv5_fecha."'>",  

							"ampliacionresfdi"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='ampliacion_resol_fdi:".$fila->id."' id='ampliacion_resol_fdi:".$fila->id."' value='".$fila->ampliacion_resol_fdi."' onkeyup='this.value=Numeros(this.value)'>",
                           /**/"ampliacionresTotal"=> "<span id='editAmpliacionesT_".$fila->id_proy."'>".$this->funciones->ampliacionesTotal($fila->id_proy)."</span>",   
                            /**/"fechaPlazoEejecF"=>"<span id='editFechaPlazoE_".$fila->id_proy."'>".$this->funciones->fecha($this->funciones->plazoEjecFinal($fila->id_proy))."</span>",
                            /**/"porcenFinanciera"=>"<span id='editFechaPlazoE_".$fila->id_proy."'>".$fila->porcentaje_ejec_financiera."</span>",          
							"porcenFisica"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='text' name='porcentaje_ejec_fisica:".$fila->id."' id='porcentaje_ejec_fisica:".$fila->id."' value='".$fila->porcentaje_ejec_fisica."' onkeyup='this.value=Numeros(this.value)'>",

                            "estado"=>"<span class='validation iconok'></span>
                            		   <select disabled class='inputborder editablePlazos' name='estado:".$fila->id."' id='estado:".$fila->id."'>
                                       ".$option1."                 
                                       </select>", 
							"actaprov"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='date' name='acta_provisional_fecha:". $fila->id."' id='acta_provisional_fecha:".$fila->id."' value='".$fila->acta_provisional_fecha."' onkeyup='this.value=Numeros(this.value)'>",
							"actadef"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editablePlazos' type='date' name='acta_definitiva_fecha:".$fila->id."' id='acta_definitiva_fecha:".$fila->id."' value='".$fila->acta_definitiva_fecha."'>",
							"vigenciaplazos"=>"<span class='validation iconok'></span>
                            		   <select disabled class='inputborder editablePlazos' name='vigencia_plazo_ejecucion:".$fila->id."' id='vigencia_plazo_ejecucion:".$fila->id."'>
                                       ".$option2."                 
                                       </select>",
                            "alertaPlazos"=>"<span id='editAlertaPlazos_".$fila->id."'>".(int)$fila->alerta." <i style='color: ".$this->funciones->alertacolores($fila->alerta).";' class='fa fa-warning fa-2x'></i>"."</span>",           
                                        

                            "opc"=>"<a data-title='Ver' href='#' onclick='filaProyecto1()' data-toggle='modal'>opciones</a>",            


						);
						/*
						$row = [];
						$row[] = $key+1;
						$row[] = $fila->nombre;
						$output['aaData'][] = $row;
						*/
					}	
			}else{
						/*
						$row = [];
						$row[] = '';
						$row[] = '';
						$output['aaData'][] = $row;
						*/
						$data[] = array("nro"=>'',"nombre"=>'');
			}	
			$data=array("data"=>$data);		
		echo json_encode($data);

	}


////////////////FINPLAZOS

//////////INICIO DESEMBOLSOS	
function actualizarDesembolsosAjax(){//5
	$id_proyecto = $this->input->post('id_val');
	echo number_format($this->funciones->desemAcumFdi($id_proyecto), 2, ",", ".").":".number_format($this->funciones->saldoFdiF($id_proyecto), 2, ",", ".");
}

function procesoEditProyecto(){//3 
$this->db->trans_begin();
		$this->Proyecto_model->EditProyecto();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;	
			}	
}
function editarAjaxDesembolsos() { //1
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Edicion de Situacion Financiera');
		$this->template->write_view('content', 'proy/desembolsos', true);
		$this->template->render();
		
	}
public function listaHist(){ //4
	//$value = $this->input->post('func');	
	$datos = $this->Proyecto_model->getHistproyectos($this->input->post('datotab'));
			if($datos){
				foreach($datos as $key=>$fila){
						$num = $key+1;
						//$nom = $fila->nombre_proy;
						$data[] = array(
							"nro"=>$num,
							"proyectoh"=>$fila->nombre_proy,
							"campoh"=>$fila->campo, 
							"datoH"=>$fila->datos, 
							"datoAntH"=>$fila->datos_aterior, 
							"fechaH"=>date("d-m-Y h:i:s",strtotime($fila->fecha)),            
						);
					}	
			}else{
						/*
						$row = [];
						$row[] = '';
						$row[] = '';
						$output['aaData'][] = $row;
						*/
						$data[] = array("nro"=>'',"proyectoh"=>'',"campoh"=>'',"datoH"=>'',"datoAntH"=>'',"fechaH"=>'');
			}	
			$data=array("data"=>$data);		
		echo json_encode($data);

	}	
public function listaAlertas(){ //4
	//$value = $this->input->post('func');	
	$datos = $this->Proyecto_model->getAlertas();
			if($datos){
				foreach($datos as $key=>$fila){
						$num = $key+1;
						/*
						if($fila->alerta < 0) $color  = '#141212';
						if($fila->alerta >= 0 and $fila->alerta < 15) $color  = '#f30606';
						if($fila->alerta >= 15 and $fila->alerta < 30) $color  = '#f7ff3c';
						if($fila->alerta > 30) $color  = '#aee1a6';
						if($fila->alerta == null) $color  = '#ffff';
						*/
						$data[] = array(
							"nro"=>$num,
							"alerta"=>" <i style='color: ".$this->funciones->alertacolores($fila->alerta).";' class='fa fa-warning fa-2x'></i>",
							"dias"=>(int)$fila->alerta, /*con (int convertimos un string a integer)*/
							"cart"=>$fila->cartera,
							"nombre"=>"<label class=''>".$fila->nombre_proy."</labek>",
							"depa"=>$fila->departamento,
							"munc"=>$fila->municipio,
						);
					}	
			}else{
						/*
						$row = [];
						$row[] = '';
						$row[] = '';
						$output['aaData'][] = $row;
						*/
						$data[] = array("nro"=>'',"alerta"=>'',"dias"=>'',"cart"=>'',"nombre"=>'',"depa"=>'',"munc"=>'');
			}	
			$data=array("data"=>$data);		
		echo json_encode($data);

	}		
public function listaDesembolsos(){ //2
	//$value = $this->input->post('func');	
	$datos = $this->Proyecto_model->getproyectosDesembolsos();
			if($datos){
				foreach($datos as $key=>$fila){
						$num = $key+1;
						$nom = $fila->nombre_proy;
						$data[] = array(
							"nro"=>"<label>".$num."</label>",
							"edit"=>"<a href='#' onclick='editarProyectoAjax();'><i class='fa fa-toggle-off fa-2x' style='color:#1abb9c'></i><span style='display:none'>0</></a>",
							"cart"=>$fila->cartera,
							"depa"=>$fila->departamento,
							"munc"=>$fila->municipio,
							"nombre"=>"<label ondblclick='marcarFiladbl();'>".$nom."</label>",
							"nConv"=>$fila->num_convenio,

							"costoTotal"=>number_format($fila->costo_total, 2, ",", "."),
							"financFdi"=>number_format($fila->financiamiento_fdi, 2, ",", "."),            
							"financGam"=>number_format($fila->financiamiento_gam, 2, ",", "."),
							"financBenef"=>number_format($fila->financiamiento_benef, 2, ",", "."),
							


							"desem1"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableDesembolsos' type='text' name='primer_desembolso1-".$fila->id."' id='primer_desembolso1-".$fila->id."' value='".number_format($fila->primer_desembolso, 2, ",", ".")."' onchange='this.value=NumerosMonto(this.value, this.id)' onkeyup='this.value=NumerosPuntoComa(this.value)'><input disabled type='hidden' name='primer_desembolso:".$fila->id."' id='primer_desembolso:".$fila->id."' value='".$fila->primer_desembolso."'>",
                            "fechaDesem1"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableDesembolsos' type='date' name='fecha_primer_des:".$fila->id."' id='fecha_primer_des:".$fila->id."' value='".$fila->fecha_primer_des."'>",
							"desem2"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableDesembolsos' type='text' name='segundo_desembolso-".$fila->id."' id='segundo_desembolso-".$fila->id."' value='".number_format($fila->segundo_desembolso, 2, ",", ".")."' onchange='this.value=NumerosMonto(this.value, this.id)' onkeyup='this.value=NumerosPuntoComa(this.value)'><input disabled type='hidden' name='segundo_desembolso:".$fila->id."' id='segundo_desembolso:".$fila->id."' value='".$fila->segundo_desembolso."'>",
                            "fechaDesem2"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableDesembolsos' type='date' name='fecha_segudno_des:".$fila->id."' id='fecha_segudno_des:".$fila->id."' value='".$fila->fecha_segudno_des."'>",
							"desem3"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableDesembolsos' type='text' name='tercer_desembolso-".$fila->id."' id='tercer_desembolso-".$fila->id."' value='".number_format($fila->tercer_desembolso, 2, ",", ".")."' onchange='this.value=NumerosMonto(this.value, this.id)' onkeyup='this.value=NumerosPuntoComa(this.value)'><input disabled type='hidden' name='tercer_desembolso:".$fila->id."' id='tercer_desembolso:".$fila->id."' value='".$fila->tercer_desembolso."'>",
                            "fechaDesem3"=>"<span class='validation iconok'></span>
                                        <input disabled class='inputborder editableDesembolsos' type='date' name='fecha_tercer_des:".$fila->id."' id='fecha_tercer_des:".$fila->id."' value='".$fila->fecha_tercer_des."'>",


							"desemAcum"=>"<span id='editDesemAcum_".$fila->id_proy."'>".number_format($fila->desembolso_acumulado_fdi, 2, ",", ".")."</span>",
                            "saldoFdi"=>"<span id='editSaldoFdi_".$fila->id_proy."'>".number_format($fila->saldo_fdi, 2, ",", ".")."</span>",
							"opc"=>"<a data-title='Ver' href='#' onclick='filaProyecto1()' data-toggle='modal'>opciones</a>",            


						);
						
					}	
			}else{
						/*
						$row = [];
						$row[] = '';
						$row[] = '';
						$output['aaData'][] = $row;
						*/
						$data[] = array("nro"=>'',"nombre"=>'');
			}	
			$data=array("data"=>$data);		
		echo json_encode($data);

	}


////////////////FIN DESEMBOLSOS

function editar_proy() {
		$this->template->write('title', 'Seguimiento de Proyectos', TRUE);
		$this->template->write('header', 'Edicion de Proyectos');
		$dato['filas'] = $this->Proyecto_model->getproyectosborrador();
		$dato['departamentos'] = $this->Proyecto_model->listaDepartamento();
		$this->template->write_view('content', 'proy/borrador', $dato, true);
		$this->template->render();
		//echo $this->router->class."<br>";
		//echo $this->router->method;
	}
function proceso(){
$this->db->trans_begin();
		$this->Proyecto_model->editarProy();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;	
			}	
}


	

	function cerrarses() {//
		$this->logeo->logout();
		//redirect('Publico');
	}
}