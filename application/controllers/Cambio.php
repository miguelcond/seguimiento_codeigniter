<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Cambio extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cambio_model');
		$this->template->write_view('sidenavs', 'template/administrador', true);	//menu
		$this->template->write_view('navs', 'template/default_topnavs_proy.php', true);//cabecera
	}
/*
	public function index(){
		$this->template->write('title','Seguimiento de Proyectos',TRUE);
		$this->template->write('header','Inicio');
		$dato['usuarios'] = $this->Administrador_model->getUsuarios();
		$dato['cantidad'] = $this->Administrador_model->cant_usuarios();
		$this->template->write_view('content','admin/administrador',$dato,TRUE);
		$this->template->render();
	}
*/	
	function verificaPass()
	{
		$passw = trim($this->input->get('passsactual'));
		if($this->Cambio_model->verifPassw($passw) == FALSE)
		 echo 0;
		else 
		 echo 1;
	}
function modifPAss()
	{
		$this->db->trans_begin();
		$this->Cambio_model->editarPass();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;
			}

       //if($this->funcionario_model->editarfun() == TRUE) echo 1;
       // 	else echo 0;
			
	}
function logout() {
		$this->CI->session->sess_destroy();
	}
}
?>
