<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Administrador extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Administrador_model');
		$this->template->write_view('sidenavs', 'template/administrador', true);	//menu
		$this->template->write_view('navs', 'template/default_topnavs_proy.php', true);//cabecera
	}

	public function index(){
		$this->template->write('title','Seguimiento de Proyectos',TRUE);
		$this->template->write('header','Inicio');
		$valor['deptos'] = $this->Administrador_model->listaDeptos();
		//$dato['usuarios'] = $this->Administrador_model->getUsuarios();
		//$dato['cantidad'] = $this->Administrador_model->cant_usuarios();
		$this->template->write_view('content','admin/dashboardAdmin',$valor,TRUE);
		//$this->template->write_view('content','admin/administrador',$dato,TRUE);
		$this->template->render();
	}
	public function cantTipoProyAdmin(){
	   $depto = $this->input->post('dep');
	   //var_dump($depto);
       $datos = $this->Administrador_model->countTipoProyectosAdmin($depto);
       $json = json_encode($datos); 
       echo $json;
	}
	public function cantCArteraAdmin(){
	   $depto = $this->input->post('dep');
	   //var_dump($depto);
       //$datos = $this->Proyecto_model->countCartera($depto);
       $datos = $this->Administrador_model->countCartera($depto);
       $json = json_encode($datos); 
       echo $json;
	}
	function Administrar() {
		$this->template->write('title','Seguimiento de Proyectos',TRUE);
		$this->template->write('header','Inicio');
		$dato['usuarios'] = $this->Administrador_model->getUsuarios();
		$dato['cantidad'] = $this->Administrador_model->cant_usuarios();
		$this->template->write_view('content','admin/administrador',$dato,TRUE);
		$this->template->render();
	}
	function asigSeguimiento() {
		$this->template->write('title','Seguimiento de Proyectos',TRUE);
		$this->template->write('header','Seguimiento de Proyectos');
		$dato['usuarios'] = $this->Administrador_model->getUsuarios();
		$dato['cantidad'] = $this->Administrador_model->cantUsuariosTecnicos();
		$this->template->write_view('content','admin/asigSeguimiento',$dato,TRUE);
		$this->template->render();
	}
	function asigCierre() {
		$this->template->write('title','Cierre de Proyectos',TRUE);
		$this->template->write('header','Cierre de Proyectos');
		$dato['usuarios'] = $this->Administrador_model->getUsuarios();
		$dato['cantidad'] = $this->Administrador_model->cant_usuarios();
		$this->template->write_view('content','admin/asigCierre',$dato,TRUE);
		$this->template->render();
	}
	public function activoUsuario() {
        $this->Administrador_model->activo($this->input->post('id'));
    }
    function getTipoUsuario()
	{
		$filas = $this->Administrador_model->listaTipoUs();
		$option = "<option VALUE=''>Seleccione ....</option>";
		foreach ($filas as $fila) 
		{
			$option.="<option value = '".$fila->id_tipo_usuario."'>".$fila->nombre."</option>";		
		}
		echo $option;

	}
	function guardardatosUs()
	{
		$this->db->trans_begin();
		$this->Administrador_model->guardarUs();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;
			}
		//if($this->funcionario_model->guardarfun() == TRUE) echo 1;
        //	else echo 0;
			
	}
	function getUsuario()
    {
       $id = $this->input->get('id');
       $datos = $this->Administrador_model->selec_datosUs($id);
       $json = json_encode($datos); 
       echo $json;
    }
    function editardatosUs()
	{
		$this->db->trans_begin();
		$this->Administrador_model->editarUs();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;
			}

       //if($this->funcionario_model->editarfun() == TRUE) echo 1;
       // 	else echo 0;
			
	}
	function verificaCi()
	{
		$doc = trim($this->input->get('numci'));
		$datos = $this->Administrador_model->verifnumci($doc);
		if($datos) echo 1;
		else echo 0; 
	}
	function asigProy(){
		$isUsuario = $this->input->post('idUs');
		$this->template->write('title','Seguimiento de Proyectos',TRUE);
		$this->template->write('header','Asignacion de Proyectos');
		$dato['asignadosUs'] = $this->Administrador_model->getAsignadosUs($isUsuario);
		$dato['porAsignar'] = $this->Administrador_model->getPorAsignadar();
		$dato['cantidadAsignados'] = $this->Administrador_model->cant_asignados();
		$dato['cantidadPorAsignar'] = $this->Administrador_model->cant_porAsignar();
		$dato['cantidadAsignUsuaro'] = $this->Administrador_model->cant_asignUs($isUsuario);
		$dato['totalProyectos'] = $this->Administrador_model->total_proy();
		$dato['datosUs'] = $this->Administrador_model->datos_usuario($isUsuario);

		//$dato['idus'] = $id;
		$this->template->write_view('content','admin/asignar',$dato,TRUE);
		$this->template->render();
		
		/*
		$dato['cantidad'] = $this->Administrador_model->cant_usuarios();
		$dato['idus']=$id;
		$this->load->view('admin/asignar', $dato);
		*/
	}
	function asignProyectoUsuario()
	{
		$this->db->trans_begin();
		$this->Administrador_model->guardarAsignacion();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;
			}
		//if($this->funcionario_model->guardarfun() == TRUE) echo 1;
        //	else echo 0;
			
	}
	function desasignarProy()
	{
		$this->db->trans_begin();
		$this->Administrador_model->guardarDesasignarProy();
		if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				echo 0;				
			} else {
				$this->db->trans_commit();
				echo 1;
			}
		//if($this->funcionario_model->guardarfun() == TRUE) echo 1;
        //	else echo 0;
			
	}


function logout() {
		$this->CI->session->sess_destroy();
	}
}
?>
