<?php //defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller 
{
  function __construct(){
    parent::__construct();
    $this->load->library('pdf2');
    $this->load->library('pdf4');
    $this->load->model('Reportes_model');
    $this->template->write_view('sidenavs', 'template/usuario1', true);
    $this->template->write_view('navs', 'template/default_topnavs_proy.php', true);
   
  }
 

  function index()
  {
    
        $this->template->write('title','Seguimiento de Proyectos',TRUE);
        $this->template->write('header','Inicio - Reportes');
        $this->template->write_view('content','reportes/reportemenu','',TRUE);
        //$this->template->write_view('content','admin/administrador',$dato,TRUE);
        $this->template->render();
  }
  function reportegralPdf(){
    $campos = $this->input->post('chk');
    $dato1 = array();
    $dato1['tiporeporte'] = $this->input->get('vartipo');
    $count = 0;
    if(isset($_POST['chk'])=="")
         {
          echo "please select at least one";
         }

      else  
      foreach($campos as $campoTabla)
      {
       //echo $campoTabla.", ";
       $count ++;
      $dato1['tituloColumn'.$count] = $this->funciones->nombrecolumn($campoTabla);
      $dato1['campoMostrar'.$count] = $campoTabla;
       
      } 
  $dato1['filas'] = $this->Reportes_model->getproyectosgral();
  $dato1['cant'] = $count;
  ob_start();
  $this->load->view('reportes/reportegral', $dato1);

  $this->pdf4->set_paper('letter', 'landscape');
            $this->pdf4->loadHtml(ob_get_clean());
            $this->pdf4->render();
            $pdf = $this->pdf4->output();
            $filename = "reporte_seguimiento.pdf";
            file_put_contents($filename,$pdf);
            $this->pdf4->stream($filename);

  }
  function reportegralExcel(){
    $campos = $this->input->post('chk');
    $dato1 = array();
    $dato1['tiporeporte'] = $this->input->get('vartipo');
    $count = 0;
    if(isset($_POST['chk'])=="")
         {
          echo "please select at least one";
         }

      else  
      foreach($campos as $campoTabla)
      {
       //echo $campoTabla.", ";
       $count ++;
      $dato1['tituloColumn'.$count] = $this->funciones->nombrecolumn($campoTabla);
      $dato1['campoMostrar'.$count] = $campoTabla;
       
      } 
  $dato1['filas'] = $this->Reportes_model->getproyectosgral();
  $dato1['cant'] = $count;
  $this->load->view('reportes/reportegral', $dato1);

  }


function getDepartamentoR()
  {
    $filas = $this->Reportes_model->listaDeptos();
    $option = "<option value=''>Seleccione ....</option>";
    foreach ($filas as $fila) 
    {
      $option.="<option value = '".$fila->departamento."'>".$fila->departamento."</option>";   
    }
    echo $option;

  }
  function deptoGetProvR()
  {
    $dep=$this->input->post('departam');
    $filas = $this->Reportes_model->deptoListaProvincias($dep);
    $option = "<option value=''>Seleccione ....</option>";
    foreach ($filas as $fila) 
    {
      $option.="<option value = '".$fila->provincia."'>".$fila->provincia."</option>";   
    }
    echo $option;

  }
   function deptoGetMunR()
  {
    $dep=$this->input->post('departam');
    $filas = $this->Reportes_model->deptoListaMunicipios($dep);
    $option = "<option value=''>Seleccione ....</option>";
    foreach ($filas as $fila) 
    {
      $option.="<option value = '".$fila->municipio."'>".$fila->municipio."</option>";   
    }
    echo $option;

  }
  function ProvGetMunR()
  {
    $prov=$this->input->post('prov');
    $depto=$this->input->post('departam');
    $filas = $this->Reportes_model->ProvListaMunicipios($depto,$prov);
    $option = "<option value=''>Seleccione ....</option>";
    foreach ($filas as $fila) 
    {
      $option.="<option value = '".$fila->municipio."'>".$fila->municipio."</option>";   
    }
    echo $option;

  }
 function getDatos(){
  //$idval=$this->input->post('id1');
      //$dato1['datos1'] = $this->input->post('depto');
      //$dato1['datos2'] = $this->input->post('prov');
      //$dato1['datos3'] = $this->input->post('mun');
      $dato1['filas'] = $this->Reportes_model->getproyectosgralShow();
      $this->load->view('reportes/showReport', $dato1);
 }



  public function reportevalidaciondiario()
    {
        //$f1 = explode('/',$this->input->post('fecha1'));
        //$fecha_inicio = $f1[2].'-'.$f1[1].'-'.$f1[0];
        //$fecha_inicio_aux = $fecha_inicio;
        //$f2 = explode('/',$this->input->post('fecha2'));
        //$fecha_fin = $f2[2].'-'.$f2[1].'-'.$f2[0];
        
        //$idFuncionario = $this->session->userdata('idfuncionario');
        $columns = array();

    $col_color = "255,255,255";

    $celda_invisible = array('height' => '0', 'align' => 'J', 'font_name' => 'Times', 'font_size' => '8', 'font_style' => '', 'textcolor' => '255,255,255', 'drawcolor' => '255,255,255', 'linewidth' => '0', 'linearea' => 'LTBR');
    $celda_invisible2 = array('height' => '8', 'align' => 'J', 'font_name' => 'Times', 'font_size' => '10', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '255,255,255', 'linewidth' => '0', 'linearea' => 'LTBR');
    $tbl_etiqueta = array('height' => '4', 'align' => 'R', 'font_name' => 'Times', 'font_size' => '9', 'font_style' => 'B', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => '');
    $tbl_cabecera = array('height' => '5', 'align' => 'C', 'font_name' => 'Times', 'font_size' => '7', 'font_style' => 'B', 'fillcolor' => '173,173,173', 'textcolor' => '255,255,255', 'drawcolor' => '255,255,255', 'linewidth' => '0.2', 'linearea' => 'LTBR');

    $tbl_cuerpo1 = array('height' => '4', 'align' => 'C', 'font_name' => 'Times', 'font_size' => '8', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => 'LTBR');
    $tbl_fondoTitulos1 = array('height' => '4', 'align' => 'C', 'font_name' => 'Times', 'font_size' => '7', 'font_style' => 'b','fillcolor' => '110,110,110', 'textcolor' => '255,255,255', 'drawcolor' => '200,200,200', 'linewidth' => '0.2', 'linearea' => 'LTBR');
    $tbl_cuerpo_totales = array('height' => '4', 'align' => 'R', 'font_name' => 'Times', 'font_size' => '8', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => 'LTBR');
    $tbl_cuerpo2 = array('height' => '4', 'align' => 'J', 'font_name' => 'Times', 'font_size' => '8', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => 'BR');
    $tbl_cuerpo3 = array('height' => '4', 'align' => 'J', 'font_name' => 'Times', 'font_size' => '8', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => 'R');
    $tbl_cuerpo4 = array('height' => '4', 'align' => 'R', 'font_name' => 'Times', 'font_size' => '8', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => 'LTBR');
    $tbl_cuerpo = array('height' => '4', 'align' => 'J', 'font_name' => 'Times', 'font_size' => '9', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => '');
    $tbl_cuerpo_firmas = array('height' => '4', 'align' => 'C', 'font_name' => 'Times', 'font_size' => '8', 'font_style' => '', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2','linearea' => '');

    $ancho = array(30,55,38,25,25,25,25,25,25,25,15,15,15,15);
    $anchoinvisible = 10;
    $anchoinvisible2 = 200;
    $contador = 0;
    //$nombre_validador = $this->adminentidades_model->getNombreValidador($idFuncionario);
    
    $columnas = array();
    $sumTotBienesDeclarados = 0;
    $sumTotBienesValidados = 0;
    $sumTotBienesValidadosDocDef = 0;
    $sumTotBienesValidadosDocInt = 0;
    $sumTotBienesValidadosSinDoc = 0;
    $sumTotBienesBaja = 0;
    $sumTotBienesValidadosBaja = 0;
    $col1 = array();
    $col2 = array();
    $col3 = array();
    $col4 = array();
    $col5 = array();
    $col6 = array();
    
    $totaldocumentosasignados=0;
    $totaldocumentosvalidados=0;
    
    $col3[] = array_merge(array('text' => "",'width' => $anchoinvisible, 'fillcolor' => $col_color),$celda_invisible);
    $col3[] = array_merge(array('text' => utf8_decode('Fecha de Inicio: '/*.$fecha_inicio*/),'width' => $anchoinvisible2, 'fillcolor' => $col_color),$celda_invisible2);
    $col3[] = array_merge(array('text' =>  utf8_decode('Fecha Fin: '/*.$fecha_fin*/),'width' => $anchoinvisible2, 'fillcolor' => $col_color),$celda_invisible2);
    $columnas[] = $col3;
    
                $col1[] = array_merge(array('text' => "",'width' => $anchoinvisible, 'fillcolor' => $col_color),$celda_invisible);
                $col1[] = array_merge(array('text' => utf8_decode('Fecha'),'width' => $ancho[0], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $col1[] = array_merge(array('text' => utf8_decode('Entidad Públilca'), 'width' =>  $ancho[1], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $col1[] = array_merge(array('text' => utf8_decode('Nº Bienes Declarados'), 'width' =>  $ancho[3], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $col1[] = array_merge(array('text' => utf8_decode('Nº Bienes Validados'), 'width' =>  $ancho[4], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $col1[] = array_merge(array('text' => utf8_decode('Nº Documentos Declarados'), 'width' =>  $ancho[5], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $col1[] = array_merge(array('text' => utf8_decode('Nº Documentos Validados'), 'width' =>  $ancho[6], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $col1[] = array_merge(array('text' => utf8_decode('Nº Documentos Adicionados'), 'width' =>  $ancho[7], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $col1[] = array_merge(array('text' => utf8_decode('% Avance General'), 'width' =>  $ancho[8], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
                $columnas[] = $col1;
    
    $suma1 = 0;
    $suma2 = 0;
    $suma3 = 0;

        $col7[] = array_merge(array('text' => "",'width' => $anchoinvisible, 'fillcolor' => $col_color),$celda_invisible);
        $col7[] = array_merge(array('text' => "",'width' => $ancho[0], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $col7[] = array_merge(array('text' => "TOTALES ", 'width' =>  $ancho[1], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $col7[] = array_merge(array('text' => "", 'width' =>  $ancho[3], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $col7[] = array_merge(array('text' => $suma1, 'width' =>  $ancho[4], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $col7[] = array_merge(array('text' => "", 'width' =>  $ancho[5], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $col7[] = array_merge(array('text' => $suma2, 'width' =>  $ancho[6], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $col7[] = array_merge(array('text' => $suma3, 'width' =>  $ancho[7], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $col7[] = array_merge(array('text' => "", 'width' =>  $ancho[8], 'fillcolor' => $col_color),$tbl_fondoTitulos1);
        $columnas[] = $col7;
        unset($col7);


    $pdf = new Pdf2();
    $fecha_hoy = $pdf->fechacompleta();
    
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(true, 40);
   
    

    
    
 
    //$nombre_validador = $this->adminentidades_model->getNombreValidador($idFuncionario);
    $pdf->SetFont('Arial', 'B', 8);
     $encabezados = array('Nro',
            'Acción',
            'Entidad',
            'Rubro',
            'Idbien',
            'Iddocumento',
            //'Agregado por el Validador',
            'Documento',
            'Nro Documento',
            'Fecha y hora de Validación'
            );
      $w = array(7,17,48,38,12,12,57,25,29);         
        foreach ($encabezados as $val){
            $encabezados_[] = iconv('UTF-8', 'windows-1252', $val);
        }
        $pdf->setEncabezadoG($encabezados_);
        $pdf->setWidthsG($w);
        $pdf->xheader = 40;
        $pdf->yheader = 8;
        $pdf->cabecera = 2;
        $pdf->titulo = "REPORTE DE AVANCE DE VERIFICACION IN SITU";
        $pdf->validador = 'mike mate';//$nombre_validador->nombre;
        $pdf->fechaini = "Fecha de Inicio: "/*.$fecha_inicio*/;
        $pdf->fechafin = "Fecha de Fin: "/*.$fecha_fin*/;
        $pdf->fecha = "Fecha:  ".$fecha_hoy;
        //$pdf->AddFont('gothic','','gothic.php');
        //$pdf->AddFont('gothicb','B','gothicb.php');
        $pdf->AddPage('L','Letter',null,null);
        $pdf->Header2();
        /*
        $datos = $this->adminentidades_model->reportedetalladodocumentos($fecha_inicio,$fecha_fin,$idFuncionario);
       
        $num = 1;
        $pdf->SetFont('Arial', '', 7);
        $pdf->SetFillColor(0);
        $pdf->SetTextColor(0);
        if(!empty($datos))
        {
             foreach($datos as $valor)
             {
                        $s = array($num,
                            iconv('UTF-8', 'windows-1252', $valor->accion),
                            iconv('UTF-8', 'windows-1252', $valor->entidad) ,
                            iconv('UTF-8', 'windows-1252', $valor->rubro),
                            $valor->idbien,
                            $valor->iddocumento,
                            //$valor->adicionado,
                            iconv('UTF-8', 'windows-1252', $valor->documento),
                            $valor->nrodocumento,
                            $valor->fechadocumento);
                       $pdf->Row($s, true, '', 5);
                        $num++;
            }
       
        }
        */
        
        $pdf->xheader = 40;
        $pdf->yheader = 8;
        $pdf->cabecera = 3;
        /*$pdf->AddPage('L','Letter',null,null);
        $pdf->SetMargins(10,30,25);
        $pdf->SetAutoPageBreak(true,15);
        $pdf->SetY(30);//37
        $pdf->Ln(5);
        $pdf->SetFont('Times','B',16);
        $pdf->Cell(0,0,  utf8_decode("REPORTE DE VALIDACIÓN POR DÍA"),0,0,'C');
        $pdf->SetFont('Times','B',14);
        $pdf->Ln(2);
        $pdf->Ln(5);
        $nombre = $nombre_validador->nombre;
        $titulo1=  utf8_decode("Validador: ".$nombre); 
        $pdf->SetFont('Times','B',14);
        $pdf->Cell(0,0,$titulo1,0,0,'C');
        $fecha_hoy = $pdf->fechacompleta();
        $pdf->Ln(5);
        $pdf->Cell(0,0,"Fecha: ".$fecha_hoy,0,0,'C');
        $pdf->Ln(5);
        $pdf->WriteTable($columnas);*/
        
        

        $pdf->Output();
    }

   
}
?>