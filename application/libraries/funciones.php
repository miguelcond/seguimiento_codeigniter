<?php
/**
* 
*/
class Funciones
{
	
	function __construct()
	{
		$this->CI = & get_instance();
	}

	public function toSQL($textstring) {
		$textstring = trim($textstring);
   	 	if (get_magic_quotes_gpc()) $textstring = stripslashes($textstring);
    	//$value = mysql_real_escape_string($value);
		$carac= array(":", "  ", ";", "%", "$", "=", "|", "\n", "\r");
		$textstring = str_replace($carac, "", $textstring);
		$textstring = str_replace("'","''",$textstring);
		$textstring = str_replace(",","",$textstring);
		$textstring = str_replace("\"","",$textstring);
		$textstring = str_replace("\\","\\\\",$textstring);
		//$textstring = str_replace("/","//",$textstring);
		return ($textstring);
	}
	
	function valorAnterior($id,$tabla, $campo){
		/*
		if ($tabla == 'datos_proyecto.vencimiento_plazos' and $campo == 'estado')

		if ($tabla == 'datos_proyecto.vencimiento_plazos' and $campo == 'vigencia_plazo_ejecucion')	
		*/
		
		$query = $this->CI->db->query("select $campo from $tabla where id_proy= '$id'");
		if($query->num_rows() > 0) return $query->row()->$campo; 
		   else return '';
	}  
	public function bitacoragral($table, $operacion, $datos, $idus,$idregmod,$sql,$ip){
		$query = $this->CI->db->query("INSERT INTO seguridad.bitacorageneral(
            tabla, operacion, datos, idusuario, idregistromodif, sql, ip)
            VALUES ('$table', '$operacion', '$datos', '$idus', '$idregmod','$sql','$ip')");
	}
	public function getip(){
		$ip = "";
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else{
            $ip   = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
	public function fecha($f) {
		if ($f == 'NO CORRESPONDE') return $f;
		if($f == "") $new = "";
		  else{
			$v = explode("-", $f);
		    $new = $v[2]."/".$v[1]."/".$v[0];	
		  }
		
		return $new;
	}
	public function ampliacionesTotal($idproy){
		$data = array();
        $query = $this->CI->db->query("select * from datos_proyecto.plazos_ejecucion where id_proy= '$idproy'");
		if($query->num_rows() > 0) {
			$ampliacionesT = $query->row()->adenda_conv1 + $query->row()->adenda_conv2 + $query->row()->adenda_conv3 + $query->row()->adenda_conv4 + $query->row()->adenda_conv5 + $query->row()->ampliacion_resol_fdi;
			return $ampliacionesT;  
		}
		else {
			$data['error'] = "-  no existen datos";
		    return $data;
		} 
	}
	public function plazoEjecFinal($idproy){
		$data = array();
		$fecha_inic = '1970-01-01';
        $query = $this->CI->db->query("select * from datos_proyecto.plazos_ejecucion where id_proy= '$idproy'");
		if($query->num_rows() > 0) {
			$diasplazo = $query->row()->plazo_ejec_convenio + $this->ampliacionesTotal($idproy);
			if($query->row()->fecha_inicio != '') $fecha_inic = $query->row()->fecha_inicio;
			$plazoEjecF = date("Y-m-d",strtotime($fecha_inic."+ $diasplazo days")) ;
			return $plazoEjecF;  
		}
		else {
			$data['error'] = "-  no existen datos";
			return $data;	
		}

	}
	/*
	public function porcentajeEjecFinanciera($idproy){
		$data = array();
        $query = $this->CI->db->query("select * from datos_proyecto.plazos_ejecucion where id_proy= '$idproy'");
		if($query->num_rows() > 0) {
			$diasplazo = $query->row()->plazo_ejec_convenio + $this->ampliacionesTotal($idproy);
			$plazoEjecF = date("Y-m-d",strtotime($query->row()->fecha_inicio."+ $diasplazo days")) ;
			return $plazoEjecF;  
		}
		else {
			$data['error'] = "-  no existen datos";
			return $data;	
		}

	}
	*/
	public function desemAcumFdi($idproy){
		$data = array();
        $query = $this->CI->db->query("select desembolso_acumulado_fdi from datos_financiera.view_desembolso_acumulado_fdi where id_proy= '$idproy'");
		if($query->num_rows() > 0) {
			return $query->row()->desembolso_acumulado_fdi;  
		}
		else {
			$data['error'] = "-  no existen datos";
		    return $data;
		} 
	}
	public function saldoFdiF($idproy){
		$data = array();
        $query = $this->CI->db->query("select saldo_fdi from datos_financiera.view_saldo_fdi where id_proy= '$idproy'");
		if($query->num_rows() > 0) {
			return $query->row()->saldo_fdi;  
		}
		else {
			$data['error'] = "-  no existen datos";
		    return $data;
		} 
	}
	public function alertacolores($alert){
		if($alert == null) return $color  = '#ffff';
		if($alert < 0) return $color  = '#141212';
		if($alert >= 0 and $alert < 15) return $color  = '#f30606';
		if($alert >= 15 and $alert < 30) return $color  = '#f7ae03';
		if($alert > 30) return $color  = '#aee1a6';
		
	}
	public function nombrecolumn($campo){
		$query = $this->CI->db->query("select descripcion from datos_proyecto.nombre_campo where campo ilike('%$campo%')");
		/*
		$titulo = '';
		switch ($campo) {
			case 'cartera':
				$titulo = 'CARTERA';
				break;
			case 'nombre_proy':
				$titulo = 'MOBRE DEL PROYECTO';
				break;	
			case 'num_convenio':
				$titulo = 'Nro CONVENIO';
				break;	
			case 'costo_total':
				$titulo = 'COSTO TOTAL';
				break;		
			default:
				$titulo = 'no existe';
				break;
		}
		*/
		return $query->row()->descripcion;
	}

}
?>