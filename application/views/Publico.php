<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $page ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/images/logo.png') ?>"/>
<!--===============================================================================================-->
	
	
	<link href="<?php echo base_url('assets/css/main.css') ?>" rel="stylesheet">
<!--===============================================================================================-->
</head>
<!--<body style="background-image:url(<?php //echo base_url('assets/images/1920px-Aspersión_1.jpg')?>);">-->
<body >	

	<div class="bg-contact2">
		<div class="container-contact2">
				<div class="line-div">
					<div class="wrap-contact2 login-intro">
						<?= form_open('Publico/login', array('name'=>'form1', 'id'=>'form1', 'class'=>'forlog')); ?>
							
							<div class="fondoT">
								<img src="<?php echo base_url('assets/images/logo.png') ?>" alt="..." width="200" height="90" style="margin: 9px 78px;">
							</div>
							<div class="fondoT1" style="background-image:url(<?php echo base_url('assets/images/fondo.jpg')?>);">
									<div class="wrap-input2 validate-input" data-validate="Name is required">
										<input class="input2" type="text" name="usuario" id="usuario" value="<?= set_value('usuario')?>" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');"/>
										<span class="focus-input2 fa fa-user" data-placeholder="USUARIO"></span>
									</div>

									<div class="wrap-input2 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
										<input class="input2" type="password" name="password" id="password" value="<?= set_value('password')?>"  />
										<span class="focus-input2" data-placeholder="PASSWORD"></span>
									</div>
									
									<div class="container-contact2-form-btn">
										<input class="button-login" type="submit" name="entrar" id="entrar" value="INGRESAR" style="display:block;" />
									</div>
									<div style="background-color: #d9bd98; border-radius:5px;">
										<?= validation_errors('<div class="log-error">','</div>')?>
										<? if(isset($error)) echo "<label style = 'color: #ce0d0d;' for='label error'>".$error."</label>";?>
									</div>>
									
							</div>
							
							<?= form_close() ?>
					</div>
				</div>	
		</div>
	</div>

	
<center>
	<hr class="line-fdi">
	<h1 id="text3d">SISTEMA DE SEGUIMIENTO DE PROYECTOS</h1>
</center>

<?php //echo md5(123456)?>

<!--===============================================================================================-->
	<script src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
<!--===============================================================================================-->
	
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/js/main.js') ?>"></script>


</body>
</html>
<script type="text/javascript">
   $(document).ready(function(){
   	if($('#usuario').val().trim() != "") {
                $('#usuario').addClass('has-val');
            }
            else {
                $('#usuario').removeClass('has-val');
            }
     if($('#password').val().trim() != "") {
                $('#password').addClass('has-val');
            }
            else {
                $('#password').removeClass('has-val');
            }
    
   }); 

</script>