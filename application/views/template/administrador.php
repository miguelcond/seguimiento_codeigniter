<div class="col-md-3 left_col menu_fixed">
	<div class="left_col scroll-view">
 		<div class="navbar nav_title" style="border: 0;">
			<img src="<?php echo base_url('assets/images/logo.png') ?>" alt="..." width="140" height="60" style="float: right; margin: 9px 78px;">
		</div>

		<div class="clearfix"></div>
		<!-- menu profile quick info -->
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="<?php echo base_url('assets/images/img.jpg') ?>" alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info">
				<span>Bien Venido,</span>
				<h2><?= strtoupper($this->session->userdata('mailusu'))?></h2>
				<h2></h2>
			</div>
		</div>
		<!-- /menu profile quick info -->
		<br>
		<!-- Sidebar Menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>ADMINISTRADOR DEL SISTEMA</h3>
				<hr style="width: 68%; margin: 9px 23px 1px 12px; border-color: #7f858d;">
				<ul class="nav side-menu">
					<li><a href="<?php echo base_url('Administrador/') ?>"><i class="fa fa-home"></i> Inicio</a></li>
					<li><a href="<?php echo base_url('Administrador/Administrar') ?>"><i class="fa fa-users"></i> Administrar cuentas</a></li>
					
					<li><a><i class="fa fa-long-arrow-right fa-2x"></i> Asignar Proyectos <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url('Administrador/asigSeguimiento') ?>"><i class="fa fa-area-chart"></i>Seguimiento</a></li>
							<!--<li><a href="<?php echo base_url('Administrador/asigCierre') ?>"><i class="fa fa-lock"></i>Cierre</a></li>-->
						</ul>
					</li>
					<!--
					<li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="#level1">Menu 1</a></li>
							<li><a>Menu 2 <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="#level2_1">Menu 2.1</a></li>
									<li><a href="#level2_2">Menu 2.2</a></li>
									<li><a href="#level2_3">Menu 2.3</a></li>
								</ul>
							</li>
							<li><a href="#level3">Menu 3</a></li>
						</ul>
					</li>
					-->
					
				</ul>
			</div>
		</div>
		<!-- /Sidebar Menu -->

		<!-- menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Settings">
				<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="FullScreen">
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Lock">
				<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
				<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->
	</div>
</div>