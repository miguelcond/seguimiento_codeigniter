	<div class="col-md-3 left_col menu_fixed">

	<div class="left_col scroll-view">
 		<div class="navbar nav_title" style="border: 0;">
			<img src="<?php echo base_url('assets/images/logo.png') ?>" alt="..." width="140" height="60" style="float: right; margin: 9px 78px;">
			
		</div>

		<br><br><br><br>
		<div class="clearfix"></div>
		<!-- menu profile quick info -->
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="<?php echo base_url('assets/images/admin.png') ?>" alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info">
				<span>Bien Venido,</span><br>
				<h2><?= strtoupper($this->session->userdata('mailusu'))?></h2>
			</div>
		</div>
		<!-- /menu profile quick info -->
		<br>
		<!-- Sidebar Menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>(Usuario Técnico)</h3>
				<hr style="width: 68%; margin: 9px 23px 1px 12px; border-color: #7f858d;">
				<ul class="nav side-menu">
					<li><a href="<?php echo base_url('Proyecto/') ?>"><i class="fa fa-home"></i> Inicio</a></li>
					<li><a><i class="fa fa-list"></i> Listado de proyectos <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url('Proyecto/lista_proy') ?>">Listado/Registro de proyectos</a></li>
							<!--
							<li><a>Seguimiento <span class="fa fa-chevron-down"></span></a>
								 <ul class="nav child_menu">
									<li class="sub_menu"><a href="<?php //echo base_url('Proyecto/editar_proy') ?>">Borrador</a></li>
									<li class="sub_menu"><a href="<?php //echo base_url('Proyecto/editarPlazosEjec') ?>">Plazos Ejecusión</a></li>
									<li><a href="#level2_3">Menu 2.3</a></li>
								</ul>
							</li>
								
							<li><a>Cierre <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="#level2_1">Menu 2.1</a></li>
									<li><a href="#level2_2">Menu 2.2</a></li>
									<li><a href="#level2_3">Menu 2.3</a></li>
								</ul>
							</li>

						-->
						</ul>
					</li>
					<li><a><i class="fa fa-area-chart"></i> Edición Seguimiento <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url('Proyecto/editarAjaxDesembolsos') ?>"></i>Situacion Financiera</a></li>
							<li><a href="<?php echo base_url('Proyecto/editarAjaxPlazos') ?>"></i>Plazos Ejecusión y Estado del Proyecto</a></li><!--Plazos Ejecusión-->
							<li><a href="<?php echo base_url('Proyecto/editarAcuerdo') ?>"></i>Acuerdo Conciliatorio / Ejecución</a></li> <!--Vencimiento Plazos-->
							<li><a href="<?php echo base_url('Proyecto/editarSituaciones') ?>"></i> Situacion General del Proyecto / Inspecciones</a></li>
							<!--
							<li><a>Menu 2 <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="#level2_1">Menu 2.1</a></li>
									<li><a href="#level2_2">Menu 2.2</a></li>
									<li><a href="#level2_3">Menu 2.3</a></li>
								</ul>
							</li>
						  -->
						</ul>
					</li>
					<!--
					<li><a><i class="fa fa-lock"></i> Cierre <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="#level1">Menu 1</a></li>
							<li><a>Menu 2 <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="#level2_1">Menu 2.1</a></li>
									<li><a href="#level2_2">Menu 2.2</a></li>
									<li><a href="#level2_3">Menu 2.3</a></li>
								</ul>
							</li>
							<li><a href="#level3">Menu 3</a></li>
						</ul>
					</li>
						
					<li><a><i class="fa fa-newspaper-o"></i> Reportes <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php //echo base_url('Reportes/') ?>">Reportes generales</a></li>
						</ul>
					</li>-->
				
					
				</ul>
			</div>
		</div>
		<!-- /Sidebar Menu -->

		<!-- menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Settings">
				<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="FullScreen">
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Lock">
				<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url('Proyecto/cerrarses')?>">
				<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->
	</div>

</div>