<!-- Top Nav -->
<div class="top_nav">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle"><a id="menu_toggle"><i class="fa fa-bars"></i></a>
				<!--<center><img src="<?php echo base_url('assets/images/logo.png') ?>" alt="..." class="profile_img" width="140" height="60"></center>-->
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><img src="<?php echo base_url('assets/images/admin.png') ?>" alt=""><?= $this->session->userdata('usuario')?><span class="fa fa-angle-down"></span></a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">



						<li>
							 <a href="#" data-target=".bs-example-modal-lg" data-toggle="modal" id="modificarPass" name="modificarPass"><i class="fa fa-unlock-alt"></i> Cambiar contraseña</a>
						</li>
						<!--
						<li><a href="javascript:;">
							<span class="badge bg-red pull-right">50%</span>
							<span>Settings</span></a>
						</li>
					    -->
						<li><a href="javascript:;"><i class="fa fa-lightbulb-o"></i> Ayuda</a></li>
						<li><?= anchor('Publico/cerrarses',' Cerrar session','class="fa fa-power-off"')?></li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</div>
<!-- /Top Nav -->

<!--inicio modal 2-->
<div class="modal fade" data-backdrop="static" id="modPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
	      <div class="modal-header">
	                
	      </div>
			      <div class="modal-body">
			      		<!-- Basic setup -->
					      <div class="panel panel-white" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
					            
					            <div  class="alert alert-danger"  id="validacionpermiso" style="display: none;">
					                
					            </div>
					        
					       		<div class="modal-header bg-info">
					       			<!--falta aplicar la clase resetform para limpiar el formulrio-->
					                <button type="button" class="close resetformPass" data-dismiss="modal">&times;</button>
					                <h5 class="modal-title">MODIFICAR CONTRASEÑA</h5>
					            </div>     

					       			<div style="padding: 20px; margin: 0px 0px -37px 0px;">  
			<!--cuerpo modal-->
									<div class="row">
						<div class="col-md-12 col-sm-12 ">
							<div class="x_panel">
								<div class="x_title">
									<h2>MOD  <small>CONTRASEÑA</small></h2>
									<ul class="nav navbar-right panel_toolbox">
										<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
											<ul class="dropdown-menu" role="menu">
												<li><a class="dropdown-item" href="#">Settings 1</a>
												</li>
												<li><a class="dropdown-item" href="#">Settings 2</a>
												</li>
											</ul>
										</li>
										
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="">
									<br />
									<form id="formPass" data-parsley-validate class="form-horizontal form-label-left">
										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">ACTUAL CONTRASEÑA<span class="required">*</span>
											</label>
											<div class="col-md-6">
												<button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button><input type="password" id="contrasena" name="contrasena" required="required" class="form-control" style="width: 88%; display: inline; vertical-align: top;">

											</div>
										</div>
										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">NUEVA CONTRASEÑA<span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												<button id="show_password2" class="btn btn-primary" type="button" onclick="mostrarPassword1()"> <span class="fa fa-eye-slash icon1"></span> </button><input type="password" id="contrasenaNueva" name="contrasenaNueva" required="required" class="form-control" style="width: 88%; display: inline; vertical-align: top;">
											</div>
										</div>
										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">CONFIRMAR NUEVA CONTRASEÑA<span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												<button id="show_password3" class="btn btn-primary" type="button" onclick="mostrarPassword2()"> <span class="fa fa-eye-slash icon2"></span> </button><input type="password" id="contrasenaConfirm" name="contrasenaConfirm" required="required" class="form-control" style="width: 88%; display: inline; vertical-align: top;">
											</div>
										</div>
														
										<div class="ln_solid"></div>
										

									</form>
								</div>
							</div>
						</div>
					</div>






			<!--fin cuerpo modal-->
											<div class="modal-footer">
								            	<button type="button" class="btn btn-danger resetformPass" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
								             <!--falta apñicar la class resetform para limpiar el formulario--> 
								             	<button id="guardarPass" type="button" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                
								            </div>
							         </div>



						  </div>
			                <!-- /basic setup -->
			      </div>
    </div>
  </div>
</div>
<!--fin modal2-->