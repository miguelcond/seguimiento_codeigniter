<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">    
            <button type="button" class="close" data-dismiss="modal" onclick="cancel_mueble()">&times;</button> 
            <h4 class="modal-title" id="largeModalHead"><span id="accion_form_mueble">Registrar</span> Mueble</h4>            
        </div>
        <div class="modal-body">
            <br />
            <br />
            <div class="stepwizard">
                <form action="#" id="formueble" role="form" data-toggle="validator" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    <!-- SmartWizard html -->                    
                    <div id="smartwizard2" class="myForm_mueble">
                        <ul>
                            <li><a href="#step-1_proy">Paso 1<br /><span>Información del bien</span></a></li>
                            <li><a href="#step-2_proy">Paso 2<br />
                            <span>Ubicación del Bien</span></a></li>
                            <li><a href="#step-3_proy">Paso 3<br />
                            <span>Fotografía</span></a></li>
                        </ul>

                        <div>
                            <div id="step-1_proy" class="" style="height:360px;">
                                 <input type="hidden" name="validador" id="validador">
                                 <input type="hidden" name="id_bien_mueble" id="id_bien_mueble">
                            <input type="hidden" name="id_entidad_mueble" id="id_entidad_mueble" value="{{entidad.id_entidad}}">
                            
                            <div class="form-group">
                                
                                <div class="col-xs-3">
                                    <span class="control-label">Tipo Mueble <span class="text-danger">*</span></span>
                                    <select class="form-control" id="tipo_mueble" name="tipo_mueble">
                                    
                                    </select>
                                </div>
                                <div class="col-xs-3">
                                    <span class="control-label">Estado <span class="text-danger">*</span></span>
                                    <select class="form-control" name="estado_mueble" id="estado_mueble">
                                        <option value="">Seleccionar</option>
                                        <option value="1">Bueno</option>
                                        <option value="2">Malo</option>
                                        <option value="3">Regular</option>
                                        
                                    </select>
                                </div>
                                <div class="col-xs-3">
                                    <span class="control-label">Cantidad <span class="text-danger">*</span></span>
                                    <input type="text" class="form-control" name="cantidad_mueble" id="cantidad_mueble"/>
                                </div>
                                
                                 <div class="form-group">
                                <div class="col-xs-3">
                                    <span class="control-label">Descripción <span class="text-danger">*</span></span>
                                    <textarea name="descripcion_mueble" id="descripcion_mueble" class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                                
                                                            
                                <div class="col-xs-3">
                                    <span class="control-label">Código actual <span class="text-danger">*</span></span>
                                    <input type="text" class="form-control" name="codactual_mueble" id="codactual_mueble"/>
                                </div>
                            <div class="form-group">
                                <div class="col-xs-3">
                                    <span class="control-label">Códigos Anteriores</span>
                                    <textarea name="codanterior_mueble" id="codanterior_mueble" class="form-control" rows="2"></textarea>
                                	<span class="control-label">Nota: Debe separar los códigos con una coma ","</span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                            
                                <div class="col-xs-12">
                                <legend><span class="fa fa-comment"></span>Observaciones</legend>
                                    <textarea name="observaciones_mueble" id="observaciones_mueble" class="form-control" rows="5"></textarea>
                                    
                                </div>

                            </div>
							</div>
                          </div>
                            <div id="step-2_proy" class="">
                                <div class="col-xs-12">
                                    <legend><span class="fa fa-map-marker"></span> Ubicación del Bien</legend>
                                    <div class="row">
                                        
                                        <div class="col-xs-3">
                                            <label class="control-label">Departamento <span class="text-danger">*</span></label>
                                            <select class="form-control" id="cbdepartamento_mueble" name="cbdepartamento_mueble">

                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="control-label">Provincia <span class="text-danger">*</span></label>
                                            <select class="form-control" id="cbprovincia_mueble" name="cbprovincia_mueble">

                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="control-label">Municipio <span class="text-danger">*</span></label>
                                            <select class="form-control" id="cbmunicipio_mueble" name="cbmunicipio_mueble">
                                            </select>
                                        </div>
                                    </div>
                                  
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <span class="control-label">Dirección</span>
                                            <input type="text" class="form-control" name="direccion_mueble" id="direccion_mueble" />
                                        </div>
                                        
                                    </div><hr>
                                   
                                </div>
                            </div>
                            
                            <div id="step-3_proy" class="">                                
                                <div class="content-frame-body content-frame-body-left">
                                    <div class="gallery mueble">
                                      {% for i in 1..10 %}
                                        <div class="gallery-item">
                                            <a  href="" title="" id="hrefmueble{{i}}" data-gallery="#blueimp-gallery-muebles">
                                                <div class="image dropzone dropzone-mini  dz-clickable" id="divImgmueble{{i}}">
                                                  <img src="#" id="imgmueble{{i}}" class="dz-message" alt=""/>
                                                  <ul class="gallery-item-controls">
                                                        <li><h3 class="badge">{{i}}</h3></li>
                                                        <!--<li><label class="check"><input type="checkbox" name="mostrarimgmueble_{{i}}" id="mostrarimgmueble_{{i}}" class="icheckbox"/></label></li>-->
                                                        <li><span class="gallery-item-remove limpiar-inputfile" id="eliminarimgmueble_{{i}}"><i class="fa fa-times"></i></span></li>
                                                    </ul>
                                                </div>
                                            </a>
                                            <div class="meta">
                                                <input type="text" name="descimgmueble_{{i}}" id="descimgmueble_{{i}}" class="form-control" placeholder="Descripción"/>
                                                <input type="hidden" name="nombreimgmueble_{{i}}" id="nombreimgmueble_{{i}}" class="form-control"/>
                                                <input type="hidden" name="selecimgmueble_{{i}}" id="selecimgmueble_{{i}}" class="form-control"/>
                                            </div>
                                            <div class="col-md-12">
                                                
                                                
                                                <!--para mostrar la imagen-->
                                                <input type="file" name="fileimgmueble_{{i}}" id="fileimgmueble_{{i}}" class="keyBlock" style="visibility:hidden;display: none;" onchange="document.getElementById('imgmueble{{i}}').src = window.URL.createObjectURL(this.files[0]);document.getElementById('hrefmueble{{i}}').href = window.URL.createObjectURL(this.files[0]);">
                                                
                                                <button class="btn btn-info" onclick="cargarFilemueble({{i}},event)"><span class="fa fa-upload"></span>Cargar Imagen</button>
                                               
                                            </div>
                                        </div>
                                        {% endfor %}
                                      
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div style="clear:both;"></div>        
        </div>
        <div class="modal-footer">
            <div style="clear:both;"></div>
        </div>
    </div>
</div>
<script>
$(function () {
        
        $(".limpiar-inputfile").click(function () {
            var oID = $(this).attr("id");
			//alert("entro:"+oID);
			
			var lista 			= oID.split("_");//split, explode
		
		
		$('#selecimgmueble_'+lista[1]).val(oID);
		//var idlista			= lista[1];
			//alert('valor de this id:'+lista[1]);
			
			
        });
});

function cargarFilemueble(nImg,event){
        event.preventDefault();
        $('#fileimgmueble_'+nImg).click();
		$('#selecimgmueble_'+nImg).val('');
    }

function eliminarimgmueble(i,data){
    
	//alert("eliminira"+data);
	$('#nombreimgmueble_'+i).val(data);
	
	
}


</script>

<script type="text/javascript">

jQuery.validator.addMethod("formato", function(value,  element) {
return this.optional(element) || /^([a-z]|\d|[ ]|\,)+$/i.test(value);
}, "Error en formato de letras");

jQuery.validator.addMethod("numero", function(value,  element) {
return this.optional(element) || /^([0-9])*$/.test(value);
}, "Debes introducir solo numeros");


jQuery.validator.addMethod("codrepetido", function() {
var result;
if($('#validador').val() == $('#codactual_mueble').val()){//entra solo cuando se introduce un codigo diferente al que esta en la bd
    result = true;
   
} else{

            var cod = $('#codactual_mueble').val();
             $.ajax({
                    url: "{{ url("muebles/validacod")}}",
                    async: false,
                    method: 'POST',
                    data: {codmueble: cod},
                   
                    success: function(data) {
                         if(data == 1){
                         
                         result = false;
                         //$('#codactual_mueble').val('');
                         } else {
                            
                            result = true;
                         }
                         
                             
                    }            
            }); 
  }           
return result;
}, "Este codigo ya ha sido registrado !!");





$(document).ready(function(){  
    valido_mueble = 0;
    contador = 1;
    var validatormueble = $("#formueble").validate({
        rules: {
            cantidad_mueble: {
                required: true,
                numero: true,
                minlength: 1,
                maxlength: 3
            },
            tipo_mueble: {
                required: true
            },
            
			estado_mueble: { 
                required: true
            },
			descripcion_mueble: {
                required: true
                
            },
            tipotitulo_mueble: {
                required: true
            },
            codactual_mueble: {
                required: true,
                codrepetido: true
            },
            
            cbpais_mueble: {
                required: true
            },
            cbdepartamento_mueble: {
                required: true
            },
            cbprovincia_mueble: {
                required: true
            },
            cbmunicipio_mueble: {
                required: true
            },
            cbmunicipio_mueble: {
                required: true
            }
        },


    });
	
	
    $('#smartwizard2').smartWizard({
        selected: 0,  
        keyNavigation:false, 
        autoAdjustHeight:true, 
        cycleSteps: false, 
        backButtonSupport: true, 
        useURLhash: true, 
        lang: {  
            next: 'Siguiente', 
            previous: 'Anterior'
        },
        toolbarSettings: {
            toolbarPosition: 'bottom', 
            toolbarButtonPosition: 'right', 
            showNextButton: true, 
            showPreviousButton: true, 
            toolbarExtraButtons: [
           $('<button></button>').text('Guardar')
                    .addClass('btn btn-info hide')
                    .on('click', function(){
                
                        
                //inicio para que valide el wisard en caso de editar, solo valida cuando se hace siguiente por eso es necesario volveer a validar desde el principio el formulario
                if($('#accion_form_mueble').text() == 'Editar'){
                    contador = 3;
                    var valid = true;
                    valid = $("#formueble").valid() && valid; //valida el formulario
                     
                     if(!valid){
                        wizard.find(".stepContainer").removeAttr("style");
                        validatormueble.focusInvalid();
                        valido_mueble = 0;
                        
                    }else {
                        valido_mueble = 1;
                    }
                    
                }
                //fin valida en caso de editar
                        //contador++;
                        //alert(valido_mueble);
                        //alert('contador:'+contador);
                        if(contador < 3) {
                        alert("Falta introducir datos al formulario..");
                        return false;  
                        event.preventDefault();
                        
                        }
                        if(valido_mueble == 1 && contador >= 3){
                            guardar_mueble();
                            return false;
                        }
                console.log('sdasd');

                    }),
                ],                
        },             
        theme: 'arrows',
        transitionEffect: 'fade', 
        transitionSpeed: '400'
    });
    
    $("#smartwizard2").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
        //wizard para que vaya a siguiente wisard, empieza a validar cuando se va  al siguiente
		var wizard = $('#smartwizard2');
        //alert(wizard.hasClass("myForm_mueble"));
        if(wizard.hasClass("myForm_mueble")){
            

            var valid = true;
			//con each recorremos todos los tags
			 $('input,textarea,select',$(anchorObject.attr("href"))).each(function(i,v){
                valid = validatormueble.element(v) && valid;
                //valid es verdadero si cumple validacion en cada iteracion
				//alert("valor de valid:"+valid);
				valido_mueble = 1;
                
								
            });
			 //si alguna condicion no se cumpke entra 
            if(!valid){
                wizard.find(".stepContainer").removeAttr("style");
                validatormueble.focusInvalid();
                valido_mueble = 0;
                
				//alert("entro por falso");
				return false;
            }
            else{
                contador++;
                if(contador >= 3) $('.btn-info').removeClass('hide');
            }
        }
		
        return valid;
    });
	
	//fin validacion del form
    
    
	$.ajax({//
            url: "{{ url("muebles/getipomueble")}}",
            method: 'POST',
            success: function(data) {
                $('#tipo_mueble').html(data);  
            }
            
    });   
	$.ajax({//
            url: "{{ url("muebles/getpais")}}",
            method: 'POST',
            success: function(data) {
                $('#cbpais_mueble').html(data);  
            }
            
    });    

    
     $.ajax({//
            url: "{{ url("muebles/getdepartamento")}}",
            method: 'POST',
            data: {pais: 'BO'},
            success: function(data) {
                $('#cbdepartamento_mueble').html(data);  
            }
            
    });   

    $('#cbpais_mueble').change(function() {
       var valor = $('#cbpais_mueble').val();
         $.ajax({
            url: "{{ url("muebles/getdepartamento")}}",
            method: 'POST',
            data: {pais: valor},
            success: function(data) {
                $('#cbdepartamento_mueble').html(data);  
            }
            
         }); 
       
    });   
    $('#cbdepartamento_mueble').change(function() {
       var valor = $('#cbdepartamento_mueble').val();
         $.ajax({
            url: "{{ url("muebles/getprovincia")}}",
            method: 'POST',
            data: {departamento: valor},
            success: function(data) {
              
                $('#cbprovincia_mueble').html(data);  
            }
            
         }); 
       
    }); 
    $('#cbprovincia_mueble').change(function() {
       var valor = $('#cbprovincia_mueble').val();
         $.ajax({
            url: "{{ url("muebles/getmunicipio")}}",
            method: 'POST',
            data: {provincia: valor},
            success: function(data) {
              
                $('#cbmunicipio_mueble').html(data);  
            }
            
         }); 
       
    }); 
    $('#cbmunicipio_mueble').change(function() {
       var valor = $('#cbmunicipio_mueble').val();
         $.ajax({
            url: "{{ url("muebles/getlocalidad")}}",
            method: 'POST',
            data: {municipio: valor},
            success: function(data) 
            {
                $('#cblocalidad_mueble').html(data);  
            }
            
         }); 
       
    }); 
});
</script>
<script type="text/javascript">    
    function guardar_mueble(){ 
        //alert ("guardar");
		var parametro = new FormData($('#formueble')[0]);
        if($('#accion_form_mueble').text() == 'Registrar'){
            $.ajax({
                url: "{{ url("muebles/registrar")}}",
                data: parametro,
				cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function(data) {
                    //alert(data);
                    location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                },
                error: function() {
                    location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                }
            });
        }  
	if($('#accion_form_mueble').text() == 'Editar'){
            //$('#accion_form_mueble').text('Registrar');
			$.ajax({
                url: "{{ url("muebles/editar")}}",
                data: parametro,
				cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function(data) {                
                    //alert(data);
                    console.log(data);
                    location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                },
                error: function() {
                    location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                }
            });
        }

        $('#modal_large_mueble').modal('toggle'); 
    }
    function editarmueble(id){ 
        $.ajax({ 
            url: "{{ url("muebles/editar/")}}"+id, 
            success: function(data) 
            {
                
				$('#accion_form_mueble').text('Editar'); 
                $('#validador').val(data.mueble.codigo_actual);
				$('#id_bien_mueble').val(data.bien.id_bien);
				$('.btn-info').removeClass('hide');//para habilitar de nuevo el boton guardarne al omendo de editar
				//$('#latitud_mueble').val(data.mueble.latitud);
                //$('#longitud_mueble').val(data.mueble.longitud);
				$('#descripcion_mueble').val(data.mueble.descripcion);
				$('#cantidad_mueble').val(data.mueble.cantidad);
				$('#codactual_mueble').val(data.mueble.codigo_actual);
				$('#codanterior_mueble').val(data.codanterior);
                
				$('#observaciones_mueble').val(data.mueble.observaciones);
				$('#tipo_mueble').val(data.mueble.id_tipomueble);
				$('#estado_mueble').val(data.mueble.id_estadomueble);
				//$('#tipo_mueble > option[value="'+data.mueble.id_tipomueble+'"]').attr('selected', 'selected');
				//$('#estado_mueble > option[value="'+data.mueble.id_estadomueble+'"]').attr('selected', 'selected');
				
                $('#distrito_mueble').val(data.mueble.distrito);
				$('#comunidad_mueble').val(data.mueble.comunidad);
				$('#zona_mueble').val(data.mueble.zona);
				$('#direccion_mueble').val(data.mueble.direccion);
				
				$('#cbpais_mueble').val(data.bien.pais);
				getdepartamento(data.bien.id_departamento);
                getprovinciamueble(data.bien.id_departamento,data.bien.id_provincia);
                getmunicipiomueble(data.bien.id_provincia,data.bien.id_municipio);
                getlocalidadmueble(data.bien.id_municipio,data.bien.id_localidad);
				
			for(foto of data.fotos){
                    for (var i = 1; i <= 10; i++){
                        if(foto.orden_fotos == i){
                            $('#imgmueble'+i).attr('src', '{{ url('uploads/images/')}}'+foto.ubicacion);
                            $('#hrefmueble'+i).attr("href", '{{ url('uploads/images/')}}'+foto.ubicacion);//muestra imagen en grande
                            $('#descimgmueble_'+i).val(foto.descripcion_fotografia);
                            //$('#mostrarimgmueble_'+i).parent().addClass('checked');  
                            $('#eliminarimgmueble_'+i).attr('onclick','eliminarimgmueble('+i+',"'+foto.ubicacion+'")');	
								
                        }
                    }                  
                }      
				
            }
        });

    }
	
	function duplicarmueble(id){ 
        $.ajax({ 
            url: "{{ url("muebles/editar/")}}"+id, 
            success: function(data) 
            {
                
				$('#accion_form_mueble').text('Registrar');  
				$('#id_bien_mueble').val(data.bien.id_bien);
				
				//$('#latitud_mueble').val(data.mueble.latitud);
                //$('#longitud_mueble').val(data.mueble.longitud);
				$('#descripcion_mueble').val(data.mueble.descripcion);
				$('#cantidad_mueble').val(data.mueble.cantidad);
				//$('#codactual_mueble').val(data.mueble.codigo_actual);
				//$('#codanterior_mueble').val(data.codanterior);
                
				//$('#observaciones_mueble').val(data.mueble.observaciones);
				$('#tipo_mueble').val(data.mueble.id_tipomueble);
				$('#estado_mueble').val(data.mueble.id_estadomueble);
				$('#distrito_mueble').val(data.mueble.distrito);
				$('#comunidad_mueble').val(data.mueble.comunidad);
				$('#zona_mueble').val(data.mueble.zona);
				$('#direccion_mueble').val(data.mueble.direccion);
				
				$('#cbpais_mueble').val(data.bien.pais);
				getdepartamento(data.bien.id_departamento);
                getprovinciamueble(data.bien.id_departamento,data.bien.id_provincia);
                getmunicipiomueble(data.bien.id_provincia,data.bien.id_municipio);
                getlocalidadmueble(data.bien.id_municipio,data.bien.id_localidad);
				
				
				  
				
            }
        });

    }
	
	
   
    function getdepartamento(id)
    {
         var valor = $('#cbpais_mueble').val();
         $.ajax({
            url: "{{ url("muebles/getdepartamento")}}",
            method: 'POST',
            data: {pais: valor, x:id},
            success: function(data) {
                $('#cbdepartamento_mueble').html(data);  
            }            
         }); 
    }
    function getprovinciamueble(valor,idx)
    {        
         $.ajax({
            url: "{{ url("muebles/getprovincia")}}",
            method: 'POST',
            data: {departamento:valor,x:idx},
            success: function(data) {
               $('#cbprovincia_mueble').html(data);  
            }            
         }); 
    }
    function getmunicipiomueble(valor,idx)
    {        
         $.ajax({
            url: "{{ url("muebles/getmunicipio")}}",
            method: 'POST',
            data: {provincia: valor,x:idx},
            success: function(data) {              
                $('#cbmunicipio_mueble').html(data);  
            }            
         }); 
    }
    function getlocalidadmueble(valor,idx)
    {        
        $.ajax({
            url: "{{ url("muebles/getlocalidad")}}",
            method: 'POST',
            data: {municipio: valor,x:idx},
            success: function(data) {
                $('#cblocalidad_mueble').html(data); 
            }            
         }); 
    }
	function cancel_mueble(){
        valido_mueble = 0;
        contador = 1;
        $('#accion_form_mueble').text('Registrar');
		$('#smartwizard2').smartWizard('reset');
        $('#formueble')[0].reset();
        $("select").val(['']);
        $('.form-control').removeClass('error');
        $('.form-control').removeClass('required');                
        $('.error').remove();

        for (var i = 1; i <= 10; i++){
            $('#imgmueble'+i).attr('src', '');
            $('#hrefmueble'+i).attr("hrefmueble", '');
            $('#descimginmueble_'+i).val('');
            //$('#mostrarimgmueble_'+i).parent().removeClass('checked');  
            $('#eliminarimginmueble_'+i).removeAttr('onclick');
        }
    }

$(document).ready(function() {

   $('input').keypress(function(e){
    if(e.which == 13){
      return false;
    }
  });



});


</script>




