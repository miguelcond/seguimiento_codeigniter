<!-- First Section -->
<script type="text/javascript" src="<?php echo  base_url() ?>jdseg/grafJs/html2pdf.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo  base_url() ?>jdseg/grafJs/scriptAdmin.js"></script>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="dashboard_graph">

			<div class="row x_title">
				<div class="col-md-6">
					
			                        <ul class="fa-ul">
			                          <li>
			                            <i class="fa fa-info-circle fa-lg fa-li"></i><h2 ><code>DEPARTAMENTO:</code> <label id="deptChar"></label></h2>
			                            
			                          </li>
			                        </ul>
			                     
				</div>

				<div class="col-md-6">
					<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
						<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
						<span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-6" style="background-color: #2a3f54; border-radius: 5px; height: 58px; color:ghostwhite ;">
				<h2>Filtrado por departamentos</h2>
				<? if($deptos != FALSE) :?>
						<select class="form-control" id="selecCartAdmin">
							<option value="">selecciona...</option>
							<? foreach($deptos as $fila) :?>
							<option value="<?=$fila->departamento?>"><?=$fila->departamento?></option>
							<? endforeach;?>
						</select>
				<? else :?><h3>Antes de seguir con este paso debe registrar departamentos</h3>
				<? endif;?>	
			</div>
			<br><br><br><br><br>
<div id="graficoBarraAdmin">
			<div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel" style="overflow-x: scroll;">
                  <div class="x_title">
                    <h2> Carteras por departamento</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                             <a id="btnCrearPdfbarraAdmin" class="dropdown-item" href="#">generar pdf</a>
                             </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<div id="echart_bar_horizontal2" style="height:450px; width: 550px;"></div>


	                  	<table class="table table-hover" id="tabletest">
                        <thead>
                            <tr>
                                <th>Departamento</th>
                                <th>Cartera I</th>
                                <th>Cartera II</th>
                            </tr>
                        </thead>
                        <tbody id="fila_carterasAdmin">
                            
                        </tbody>
                        
                    </table>
                  	
                    

                  </div>
                </div>
      </div>
</div>


<div id="graficoTortaAdmin">
			<div class="col-md-4 col-sm-4 col-xs-12">
				 <div class="x_panel" style="overflow-x: scroll;">
                  <div class="x_title">
                    <h2>Tipo de Proyectos</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a id="btnCrearPdfTortaAdmin" class="dropdown-item" href="#">generar pdf</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  
                     <div class="x_content">
                     		<div id="echart_pie1" style="height:390px; width: 470px;"></div>
                     		<br><br>
                     		<table class="table table-hover" id="tabletest">
		                        <thead>
		                            <tr>
		                                <th>Departamento</th>
		                                <th>MAQUINARIA</th>
		                                <th>PRODUCTIVO</th>
		                                <th>PUENTE</th>
		                                <th>RIEGO</th>
		                            </tr>
		                        </thead>
		                        <tbody id="filaTipoProyAdmin">
		                            
		                        </tbody>
		                        
		                    </table>
                     		<!--
                     		<table class="margenTabla table table-bordered" style="color: lightyellow; text-align: center; width: 470px;">
			                         
			                          <tr>
			                            <td style="background-color:#26b99a;">
			                              <p id="maqGraf">-</p>
			                            </td>
			                            <td style="background-color:#34495e;">
			                              <p id="prodGraf">-</p>
			                            </td>
			                            <td style="background-color:#adb4b9;">
			                              <p id="puentGraf">-</p>
			                            </td>
			                            <td style="background-color:#3498db;">
			                              <p id="riegGraf">-</p>
			                            </td>
			                            
			                          </tr>
			                  </table>
			                -->        
			         </div>
                   

                   
                 
           </div>
				<!--<div id="chart_plot_01" class="demo-placeholder"></div>2022-->
			</div> 
</div>			


			<div class="clearfix"></div><!--inicio-->
			

			<div class="col-md-4 col-sm-4"><!--fin-->
               
           
      </div>



		</div>
	</div>

</div>
<!-- /First Section -->
<br>
<!-- Second Section -->
<div class="row">
	

</div>
<!-- /Second Section -->
<!-- Third Section -->

<div class="row">
	

</div>
<!-- /Third Section -->
