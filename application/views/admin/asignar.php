 <!-- top tiles -->
 <?php
$porcentajeasignados=round(100*$cantidadAsignados->numero/$totalProyectos->numero,2);
$porcentajeporasignar=round(100*$cantidadPorAsignar->numero/$totalProyectos->numero,2);
$porcentajeAsigusuario=round(100*$cantidadAsignUsuaro->numero/$totalProyectos->numero,2);

 ?>
         
 					<form id="recargarUsuario" action="<?= base_url('Administrador/asigProy') ?>" method="post">
 						<input type="hidden" id="idUs" name="idUs" value="<?= $datosUs->id_usuario?>">
 					</form>
 					<a href="<?php echo base_url('Administrador/asigSeguimiento') ?>"><i style="color: chartreuse;" class="fa fa-share fa-2x"></i><strong>Volver</strong></a><br>
 					<a href="#/share"> fa-share</a>
          <div class="row" style="display: inline-block;" >
          <div class="tile_count">
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width: 212px;">
              <span class="count_top"><i class="fa fa-table"></i> Total proyectos</span>
              <div class="count"><?=$totalProyectos->numero?></div>
              <span class="count_bottom"><i class="green">100% </i></span>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width: 212px;">
              <span class="count_top"><i class="fa fa-user"></i> Proyectos asignados</span>
              <div class="count"><?=$cantidadAsignados->numero?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i><?=$porcentajeasignados?>% </i></span>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width: 212px;">
              <span class="count_top"><i class="fa fa-clock-o"></i> Proyectos por asignar</span>
              <div class="count"><?=$cantidadPorAsignar->numero?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i><?=$porcentajeporasignar?>% </i></span>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width: 300px;">
              <span class="count_top"><i class="fa fa-user"></i> Asignados a: <h7 class="green"><?= $datosUs->nombre.' '.$datosUs->apellido_pat.' '.$datosUs->apellido_mat?></h7></span>
              <div class="count green"><?=$cantidadAsignUsuaro->numero?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i><?=$porcentajeAsigusuario?>% </i></span>
            </div>
           <i class="tile_stats_count"></i>
          </div>
        </div>
          <!-- /top tiles -->

<div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Proyectos por asignar</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Proyectos Asignados / desasignar</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade" id="tab_content1" aria-labelledby="home-tab">

          <!-- start recent activity -->
                            <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Proyectos asignados a: <i class="green"><?= $datosUs->nombre.' '.$datosUs->apellido_pat.' '.$datosUs->apellido_mat?></i></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
             
			<div class="x_content">
				


				<table id="datatable" class="table table-striped table-bordered dataTable no-footer tableEight" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nro</th>
							<th width="30%">PROYECTO</th>
							<th>USUARIO ASIGNADO</th>
							<th>FECHA ASIGNACION</th>
							<th>OPCIONES</th>
					    </tr>
					</thead>
					<tbody>
						 <? 
						 $cont=1;
						 foreach($asignadosUs as $asignado):?>
						 	<tr>
			                      <td><?= $cont; $cont++;?></td>
			                      <td><?= $asignado->nombre_proy?></td>
			                      <td><?= $asignado->nombre.' '.$asignado->apellido_pat.' '.$asignado->apellido_mat?></td>
			                      <td><?= $asignado->fecha_asign?></td>
			                      <td>
			                      	<a data-title="Desasignar Proyecto" href="#" class="btn btn-danger" onclick="desasignar(<?= $asignado->id_asig?>)" data-toggle="modal"><i class="fa fa-arrow-circle-left fa-1x"></i> Desasignar</a>
			                      </td>
			                      
                            </tr>
			                <?endforeach?>
					</tbody>
				</table>


			</div>
		</div>
	</div>
</div>
                            <!-- end recent activity -->

                          </div>
                          <div role="tabpanel" class="tab-pane active" id="tab_content2" aria-labelledby="profile-tab">

                            <!-- start user projects -->
                           <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>proyectos por asignar a: <small><?= $datosUs->nombre.' '.$datosUs->apellido_pat.' '.$datosUs->apellido_mat?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
             
			<div class="x_content">
				




				<table id="datatable-fixed-header" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nro</th>
							<th>NOMBRE PROYECTO</th>
							<th>DEPARTAMENTO</th>
							<th>PROVINCIA</th>
							<th>MUNICIPIO</th>
							<th>TIPO PROY</th>
              <th>CARTERA</th>
              <th>OPCIONES</th>
					    </tr>
					</thead>
					<tbody>
						 <? 
						 $cont=1;
						 foreach($porAsignar as $asignar):?>
						 	<tr>
			                      <td><?= $cont; $cont++;?></td>
			                      <td><?= $asignar->nombre_proy?></td>
			                      <td><?= $asignar->departamento?></td>
			                      <td><?= $asignar->provincia?></td>
			                      <td><?= $asignar->municipio?></td>
			                      <td><?= $asignar->tipo_proy?></td>
			                      <td><?= $asignar->cartera?></td>
			                      <td>
			                      	<a data-title="Asignar Proyecto" href="#" class="btn btn-success" onclick="asignProyectoUs(<?= $asignar->id?>,<?= $datosUs->id_usuario?>)" data-toggle="modal"><i class="fa fa-arrow-circle-right fa-1x"></i> Asignar</a>
								  </td>
			                      
                            </tr>
			                <?endforeach?>
					</tbody>
				</table>


			</div>
		</div>
	</div>
</div>
                            <!-- end user projects -->

                          </div>
                          
                        </div>
 </div>


<script>
if (window.performance.navigation.type == 1) {
   if(confirm('Desea Actualizar ? ')){
     ("#recargarUsuario").submit();

   }
else{
    alert('Correcto');
  }
}	
</script>