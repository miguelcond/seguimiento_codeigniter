<link href="<?php echo base_url('assets/css/check.css') ?>" rel="stylesheet">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Asignacion de Proyectos <small>Usuario Técnico</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
             
			<div class="x_content">
				

                  <a style="border-radius: 82px; background-color: #ebf3f1;" href="#" data-title="Usuarios Técnicos" class="icono btn btn-app resetformUs">
                    <span class="badge bg-green"><?= $cantidad->numero?></span>
                    <i class="fa fa-user-plus fa-2x"></i> Tecnicos
                  </a>

    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nro</th>
							<th>NOMBRE</th>
							<th>USUARIO</th>
							<th>UNIDAD ORGANIZACIONAL</th>
							<th>ESTADO</th>
							<th>TIPO USUARIO</th>
                            <th>OPCIONES</th>
					    </tr>
					</thead>
					<tbody>
						 <? 
						 $cont=1;
						 foreach($usuarios as $usuario):?>
						 	<? if ($usuario->tipo_usu == 1):?>
						 	<tr>
			                      <td><?= $cont; $cont++;?></td>
			                      <td><?= $usuario->nombre.' '.$usuario->apellido_pat.' '.$usuario->apellido_mat?></td>
			                      <td><?= $usuario->usuario?></td>
			                      <td><?= $usuario->unidad_org?></td>
			                      <td>
			                      		<label data-title="Estado del Usuario" class="switchBtn">
																	<input type="checkbox" class="" <?= ($usuario->activo == 't') ? 'checked ' : '' ?> disabled/>
																	<div class="slide round">Activo</div>
																</label>
			                      	
			                      </td>
			                      <td><?= $usuario->tipousuario?></td>
			                      <td>
			                      	<table>
			                      		<tr>
			                      			<td>
					                      				
			                      					<? if ($usuario->activo == 't'):?>
					                      				<form id="asign_<?=$usuario->id_usuario?>" action="<?= base_url('Administrador/asigProy') ?>" method="post">
					                      				<input type="hidden" name="idUs" value="<?= $usuario->id_usuario?>">
					                      				<a data-title="Asignar Proyecto" href="#" class="btn btn-link btn-float has-text" onclick="asignarUsuarioProy(<?= $usuario->id_usuario?>)"><i class="fa fa-long-arrow-right fa-2x"></i></a>
					                      				</form>
					                      			<? endif; ?>	
			                      			</td>
			                      		</tr>
			                      	</table>
			                      </td>
			                      
                 </tr>
                 <? endif; ?>	
			        <?endforeach?>
					</tbody>
				</table>


			</div>
		</div>
	</div>
</div>



