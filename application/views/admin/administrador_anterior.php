<link href="<?php echo base_url('assets/css/check.css') ?>" rel="stylesheet">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Responsive example<small>Users</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
             
			<div class="x_content">
				

                  <a style="border-radius: 82px; background-color: #ebf3f1;" href="#" data-target=".bs-example-modal-lg" data-toggle="modal" id="addUs" name="addUs" data-title="Registrar Usuarios" class="icono btn btn-app resetformUs">
                    <span class="badge bg-green"><?= $cantidad->numero?></span>
                    <i class="fa fa-user-plus fa-2x"></i> Users
                  </a>







				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nro</th>
							<th>NOMBRE</th>
							<th>USUARIO</th>
							<th>UNIDAD ORGANIZACIONAL</th>
							<th>ESTADO</th>
							<th>TIPO USUARIO</th>
                            <th>OPCIONES</th>
					    </tr>
					</thead>
					<tbody>
						 <? 
						 $cont=1;
						 foreach($usuarios as $usuario):?>
						 	<tr>
			                      <td><?= $cont; $cont++;?></td>
			                      <td><?= $usuario->nombre.' '.$usuario->apellido_pat.' '.$usuario->apellido_mat?></td>
			                      <td><?= $usuario->usuario?></td>
			                      <td><?= $usuario->unidad_org?></td>
			                      <td>
			                      		<label data-title="Habilitar/Deshabilitar Usuarios" class="switchBtn">
																	<input onchange="activo(<?= $usuario->id_usuario ?>)" type="checkbox" class="" <?= ($usuario->activo == 't') ? 'checked ' : '' ?>/>
																	<div class="slide round">Activo</div>
																</label>
			                      	
			                      </td>
			                      <td><?= $usuario->tipousuario?></td>
			                      <td>
			                      	<table>
			                      		<tr>
			                      			<td>
			                      				<a data-title="Editar Usuario" href="#" class="btn btn-link btn-float has-text" onclick="editarUs(<?= $usuario->id_usuario?>)" data-toggle="modal"><i class="fa fa-edit fa-2x"></i></a>|
			                      			</td>

			                      			<td>
					                      				
			                      					<? if ($usuario->tipo_usu == 1):?>
					                      				<form id="asign_<?=$usuario->id_usuario?>" action="<?= base_url('Administrador/asigProy') ?>" method="post">
					                      				<input type="hidden" id="idUs" name="idUs" value="<?= $usuario->id_usuario?>">
					                      				<a data-title="Asignar Proyecto" href="#" class="btn btn-link btn-float has-text" onclick="asignarUsuarioProy(<?= $usuario->id_usuario?>)"><i class="fa fa-long-arrow-right fa-2x"></i></a>
					                      				</form>
					                      			<? endif; ?>	
			                      			</td>
			                      		</tr>
			                      	</table>
			                      </td>
			                      
                 </tr>
			                <?endforeach?>
					</tbody>
				</table>


			</div>
		</div>
	</div>
</div>

<!--inicio modal-->
<div class="modal fade" data-backdrop="static" id="regUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
	      <div class="modal-header">
	                
	      </div>
			      <div class="modal-body">
			      		<!-- Basic setup -->
					      <div class="panel panel-white" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
					            
					            <div  class="alert alert-danger"  id="validacionpermiso" style="display: none;">
					                
					            </div>
					        
					       		<div class="modal-header bg-info">
					       			<!--falta aplicar la clase resetform para limpiar el formulrio-->
					                <button type="button" class="close resetformUs" data-dismiss="modal">&times;</button>
					                <h5 class="modal-title"><span id="accion_form_usuario">Registrar</span> Usuario</h5>
					            </div>     

					       			<div style="padding: 20px; margin: 0px 0px -37px 0px;">  
			<!--cuerpo modal-->
									<div class="row">
						<div class="col-md-12 col-sm-12 ">
							<div class="x_panel">
								<div class="x_title">
									<h2>REGISTRO <small>usuario</small></h2>
									<ul class="nav navbar-right panel_toolbox">
										<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
											<ul class="dropdown-menu" role="menu">
												<li><a class="dropdown-item" href="#">Settings 1</a>
												</li>
												<li><a class="dropdown-item" href="#">Settings 2</a>
												</li>
											</ul>
										</li>
										
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<br />
									<form id="formUsuario" data-parsley-validate class="form-horizontal form-label-left">
										<input type="hidden" name="validador" id="validador">
										<input type="hidden" name="idUsc" id="idUsc">
										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NOMBRE (s) <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												<input type="text" id="nombre" name="nombre" required="required" class="form-control ">
											</div>
										</div>
										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">APELLIDO PAT <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												<input type="text" id="apellidoPat" name="apellidoPat" required="required" class="form-control">
											</div>
										</div>
										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">APELLIDO MAT <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												<input type="text" id="apellidoMat" name="apellidoMat" required="required" class="form-control">
											</div>
										</div>
										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">CI <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												<input type="text" id="ci" name="ci" required="required" class="form-control">
											</div>
										</div><div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">UNIDAD ORGANIZACIONAL <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												<input type="text" id="org" name="org" required="required" class="form-control">
											</div>
										</div>

										<div class="item form-group">
											<label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">TIPO DE USUARIO<span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 ">
												 <select id="tipoUs" name="tipoUs" data-placeholder="Select position" class="form-control">
                       							 <option value="">Seleccione ....</option>
                       							</select>

											</div>
										</div>

										
										
										<div class="ln_solid"></div>
										

									</form>
								</div>
							</div>
						</div>
					</div>






			<!--fin cuerpo modal-->
											<div class="modal-footer">
								            	<button type="button" class="btn btn-danger resetformUs" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
								             <!--falta apñicar la class resetform para limpiar el formulario--> 
								             	<button id="guardarUs" type="button" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                <button style="display:none" id="editarUs" type="button" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Editar</button>
								            </div>
							         </div>



						  </div>
			                <!-- /basic setup -->
			      </div>
    </div>
  </div>
</div>
<!--fin modal-->

