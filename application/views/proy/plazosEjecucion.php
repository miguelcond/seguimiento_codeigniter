<style type="text/css">
#status { padding:10px; background:#1ABB9C; color:#000; font-weight:bold; font-size:12px; margin-bottom:10px; width:4%; border-radius: 25px; }
    .iconok {
    position: absolute;
    margin: -10px; 
    padding-inline: inherit;
    }
    input {
        padding:  10px;
    }
    .scrollAjax{
       
    overflow-x: scroll!Important;
    }
    .ui-dialog{
    	background: #0000;
    	
    height: auto;
    width: 550px;
    top: 229px!important;
    left: 887.5px;
    display: block;
    margin: -114px;
    }
.w3l-login-form {
    background: white;
    opacity: 0.7;
    max-width: 939px;
    margin: 0px 0px 0px 23px;
    padding: 1em;
    box-sizing: border-box;
    color: #06477e;
    position: absolute;
    top: 320px;
    height: 433px;
    width: 170px;
    font-size: 14px;
} 
.line-div1{
	border: 2px solid #2a3f54;
   border-bottom-color: #86df73;
    -webkit-border-radius: 5px;
}

    
</style>
<div class="row">
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Proyecto: </h2>
				<ul class="nav navbar-right panel_toolbox">
         <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i style="color:#f72705" class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" onclick="modalalerta()" class=""><i class="fa fa-warning"></i> Alertas</a>
              </li>
							<li><a href="#" id="compose1" class=""><i class="fa fa-clock-o"></i> Historico de cambios</a>
              </li>
             
						</ul>
					</li>
					
					
				</ul>
				<div class="clearfix"><input type="hidden" id="acuerdoOculto" value="0"></div>
		 </div>
            <div class="x_content">
				
               <table id="tableplazos" style="font-size: 11px;" with="100%" class="BorradorTable1 table datatable-basic table-striped table-hover table-bordered" >
                          <thead>
                              
                              <tr>
                                  
                                  <th>NRO</th>
                                  <th>EDIT</th>
                                  <th>CARTERA</th>
                                  <th>DEPARTAMENTO</th>
                                  <th>MUNICIPIO</th>
                                  <th><div class="" style="width:500px!Important;">NOMBRE PROYECTO</div></th>
                                  <th>NRO. CONVENIO</th>
                                  <th>FECHA INICIO</th>
	                                <th>PLAZO EJEC</th>
	                                <th>ADENDA CONV 1</th>
	                                <th>FECHA ADENDA CONV 1</th>
		                              <th>ADENDA CONV 2</th>
		                              <th>FECHA ADENDA CONV 2</th>
		                              <th>ADENDA CONV 3</th>
		                              <th>FECHA ADENDA CONV 3</th>
		                              <th>ADENDA CONV 4</th>
		                              <th>FECHA ADENDA CONV 4</th>
		                              <th>ADENDA CONV 5</th>
		                              <th>FECHA ADENDA CONV 5</th>
		                              <th>AMPLIACION RESOL FDI</th>
		                              <th style="background-color:#c2f9dc">AMPLIACION TOTAL (D.C.)</th>
		                              <th style="background-color:#f9c6c2">FECHA PLAZO DE EJECUCIÓN FINAL</th>
		                              <th style="background-color:#c2f9dc">% EJEC FINANCIERA</th>
		                              <th>% EJEC FISICA</th>
                                  
                                  <th>ESTADO</th>
                                  <th>ACTA PROVISIONAL</th>
                                  <th>ACTA DEFINITIVA</th>
                                  <th style="background-color:#f6f794">VIGENCIA PLAZO DE EJECUCIÓN</th>
                                  <th style="font-size: 10px; line-height: 4px;"> ALERTAS <br><br>
                                   <div class="" style="width:80px!Important;" data-title1="ALERTA VENCIMIENTO PLAZO DE EJECUCION O PLAZO CONCILIATORIO (DIAS)">
                                     <i style='color:#141212 ;' class='fa fa-warning'></i> <label> < 0 dias &nbsp</label><br>           
                                     <i style='color:#f30606 ;' class='fa fa-warning'></i> <label> 0 a 15 dias &nbsp</label><br>
                                     <i style='color:#f7ae03 ;' class='fa fa-warning'></i> <label> 15 a 30 dias &nbsp</label><br>
                                     <i style='color:#aee1a6 ;' class='fa fa-warning'></i> <label> > 30 dias &nbsp</label>
                                   </div>
                                  </th>
		                              <th>OPCIONES</th>
	                            	

	                            </tr>
	                          
                            </thead>
                          <tbody class="scrollAjax1">

                         </tbody>                
                 </table>



			</div>

		</div>
	</div>
</div>


<div onclick="cerrar_div();" id="dialogoProyecto" title="" style="display:none;" class="w3l-login-form line-div1"><!--div con el mensaje del nombre de proyecto-->
 <div> 
   <label id="ProyectoAeditar"></label>
 </div>
</div>

 <!-- compose -->
 <div class="row">
<div class="compose col-md-6 col-sm-6 col-xs-6" style="border-color:#169f85;">
 	<div style="margin:20px 20px 20px 20px">
      <div class="compose-header">
        Historico de cambios usuario: <?= $this->session->userdata('usuario')?>
        <button type="button" class="close compose-close1">
          <span>×</span>
        </button>
      </div>

      <div class="compose-body">
        
<!--cuerpo ventana-->
									
<table id="tableHistoricoPlazos" style="font-size: 10px; padding: 0px 0px 0px 7px;" with="100%" class="table datatable-basic table-striped table-hover table-bordered" >
                          <thead>
                              <tr>
                                  <th class="clickauto">NRO</th><!--clickauto se cliquea automaticamente para que el datatable se adapte encabezado y contenido-->
                                  <th>PROYECTO</th>
                                  <th>CAMPO Ejec</th>
                                  <th>DATO ACTUAL</th>
                                  <th>DATO ANTERIOR</th>
                                  <th>FECHA MODIFICACION</th>
	                           </tr>
	                          </thead>
                          <tbody>

                         </tbody>                
   </table>


			<!--fin cuerpo ventana-->
       

        
      </div>

    </div>  
</div>
</div>
<!-- /compose -->

<!--inicio modal-->
<div class="modal fade" data-backdrop="static" id="modalAlertas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
                  
        </div>
            <div class="modal-body">
                <!-- Basic setup -->
                <div class="panel panel-white" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                      
                      <div  class="alert alert-danger"  id="validacionpermiso" style="display: none;">
                          
                      </div>
                  
                    <div class="modal-header bg-info">
                      <!--falta aplicar la clase resetform para limpiar el formulrio-->
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h5 class="modal-title"><span>Alerta Vencimiento Plazos de Ejecucion o Plazo Conciliatorio (dias )</span> Usuario: <?= $this->session->userdata('usuario')?></h5>
                      </div>     

                      <div style="padding: 20px; margin: 0px 0px -37px 0px;">  
      <!--cuerpo modal-->
      <center>
       <i style='color:#141212 ;' class='fa fa-warning fa-2x'></i><label> < 0 dias &nbsp</label>           
       <i style='color:#f30606 ;' class='fa fa-warning fa-2x'></i><label> 0 a 15 dias &nbsp</label>
       <i style='color:#f7ae03 ;' class='fa fa-warning fa-2x'></i><label> 15 a 30 dias &nbsp</label>
       <i style='color:#aee1a6 ;' class='fa fa-warning fa-2x'></i><label> > 30 dias &nbsp</label>
       </center>
<table id="tablealertaPlazos" style="font-size: 11px;" with="100%" class="table datatable-basic table-striped table-hover table-bordered" >
                          <thead>
                              <tr>
                                  <th id="clickauto1">NRO</th>
                                  <th>ALERTA</th>
                                  <th>DIAS CALENDARIO</th>
                                  <th>CARTERA</th>
                                  <th>PROYECTO</th>
                                  <th>DEPARTAMENTO</th>
                                  <th>MUNICIPIO</th>
                                  
                             </tr>
                            </thead>
                          <tbody>

                         </tbody>                
                 </table>
      <!--fin cuerpo modal-->
                      <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
                      </div>
                    </div>

              </div>
                      <!-- /basic setup -->
            </div>
    </div>
  </div>
</div>
<!--fin modal-->
