<style type="text/css">
#status { padding:10px; background:#1ABB9C; color:#000; font-weight:bold; font-size:12px; margin-bottom:10px; width:4%; border-radius: 25px; }
    .inputborder {
    border: none;
    background-color: transparent;
    }
    .iconok {
    position: absolute;
    margin: -10px; 
    padding-inline: inherit;
    }
    input {
        padding:  10px;
    }

</style>

<div class="row">
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Responsive example<small>Borrador</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
               
			<div class="x_content">
				
                <table id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer tableEight" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nro</th>
							<th width="50%">NOMBRE PROYECTO</th>
							<th>FAMILIAS</th>
							<th>FECHA PROY</th>
							<th>DEPARTAMENTO</th>
                            <th>OTRA INFORMACION</th>
                            <th>OPCIONES</th>
					    </tr>
					</thead>
					<tbody>
						 <? $num = 1; foreach($filas as $fila):?>
						 	<tr>
			                      <td><?= $num; $num++;?></td>
			                      <td>
                                    <span class="validation iconok"></span>
                                        <textarea style="width:100%" class="inputborder editable" name="nombre_proy:<?= $fila->id;?>" id="nombre_proy:<?= $fila->id;?>" ><?= $fila->nombre_proy;?></textarea>


                                    
			                      <td>
                                    <span class="validation iconok"></span>
                                        <input class="inputborder editable" type="text" min="5" max="150" name="familias:<?= $fila->id;?>" id="familias:<?= $fila->id;?>" value="<?= $fila->familias;?>" onkeyup="this.value=Numeros(this.value)">

                                   </td>
			                      <td>
                                    
                                        <span class="validation iconok"></span>
                                        <input class="inputborder editable" type="date" name="fechaproy:<?= $fila->id;?>" id="fechaproy:<?= $fila->id;?>" value="<?= $fila->fechaproy;?>">
                                  </td>
			                      <td>
                                        <? if($departamentos != FALSE) :?>
                                                <span class="validation iconok"></span>
                                                <select class="inputborder editable" style="display:block;" id="provincia:<?= $fila->id;?>" name="provincia:<?= $fila->id;?>">
                                                        <option value = ''>Selecciona...</option>
                                                    <? foreach($departamentos as $dep) :?>
                                                        <? if($dep->id == $fila->provincia) :?>
                                                               <option value="<?= $dep->id?>" selected><?= $dep->descripcion?></option>
                                                        <? else :?>
                                                               <option value="<?= $dep->id?>" ><?= $dep->descripcion?></option> 
                                                        <? endif;?>    
                                                    <? endforeach;?>
                                                </select>
                                        <? else :?><h3>Antes de seguir con este paso debe registrar departamentos</h3>
                                        <? endif;?> 

                                   
                                        
                                  </td>
                                  <td>
                                     <span class="validation iconok"></span>
                                     <input class="inputborder editable" type="text" name="dadto:<?= $fila->id;?>" id="dadto:<?= $fila->id;?>" value="<?= $fila->dadto;?>">
                                  </td>
                                  <td class="text-center">
                                    Opcion1 | Opcion2
                                  </td>
                                  
                            </tr>
			                <?endforeach?>
					</tbody>
				</table>


			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.my-error-class {
    color:#f96464;
    font-size: small;
}
.my-valid-class {
    color:#122612 ;
}
 button#reset-button.datetime-reset-button {
        display: none;
    }
</style>


