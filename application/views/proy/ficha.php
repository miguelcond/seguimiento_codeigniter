
<style type="text/css">
    /* Curso CSS estilos aprenderaprogramar.com*/
            .body1 {font-family: Arial, Helvetica, sans-serif;}

            .table1 {     font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
                font-size: 12px;    margin: 5px;     width: 99%; text-align: left;    border-collapse: collapse}

            .th1 {     font-size: 13px;     font-weight: normal;     padding: 2px;     background: #b9c9fe;
                border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; }

            .td1 {    padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;
                color: #669;    border-top: 1px solid transparent; }

            .tr1:hover td { background: #d0dafd; color: #339; }
            .titulo1{
                text-align: center;
            background-color: #04194a;
            color: aliceblue;
            border-radius: 7px;
            font-size: x-large;
            }
            .titulo2{
            font-size: medium; font-weight: bold; font-style: oblique;
            }
            .outlinenone {
  outline: none;
  background-color: transparent;
  border: 0;
}
    </style>



        <!--inicio-->
    


  
<center><img style="width: 70%;" src="<?php echo base_url('assets/images/baner.png') ?>" alt="..." ></center>
<table class="table1">
  <tbody>
    <tr class="tr1">
      <th class="th1 titulo1" Colspan="5">FICHA TECNICA DEL PROYECTO SEGUIMIENTO</th>
      <th class="th1" width="5">CONVENIO</th>
      <td class="td1"><?php echo $filas1->num_convenio?></td>
    </tr>
    <tr class="tr1">
      <th class="th1" width="20%">NOMBRE DEL PROYECTO</th>
      <td class="td1" class="titulo2" Colspan="4"><?php echo $filas1->nombre_proy?></td>
      <th class="th1">CARTERA</th>
      <td class="td1"><?php echo $filas1->cartera?></td>
    </tr>
  </tbody>
</table>
 <table class="table1"> 
  <tbody>
    <tr class="tr1">
      <th class="th1" width="5" scope="row">DEPARTAMENTO</th>
      <td class="td1"><?php echo $filas1->departamento?></td>
      <th class="th1" width="5" scope="row">PROVINCIA</th>
      <td class="td1"><?php echo $filas1->provincia?></td>
      <th class="th1" width="5" scope="row">MUNICIPIO</th>
      <td class="td1" ><?php echo $filas1->municipio?></td>
    </tr>
   </tbody>
  </table> 
  <table class="table1">
    <tr class="tr1">
      <th class="th1" width="20%">TIPO DE PROYECTO</th>
      <td class="td1"><?php echo $filas1->tipo_proy?></td>
      <th class="th1" width="20%">FLIAS. BENEF</th>
      <td class="td1"><?php echo $filas1->fam_ben?></td>
    </tr>
    <tr class="tr1">
      <th class="th1">TIPO DE INVERSION</th>
      <td class="td1"><?php echo $filas1->tipo_inversion?></td>
      <th class="th1">ESTADO</th>
      <td class="td1"><?php echo $filas1->estado?></td>  
    </tr>
    <tr class="tr1">
      <th class="th1">FDI</th>
      <td class="td1">BS <?php echo number_format($filas1->financiamiento_fdi, 2, ",", ".")?></td>
      <th class="th1">FECHA DE INICIO</th>
      <td class="td1"><?php echo date("d/m/Y",strtotime($filas1->fecha_inicio))?></td>  
    </tr>
    <tr class="tr1">
      <th class="th1">GAM</th>
      <td class="td1">BS <?php echo number_format($filas1->financiamiento_gam, 2, ",", ".")?></td>
      <th class="th1">PLAZO DE EJECUCION EN EL CONVENIO</th>
      <td class="td1"><?php echo $filas1->plazo_ejec_convenio?> D.C.</td>  
    </tr>
    <tr class="tr1">
      <th class="th1">BENEFICIADOS</th>
      <td class="td1">BS <?php echo number_format($filas1->financiamiento_benef, 2, ",", ".")?></td>
      <th class="th1">AMPLIACIONES TOTAL</th>
      <td class="td1"><?php echo $filas1->ampliaciones_totales?> D.C.</td>  
    </tr>
    <?php
      $c_total= $filas1->financiamiento_fdi+ $filas1->financiamiento_gam + $filas1->financiamiento_benef;
    ?>
    <tr class="tr1">
      <th class="th1">COSTO TOTAL</th>
      <td class="td1">BS <?php echo number_format($filas1->costo_total, 2, ",", ".")?></td>
      <th class="th1">FECHA PLAZO DE EJECUCION FINAL</th>
      <td class="td1"><?php echo date("d/m/Y",strtotime($filas1->fecha_plazo_ejec_final))?></td>  
    </tr>
</table>
<table class="table1">
    <tr class="tr1">
      <th class="th1" width="20%">ACTA PROVICIONAL</th>
      <td class="td1"><?php echo date("d/m/Y",strtotime($filas1->acta_provisional_fecha))?></td>
      <th class="th1" width="20%">ACTA DEFINITIVA</th>
      <td class="td1"><?php echo date("d/m/Y",strtotime($filas1->acta_definitiva_fecha))?></td>
    </tr>
    <tr class="tr1">
      <th class="th1">% EJEC. FINANCIERA</th>
      <td class="td1"><?php echo $filas1->porcentaje_ejec_financiera?> %</td>
      <th class="th1">% EJEC FISICA</th>
      <td class="td1"><?php echo $filas1->porcentaje_ejec_fisica?> %</td>  
    </tr>
    <?php
    $t_desem = $filas1->primer_desembolso + $filas1->segundo_desembolso + $filas1->tercer_desembolso;
    ?>
    <tr class="tr1">
      <th class="th1">1er DESEMBOLSO</th>
      <td class="td1">BS <?php echo number_format($filas1->primer_desembolso, 2, ",", ".")?></td>
      <th class="th1">FECHA 1er DESEMBOLSO</th>
      <td class="td1"><?php echo date("d/m/Y",strtotime($filas1->fecha_primer_des))?></td>  
    </tr>
    <tr class="tr1">
      <th class="th1">2do DESEMBIOLSO</th>
      <td class="td1">BS <?php echo number_format($filas1->segundo_desembolso, 2, ",", ".")?></td>
      <th class="th1">FECHA 2do DESEMBOLSO</th>
      <td class="td1"><?php echo date("d/m/Y",strtotime($filas1->fecha_segudno_des))?></td>  
    </tr>
    <tr class="tr1">
      <th class="th1">3er DESEMBOLSO</th>
      <td class="td1">BS <?php echo number_format($filas1->tercer_desembolso, 2, ",", ".")?></td>
      <th class="th1">FECHA 3er DESEMBOLSO</th>
      <td class="td1"><?php echo date("d/m/Y",strtotime($filas1->fecha_tercer_des))?></td>  
    </tr>
    <tr class="tr1">
      <th class="th1">TOTAL DESEMBOLSADO</th>
      <td class="td1">BS <?php echo number_format($filas1->desembolso_acumulado_fdi, 2, ",", ".")?></td>
        
    </tr>
 </table>   

    
                 </div>
                <!-- /basic setup -->
    