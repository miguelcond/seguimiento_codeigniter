<?
$atts = array(
    'width'=> '800',
    'height' => '600',
    'scrollbars' => 'yes',
    'status' => 'yes',
    'resizable' => 'yes',
    'screenx' => '0',
    'screeny' => '0'
);
?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrwvmO8LJ_mYkFsdIkZ6EFXi-dWwfH05M"></script>
<div class="row">
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>listado de Proyectos asignados al usuario<small> <?= strtoupper($this->session->userdata('mailusu'))?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
             <?= anchor_popup('Reportes/reportevalidaciondiario', 'GENERAR FORMULARIO');?> 
     <div class="x_content">
                <!--
				<p class="text-muted font-13 m-b-30">
					<a href="#" onclick="setMapa();" class="btn btn-primary" data-toggle="modal" id="addproy" name="addproy"><i class="icon-add text-primary"></i><span><i class="fa fa-pencil-square-o fa-2x" style="color:red"></i> Adicionar Proyectos</span></a>
					
				</p>
                -->    
				<table id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer tableEight" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nro</th>
							<th>CARTERA</th>
							<th>DEPARTAMENTO</th>
							<th>MUNICIPIO</th>
							<th width="50%">NOMBRE DEL PROYECTO</th>
                            <th>CONVENIO</th>
                            <th>OPCIONES</th>
					    </tr>
					</thead>
					<tbody>
						 <? $count = 1; foreach($filas as $fila):?>
						 	<tr>
			                      <td><?= $count; $count++;?></td>
			                      <td><?= $fila->cartera?></td>
			                      <td><?= $fila->departamento?></td>
			                      <td><?= $fila->municipio?></td>
			                      <td><?= $fila->nombre_proy?></td>
                                  <td><?= $fila->num_convenio?></td>
                                  <td class="text-center">
                                    <table>
                                        <tr><td><a href="#" data-title="Ficha Técnica" class="btn btn-link btn-float has-text" onclick="javascript:window.open('<? echo base_url()?>Proyecto/details/<?echo $fila->id ?>','','width=1000,height=800,left=50,top=50,toolbar=yes')">Ventana</a></td>
                                            <td><a data-title="Ficha Técnica" href="#" class="btn btn-link btn-float has-text" onclick="mostrar_ficha(<?= $fila->id?>)" data-toggle="modal"><i class="fa fa-th-list"></i></a></td>
                                        </tr>
                                    </table>
                                    
                                  </td>
                                  
                            </tr>
			                <?endforeach?>
					</tbody>
				</table>
               
			</div>
		</div>
	</div>
</div>

<!--inicio eliminar modal-->
<div class="modal fade" data-backdrop="static" id="miModalFicha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
                    
          </div>
      <div class="modal-body">
          <!-- Basic setup -->
          <div class="panel panel-white" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                
                <div  class="alert alert-danger"  id="validacionpermiso" style="display: none;">
                    
                </div>
            
                    <div class="modal-header bg-info">
                        <button type="button" class="close resetform1" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Ficha Técnica</h5>
                    </div>  
                <div class="contain">
                    <div class="wrapper">
                        <div id="datosFichaModal"></div>
  
                    </div>
                  <div class="modal-footer">

                      <div id="opcionesModal">
                        <button id="cerrarFicha" type="button" class="btn btn-danger resetform1" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
                      </div>
      
                        
                 </div>  

                </div>
          </div>
       </div>
     </div>

</div>

</div>

<!--inic modal-->
<div class="modal fade" data-backdrop="static" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
                
      </div>
      <div class="modal-body">
      		<!-- Basic setup -->
		      <div class="panel panel-white" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		            
		            <div  class="alert alert-danger"  id="validacionpermiso" style="display: none;">
		                
		            </div>
		        
		       		<div class="modal-header bg-info">
		                <button type="button" class="close resetform" data-dismiss="modal">&times;</button>
		                <h5 class="modal-title"><span id="accion_form_mueble">Registrar</span> Proyecto</h5>
		            </div>     

		       			<div style="padding: 20px; margin: 0px 0px -37px 0px;">  

		       					<!--inicio wisard-->
		       					<div class="stepwizard">
                <form action="#" id="formProyecto" role="form" data-toggle="validator" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    <!-- SmartWizard html -->                    
                    <div id="smartwizard2" class="myForm_mueble">
                        <ul>
                            <li><a href="#step-1_proy">Paso 1<br /><span>Información del bien</span></a></li>
                            <li><a href="#step-2_proy">Paso 2<br /><span>Ubicación del Bien</span></a></li>
                            <li><a href="#step-3_proy">Paso 3<br /><span>Convenio</span></a></li>
                            <li><a href="#step-4_proy">Paso 4<br /><span>Metas y Resultados</span></a></li>
                        </ul>

                        <div>
                            <div id="step-1_proy" class="" style="height:450px;">
                                 <input type="hidden" name="validador" id="validador">
                                 <input type="hidden" name="id_bien_mueble" id="id_bien_mueble">
                                 <input type="hidden" name="id_entidad_mueble" id="id_entidad_mueble" value="{{entidad.id_entidad}}">
                           <br>



                           <div class="row">
                           
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="control-label">Código de Proyecto<span class="text-danger"> (Generacion automática)</span></span>
                                        <input type="text" name="codigoProyecto" id="codigoProyecto" class="form-control" readonly="">
                                    </div>
                                    
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="control-label">Tipo Proyecto<span class="text-danger">*</span></span>
                                        <select class="form-control" name="tipoProyecto" id="tipoProyecto"> 
                                        </select>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                         

                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="control-label">Cartera<span class="text-danger">*</span></span>
                                        <select class="form-control" name="carteraProyecto" id="carteraProyecto"> 
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                          <span class="control-label">Familias beneficiadas<span class="text-danger">*</span></span>
                                          <input type="number" name="familiasBeneficiadas" id="familiasBeneficiadas" class="form-control" pattern="^[0-9]+">
                                    </div>
                                           
                                </div>


                             </div>  
                           
                           
                             <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <span class="control-label">Tipo de inversión<span class="text-danger">*</span></span>
                                                <select class="form-control" name="tipoInversion" id="tipoInversion"> 
                                                </select>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <span class="control-label">Codigo SISIN</span>
                                                <input type="text" name="codigoSisin" id="codigoSisin" class="form-control">
                                            
                                            </div>
                                            
                                        </div>
                                
                             </div>
                           
                             <div class="row">
                                 
                                <div class="col-md-12" >
                                    <div class="form-group">
                                        <span class="control-label">Nombre Proyecto<span class="text-danger">*</span>(Según convenio)</span>
                                        <textarea name="nombreProyecto" id="nombreProyecto" class="form-control auto-entidad2" rows="5"></textarea>
                                    </div>
                                    
                                </div>
                                
                             </div>
                           
                           
                          </div>
                            <div id="step-2_proy" class="">
                                <div class="col-xs-12">
                                    <legend><span class="fa fa-map-marker"></span> Ubicación del Bien</legend>
                                   
                                            <div class="col-md-6" style="dispplay: block;">
                                                <div class="col-md-12">
                                                    <label class="control-label">Departamento <span class="text-danger">*</span></label>
                                                    <select class="form-control" id="departamento" name="departamento">

                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="control-label">Provincia <span class="text-danger">*</span></label>
                                                    <select class="form-control" id="cbprovincia" name="cbprovincia">

                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="control-label">Municipio <span class="text-danger">*</span></label>
                                                    <select class="form-control" id="cbmunicipio" name="cbmunicipio">
                                                    </select>
                                                </div>


                                                <!--
                                                <div class="col-md-6">
                                                    <span class="control-label">Dirección</span>
                                                    <input type="text" class="form-control auto-entidad2" name="direccion_mueble" id="direccion_mueble"/>
                                                </div>
                                                -->
                                            </div>
                                    
                                    <a href="#" id="addCoord">Adicionar marcador</a><!--2021-->
                                    
                                        <div class="col-md-6" style="dispplay: block;">
                                            <fieldset>
                                                <legend>Nro. comunidades beneficiadas</legend>
                                                <label> Seleccione Número de comunidades</label>
                                                <input type="number" id="nro_comunidades" name="nro_comunidades" class="form-control" onchange="cuotas()" style="width:100px">
                                                <table class="table table-striped" id="tabletest">
                                                    <thead>
                                                        <tr>
                                                            <th>Nro</th>
                                                            <th>Nombre comunidad</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="fila_comunidades">
                                                        
                                                    </tbody>
                                                    
                                                </table>
                                                <div class="dialog-message1"></div>
                                            </fieldset>
                                        </div>
                                    


                                    <!--inicio-->

                                    <!--fin-->

                                    

                                    <div class="col-xs-6" style="dispplay: block;">                                     
                                    	<div class="row">
                                              <div class="col-xs-6">
    					                          <label for="latitud-l">Latitud:</label>
    					                          <input class="form-control input-sm" name="txtLatitud" type="text" id="txtLatitud" value="<?php //echo $this->latitud;?>" maxlength="100" readonly/>
    					                      </div>
    					                      <div class="col-xs-6">
    					                          <label for="longitud-l">Longitud:</label>
    					                          <input class="form-control input-sm" name="txtLongitud" type="text" id="txtLongitud" value="<?php //echo $this->longitud;?>" maxlength="100" readonly/>
    					                      </div>
                  						</div>
                                    </div>
                                    
              						<div class="col-xs-12" style="background-color: #fff; padding: 10px;">
                                       
                                        <label>
                                        <input type="checkbox" name="habilitarC" id="habilitarC" class="js-switch">&nbsp;&nbsp;Desbloquear Latitud/Longitud
                                        </label><br>
                                         aqui google maps
                                        <div id="map" onmouseover="google.maps.event.trigger(map, 'resize');" style="height:400px"></div>
                                    </div>
                                   
                                </div>

                            </div>
                            
                            <div id="step-3_proy" class="">                                
                                <div class="content-frame-body content-frame-body-left">
                                    
                                    <fieldset>
                                        <legend>Datos convenio</legend>
                                            <div class="row">
                                               <div class="col-md-3">
                                                  <div class="form-group">
                                                      <span class="control-label">Nro. Convenio<span class="text-danger">*</span></span>
                                                      <input type="text" name="numeroConvenio" id="numeroConvenio" class="form-control">
                                                  </div>
                                                   
                                               </div>
                                               <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">Fecha convenio<span class="text-danger">*</span></span>
                                                        <input type="date" class="form-control" name="fechaConvenio" id="fechaConvenio">
                                                       
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">Plazo inicial (dias calendario)<span class="text-danger">*</span></span>
                                                        <input type="number" name="plazoInicial" id="plazoInicial" class="form-control" minlength="1">
                                                    
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">Motivo (inicio proyecto)<span class="text-danger">*</span></span>
                                                        <select name="motivoConvenio" id="motivoConvenio" class="form-control" >
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                           </div>  
                                           <div class="row">
                                               <div class="col-md-3">
                                                  <div class="form-group">
                                                      <span class="control-label">Monto total financiamiento<span class="text-danger">*</span></span>
                                                      <input type="text" name="montoFinanciamiento" id="montoFinanciamiento" class="form-control">
                                                  </div>
                                                   
                                               </div>
                                               <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">Fecha inicio proyecto (solo cartera 2)<span class="text-danger">*</span></span>
                                                        <input type="date" class="form-control" name="fechaInicConvenio" id="fechaInicConvenio">
                                                       
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span class="control-label">Adjuntar convenio<span class="text-danger">*</span></span>
                                                        <input type="file" name="subirConvenio" id="subirConvenio" class="btn btn-secondary">
                                                    
                                                    </div>
                                                    
                                                </div>
                                                
                                           </div>     
                                    </fieldset>
                                    <fieldset>
                                        <legend>Inversion según convenio</legend>
                                        <div class="row">
                                               <div class="col-md-3">
                                                  <div class="form-group">
                                                      <span class="control-label">FDI<span class="text-danger">*</span></span>
                                                      <input type="text" name="inversionFdi" id="inversionFdi" class="form-control" onkeyup="Calcular()" value="0">
                                                  </div>
                                                   
                                               </div>
                                               <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">GAM/GAIOC<span class="text-danger">*</span></span>
                                                        <input type="text" class="form-control" name="inversionGam" id="inversionGam" onkeyup="Calcular()" value="0">
                                                       
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">Beneficiarios</span></span>
                                                        <input type="text" name="inversionBeneficiarios" id="inversionBeneficiarios" class="form-control" onkeyup="Calcular()" value="0">
                                                    
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">Otros</span></span>
                                                        <input type="text" name="inversionOtros" id="inversionOtros" class="form-control" onkeyup="Calcular()" value="0">
                                                    </div>
                                                    
                                                </div>
                                           </div> 
                                           <div class="row">
                                               <div class="col-md-3">
                                                  <div class="form-group">
                                                      <span class="control-label">Monto Total</span></span>
                                                      <input type="text" name="inversionTotal" id="inversionTotal" class="form-control" readonly="" value="0">
                                                  </div>
                                                </div>
                                            </div>     
                                    </fieldset>
                                    <fieldset>
                                        <legend>Entidad Ejecutora</legend>
                                        <div class="col-md-3">
                                                    <div class="form-group">
                                                        <span class="control-label">Entidad ejecutora<span class="text-danger">*</span></span>
                                                        <select class="form-control" name="entidadEjecutora" id="entidadEjecutora"  >
                                                        </select>
                                                    </div>
                                                    <a href="#" id="addEntEj">Adicionar Entidad Ejecutora</a><!--2019-->
                                                    
                                        </div>
                                    </fieldset>

                                </div> 
                            </div>
                            <div id="step-4_proy" class="">                                
                                <div class="content-frame-body content-frame-body-left">
                                    <h2>aqui las metas y resultados</h2>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span class="control-label">Indicador desglosado<span class="text-danger">*</span></span>
                                                        <select name="indicadorDesglosado" id="indicadorDesglosado" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select a number" data-hide-disabled="true">
                                                        </select>
                                                    </div>
                                                    
                                        </div>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span class="control-label">Unidad de medida<span class="text-danger">*</span></span>
                                                        <select name="unidadMedida" id="unidadMedida" class="form-control" >
                                                        </select>
                                                    </div>
                                                    
                                        </div>
                                        <div class="col-md-4">
                                                  <div class="form-group">
                                                      <span class="control-label">Meta proyecto<span class="text-danger">*</span></span>
                                                      <input type="text" name="metaProyecto" id="metaProyecto" class="form-control">
                                                  </div>
                                                   
                                        </div>
                                        
                                </div> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
		       					<!--fin wisard-->

								<div class="modal-footer">
					            <button type="button" class="btn btn-danger resetform" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
					              
					            </div>
				         </div>



				        </div>
                <!-- /basic setup -->
      </div>
    </div>
  </div>
</div>
<!--fin modal-->
<!--inicio Entidad ejecutora -->
<div class="modal fade" data-backdrop="static" id="EntidadEjecutoraM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #d9edf7; margin:8px;">
        <h4 class="modal-title" id="exampleModalLabel">Registrar Entidad Ejecutora</h4>        
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-xs-12">
                <span class="control-label">Entidad Ejecutora</span>
                <input type="text" class="form-control" name="txt_EntidadEj" id="txt_EntidadEj" required />
            </div>
        </div>  
        <div class="row">
            <div class="col-xs-12">
                <span class="control-label">Sigla</span>
                <input type="text" class="form-control" name="txt_Sigla" id="txt_Sigla" required />
            </div>
        </div>    

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="cerrar_EntidadEj"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
        <button id="add_EntidadEj" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
        <div class="dialog-message1"></div>
        
      </div>
    </div>
  </div>
</div>



<!--fin tipo-->

<!--inicio geolocalizacion-->
<div class="modal fade" data-backdrop="static" id="geolocalizacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #d9edf7; margin:8px;">
        <h4 class="modal-title" id="exampleModalLabel">Registrar tipo</h4>        
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-xs-6">
                <span class="control-label">latitud</span>
                <input type="text" class="form-control" name="txtLatitud1" id="txtLatitud1" required />
            </div>
            <div class="col-xs-6">
                <span class="control-label">Longitud</span>
                <input type="text" class="form-control" name="txtLongitud1" id="txtLongitud1" required />
            </div>
        </div>   
        <div class="row">
            <div class="col-xs-6">
                <span class="control-label">latitud</span>
                <input type="text" class="form-control" name="txtLatitud2" id="txtLatitud2" required />
            </div>
            <div class="col-xs-6">
                <span class="control-label">Longitud</span>
                <input type="text" class="form-control" name="txtLongitud2" id="txtLongitud2" required />
            </div>
        </div>   
        <div class="row">
            <div class="col-xs-6">
                <span class="control-label">latitud</span>
                <input type="text" class="form-control" name="txtLatitud3" id="txtLatitud3" required />
            </div>
            <div class="col-xs-6">
                <span class="control-label">Longitud</span>
                <input type="text" class="form-control" name="txtLongitud3" id="txtLongitud3" required />
            </div>
        </div>    
        <div id="map1" onmouseover="google.maps.event.trigger(map1, 'resize');" style="height:400px"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="cerrar_geolocalizacion"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
        <button id="add_tipomueble" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
        <div class="dialog-message1"></div>
        
      </div>
    </div>
  </div>
</div>
<!--fin geolocalizacion-->


<style type="text/css">
	.my-error-class {
    color:#f96464;
    font-size: small;
}
.my-valid-class {
    color:#122612 ;
}

</style>
