<style type="text/css">
#status { padding:10px; background:#1ABB9C; color:#000; font-weight:bold; font-size:12px; margin-bottom:10px; width:4%; border-radius: 25px; }
    .inputborder {
    border: none;
    background-color: transparent;
    }
    .iconok {
    position: absolute;
    margin: -10px; 
    padding-inline: inherit;
    }
    input {
        padding:  10px;
    }

</style>

<div class="row">
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>EDICION DE LOS PAZOS DE EJECUSION<small> OK</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
               
			<div class="x_content">
				
                <table id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer tableEight" cellspacing="0" width="100%">
					<thead>
						<tr>
                   						   <th>Nro</th>
                   						   <th width="50%">NOMBRE PROYECTO1</th>
                   						   <th>FECHA INICIO</th>
                   						   <th>PLAZO EJEC CONV</th>
                   						   <th>ADENDA CONV 1</th>
                                                <th>FECHA ADENDA CONV 1</th>
                                                <th>ADENDA CONV 2</th>
                                                <th>FECHA ADENDA CONV 2</th>
                                                <th>ADENDA CONV 3</th>
                                                <th>FECHA ADENDA CONV 3</th>
                                                <th>ADENDA CONV 4</th>
                                                <th>FECHA ADENDA CONV 4</th>
                                                <th>ADENDA CONV 5</th>
                                                <th>FECHA ADENDA CONV 5</th>
                                                <th>AMPLIACION RESOL FDI</th>
                                                <th>% EJEC FISICA</th>
					    </tr>
					</thead>
					<tbody>
						 <? $num = 1; foreach($filas as $fila):?>
						 	<tr>
			                      <td><?= $num; $num++;?></td>
			                     <td>
                                        <span class="validation iconok"></span>
                                        <textarea style="width:100%" class="inputborder" name="nombre_proy:<?= $fila->id;?>" id="nombre_proy:<?= $fila->id;?>" ><?= $fila->nombre_proy;?></textarea>
                                   </td>     	
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder" type="date" name="fecha_inicio:<?= $fila->id;?>" id="fecha_inicio:<?= $fila->id;?>" value="<?= $fila->fecha_inicio;?>">
                                   </td>
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="plazo_ejec_convenio:<?= $fila->id;?>" id="plazo_ejec_convenio:<?= $fila->id;?>" value="<?= $fila->plazo_ejec_convenio;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="adenda_conv1:<?= $fila->id;?>" id="adenda_conv1:<?= $fila->id;?>" value="<?= $fila->adenda_conv1;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="date" name="adenda_conv1_fecha:<?= $fila->id;?>" id="adenda_conv1_fecha:<?= $fila->id;?>" value="<?= $fila->adenda_conv1_fecha;?>">
                                   </td>
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="adenda_conv2:<?= $fila->id;?>" id="adenda_conv2:<?= $fila->id;?>" value="<?= $fila->adenda_conv2;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="date" name="adenda_conv2_fecha:<?= $fila->id;?>" id="adenda_conv2_fecha:<?= $fila->id;?>" value="<?= $fila->adenda_conv2_fecha;?>">
                                   </td>
                                   <td>
                                   	   <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="adenda_conv3:<?= $fila->id;?>" id="adenda_conv3:<?= $fila->id;?>" value="<?= $fila->adenda_conv3;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="date" name="adenda_conv3_fecha:<?= $fila->id;?>" id="adenda_conv3_fecha:<?= $fila->id;?>" value="<?= $fila->adenda_conv3_fecha;?>">
                                   </td>
                                   <td>
                                   	    <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="adenda_conv4:<?= $fila->id;?>" id="adenda_conv4:<?= $fila->id;?>" value="<?= $fila->adenda_conv4;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                                   <td>
                                   	   <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="date" name="adenda_conv4_fecha:<?= $fila->id;?>" id="adenda_conv4_fecha:<?= $fila->id;?>" value="<?= $fila->adenda_conv4_fecha;?>">
                                   </td>
                                   <td>
                                   	   <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="adenda_conv5:<?= $fila->id;?>" id="adenda_conv5:<?= $fila->id;?>" value="<?= $fila->adenda_conv5;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                                   <td>
                                   	   <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="date" name="adenda_conv5_fecha:<?= $fila->id;?>" id="adenda_conv5_fecha:<?= $fila->id;?>" value="<?= $fila->adenda_conv5_fecha;?>">
                                   </td>
                                   <td>
                                   	   <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="ampliacion_resol_fdi:<?= $fila->id;?>" id="ampliacion_resol_fdi:<?= $fila->id;?>" value="<?= $fila->ampliacion_resol_fdi;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                                   <td>
                                   	   <span class="validation iconok"></span>
                                        <input class="inputborder editablePlazos" type="text" name="porcentaje_ejec_fisica:<?= $fila->id;?>" id="porcentaje_ejec_fisica:<?= $fila->id;?>" value="<?= $fila->porcentaje_ejec_fisica;?>" onkeyup="this.value=Numeros(this.value)">
                                   </td>
                         
                            </tr>
			                <?endforeach?>
					</tbody>
				</table>


			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.my-error-class {
    color:#f96464;
    font-size: small;
}
.my-valid-class {
    color:#122612 ;
}
 button#reset-button.datetime-reset-button {
        display: none;
    }
</style>


