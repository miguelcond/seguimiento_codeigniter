<style type="text/css">
#status { padding:10px; background:#1ABB9C; color:#000; font-weight:bold; font-size:12px; margin-bottom:10px; width:4%; border-radius: 25px; }
    .iconok {
    position: absolute;
    margin: -10px; 
    padding-inline: inherit;
    }
    input {
        padding:  10px;
    }
    .scrollAjax{
       
    overflow-x: scroll!Important;
    }
    .ui-dialog{
    	background: #0000;
    	
    height: auto;
    width: 550px;
    top: 229px!important;
    left: 887.5px;
    display: block;
    margin: -114px;
    }
.w3l-login-form2 {
    background: #2a3f54;
    opacity: 0.7;
    max-width: 939px;
    margin: 0px 0px 0px 151px;
    padding: 1em;
    box-sizing: border-box;
    color: #f4f6f9;
    position: absolute;
    top: 305px;
    height: 433px;
    width: 224px;
    font-size: 16px;
} 
.w3l-login-form1 {
  background: #2a3f54;
    max-width: 950px;
    margin: 0px 0px 0px 123px;
    padding: 1em;
    box-sizing: border-box;
    color: #1abb9c;
    position: absolute;
    top: 131px;
}  
.line-div1{
	border: 2px solid #2a3f54;
   border-bottom-color: #86df73;
    -webkit-border-radius: 5px;
}
.w3l-login-form {
    background: white;
    opacity: 0.7;
    max-width: 939px;
    margin: 0px 0px 0px 23px;
    padding: 1em;
    box-sizing: border-box;
    color: #06477e;
    position: absolute;
    top: 305px;
    height: 433px;
    width: 170px;
    font-size: 14px;
} 
</style>
<div class="row">
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Proyecto: </h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i style="color:#f72705" class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" id="compose1" class=""><i class="fa fa-clock-o"></i> Historico de cambios</a>
							</li>
							
						</ul>
					</li>
					
					</li>
				</ul>
				<div class="clearfix"></div>
		 </div>
            <div class="x_content">
				
               <table id="tabledesembolsos" style="font-size: 11px;" with="100%" class="BorradorTable1 table datatable-basic table-striped table-hover table-bordered" >
                          <thead>
                              
                              <tr>
                                  
                                  <th>NRO</th>
                                  <th>EDIT</th>
                                  <th>CARTERA</th>
                                  <th>DEPARTAMENTO</th>
                                  <th>MUNICIPIO</th>
                                  <th><div class="" style="width:500px!Important;">NOMBRE PROYECTO</div></th>
                                  <th>NRO. CONVENIO</th>
                                  <th>COSTO TOTAL</th>
                                  <th>FINANC. FDI</th>
	                                <th>FINANC. GAM</th>
	                                <th>FINANC. BENEFICIARIOS</th>
                                  <th>1ER DESEMBILSO</th>
	                                <th>FECHA 1er DESEMBOLSO</th>
	                                <th>2DO DESEMBOLSO</th>
	                                <th>FECHA 2do DESEMBOLSO</th>
		                              <th>3ER DESEMBOLSO</th>
		                              <th>FECHA 3er DESEMBOLSO</th>
		                             <th style="background-color:#c2f9dc">DESEMBOLSO ACUMULADO FDI</th>
		                              <th style="background-color:#f9c6c2">SALDO FDIL</th>
		                              <th>OPCIONES</th>
	                            	

	                            </tr>
	                          
                            </thead>
                          <tbody class="scrollAjax1">

                         </tbody>                
                 </table>


			</div>

		</div>
	</div>
</div>


<div onclick="cerrar_div();" id="dialogoProyecto" title="" style="display:none;" class="w3l-login-form line-div1"><!--div con el mensaje del nombre de proyecto-->
 <div> 
   <label id="ProyectoAeditar"></label>
 </div>
</div>

 <!-- compose -->
 <div class="row">
<div class="compose col-md-6 col-sm-6 col-xs-6" style="border-color:#169f85;">
 	<div style="margin:20px 20px 20px 20px">
      <div class="compose-header">
        Historico de cambios usuario: <?= $this->session->userdata('usuario')?>
        <button type="button" class="close compose-close1">
          <span>×</span>
        </button>
      </div>

      <div class="compose-body">
        
<!--cuerpo ventana-->
									
<table id="tableHistoricoDesembolsos" style="font-size: 10px;" with="100%" class="table datatable-basic table-striped table-hover table-bordered" >
                          <thead>
                              <tr>
                                  <th class="clickauto">NRO</th><!--clickauto se cliquea automaticamente para que el datatable se adapte encabezado y contenido-->
                                  <th>PROYECTO</th>
                                  <th>CAMPO</th>
                                  <th>DATO ACTUAL</th>
                                  <th>DATO ANTERIOR</th>
                                  <th>FECHA MODIFICACION</th>
	                           </tr>
	                          </thead>
                          <tbody>

                         </tbody>                
   </table>


			<!--fin cuerpo ventana-->
       

        
      </div>

    </div>  
</div>
</div>
<!-- /compose -->
