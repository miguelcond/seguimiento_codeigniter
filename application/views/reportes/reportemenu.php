<link href="<?php echo base_url('assets/css/reportes.css') ?>" rel="stylesheet">
<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Reportes generales: </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i style="color:#f72705" class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" id="" class=""><i class="fa fa-clock-o"></i> opciones</a>
                            </li>
                            
                        </ul>
                    </li>
                    <!--
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                    -->
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content ">
<!--inicio select-->
<div class="col-md-12 col-sm-12  ">
                <div class="x_panel borderpanel">
                  <div class="x_title">
                    <h2><i class="fa fa-align-left"></i> Seleccione....<small> condiciones del reporte</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!--inicio contenido select-->
                <?= form_open('#', array('name'=>'formReporte', 'id'=>'formReporte', 'class'=>'forlog')); ?>
                <div class="col-md-3 col-sm-3  form-group has-feedback">
                <label class="label2" for="RepDepartamento">Departamento</label>
                    <select class="form-control" id="RepDepartamento" name="RepDepartamento">

                    </select>
               </div> 
               <div class="col-md-3 col-sm-3  form-group has-feedback">
                <label  class="label2" for="RepProvincia">Provincia</label>
                    <select class="form-control" id="RepProvincia" name="RepProvincia">

                    </select>
               </div> 
               <div class="col-md-3 col-sm-3  form-group has-feedback">
                <label class="label2" for="RepMunicipio">Município</label>
                    <select class="form-control" id="RepMunicipio" name="RepMunicipio">

                    </select>
               </div> 
               
               <br/>
               <!--fin contenido select-->
              
     </div>
  </div>
</div>               

<!--fin select-->

            <!--inicio acordeon-->
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel borderpanel">
                  <div class="x_title">
                    <h2><i class="fa fa-align-left"></i> Seleccione....<small> reporte por campos</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      

                      <!--inicio situaicon financiera-->  
                      <div class="panel">
                        <a class="acordeonreporte panel-heading">
                          <span style="display: none;">0</span><span style="display: none;">1</span><h4 class="panel-title">Situacion Financiera</h4>
                        </a>
                        <div id="panel1" class="estyleacordeon" style="font-size: 10px;">
                          
                            <!--1er filset-->
                             <div class="col-md-3 col-sm-3  form-group has-feedback">        
                                   <fieldset >
                                    <legend>Seleccione campos</legend>
                                      
                                        <input type="checkbox" id="seleccionar-todos_1" class="seleccionar-todos" checked="checked"> <label class="label1" for="seleccionar-todos_1">SELECCIONAR TODO ..</label>
                                        <div id="listado1" onclick="seleccheck(1)">  
                                        <input type="checkbox" name="chk[]" id="opc1" value="cartera" class="" checked="checked"/><label for="opc1">CARTERA</label>

                                        <input type="checkbox" name="chk[]" id="opc2" value="departamento" class="" checked="checked"/> <label for="opc2">DEPARTAMENTO</label>

                                        <input type="checkbox" name="chk[]" id="opc3" value="municipio" class="" checked="checked"/> <label for="opc3">MUNICIPIO</label>

                                        <input type="checkbox" name="chk[]" id="opc4" value="nombre_proy" class="" checked="checked"/> <label for="opc4">NOMBRE DEL PROYECTO</label>

                                        <input type="checkbox" name="chk[]" id="opc5" value="tipo_inversion" class="" checked="checked"/> <label for="opc5">TIPO DE INVERSION</label>

                                        <input type="checkbox" name="chk[]" id="opc6" value="financiamiento_fdi" class="" checked="checked"/><label for="opc6">FINANCIAMIENTO FDI</label>

                                        </div> 
                                   </fieldset>
                               </div>       
                            <!--fin 1er fielset-->
                            <!--2do filset-->
                             <div class="col-md-3 col-sm-3  form-group has-feedback">        
                                   <fieldset>
                                    <legend>Seleccione campos</legend>
                                      
                                        <input type="checkbox" id="seleccionar-todos_2" class="seleccionar-todos" checked="checked"> <label class="label1" for="seleccionar-todos_2">SELECCIONAR TODO ..</label>
                                        <div id="listado2" onclick="seleccheck(2)">  
                                        

                                        <input type="checkbox" name="chk[]" id="opc7" value="primer_desembolso" class="" checked="checked"/> <label for="opc7">1ER DESEMBOLSO</label>

                                        <input type="checkbox" name="chk[]" id="opc8" value="fecha_primer_des" class="" checked="checked"/> <label for="opc8">FECHA 1ER DESEMBOLSO</label>

                                        <input type="checkbox" name="chk[]" id="opc9" value="segundo_desembolso" class="" checked="checked"/> <label for="opc9">2DO DESEMBOLSO</label>

                                        <input type="checkbox" name="chk[]" id="opc10" value="fecha_segudno_des" class="" checked="checked"/> <label for="opc10">FECHA 2DO DESEMBOLSO</label>

                                        <input type="checkbox" name="chk[]" id="opc11" value="tercer_desembolso" class="" checked="checked"/> <label for="opc11">3ER DESEMBOLSO</label>

                                         <input type="checkbox" name="chk[]" id="opc12" value="fecha_tercer_des" class="" checked="checked"/> <label for="opc12">FECHA 3ER DESEMBOLSO</label>
                                        
                                       </div> 
                                   </fieldset>
                               </div>       
                            <!--fin 2do fielset-->
                            <!--3er filset-->
                             <div class="col-md-3 col-sm-3  form-group has-feedback">        
                                   <fieldset>
                                    <legend>Seleccione campos</legend>
                                      
                                        <input type="checkbox" id="seleccionar-todos_3" class="seleccionar-todos" checked="checked"> <label class="label1" for="seleccionar-todos_3">SELECCIONAR TODO ..</label>
                                        <div id="listado3" onclick="seleccheck(3)">  
                                        <input type="checkbox" name="chk[]" id="opc13" value="desembolso_acumulado_fdi" class="" checked="checked"/><label for="opc13">DESEMBOLSO ACUMULADO FDI</label>

                                        <input type="checkbox" name="chk[]" id="opc14" value="porcentaje_ejec_fisica" class="" checked="checked"/> <label for="opc14">% AVANCE FISICO</label>

                                        <input type="checkbox" name="chk[]" id="opc15" value="porcentaje_ejec_financiera" class="" checked="checked"/> <label for="opc15">% EJECUCION FINANCIERA</label>
                                        </div> 
                                   </fieldset>
                               </div>       
                            <!--fin 3er fielset-->
                         
                        </div>
                      </div>
                      <!--fin situaicon financiera-->

                      
                      
                     
                     
                     

                    </div>
                    <!-- end of accordion -->
                    <div class="col-md-12 col-sm-12  form-group has-feedback">  
                        <!--
                        <button onClick="actionForm(this.form.id, 'Reportes/reportegralExcel'); return false;"> Exportar</button> solo con button agarra el id del formulario al hacer click
                        <button onClick="actionForm(this.form.id, 'Reportes/reportegralPdf'); return false;"> Exportar</button>
                        -->
                        <a data-title="Exportar" href="#" onClick="actionForm('formReporte', 'Reportes/reportegralPdf',0); return false;"><img style="width: 1.5%;" src="<?php echo base_url('assets/images/pdf.ico') ?>" alt="..." > </a>&nbsp;&nbsp;&nbsp;
                        <a data-title="Exportar" href="#" onClick="actionForm('formReporte', 'Reportes/reportegralExcel',1); return false;"><img style="width: 1.5%;" src="<?php echo base_url('assets/images/excel.png') ?>" alt="..." ></a>

                        </div>

                  </div>
                </div>
              </div>
<!--fin acordeon-->


           <?= form_close() ?>
<!--inicio mostrar reportes-->
<div class="col-md-12 col-sm-12  ">
                <div class="x_panel borderpanel">
                  <div class="x_title">
                    <h2><i class="fa fa-align-left"></i> Resultado de la consulta<small> mostrando informacion.....</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!--inicio contenido select-->
                    <div id="showReport"></div>
               <!--fin contenido select-->
              
     </div>
  </div>
</div>            
<!--fin mostrar reportes-->


            </div>
        </div>
    </div>
</div>




