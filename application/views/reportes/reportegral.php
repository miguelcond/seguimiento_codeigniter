<?

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=reporte_seguimiento.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<!DOCTYPE html>
<html>
<head>
    <title></title>

    <meta charset="UTF-8">
</head>
<body>
<img style="width: 80%;" src="<?php echo base_url('assets/images/baner2.png') ?>" alt="..." >
<? if($tiporeporte == 1):?>
<br><br><br><br><br>
<?endif; ?>    
<h2>LISTADO GENERAL DE PROYECTOS DEL FDI</h2>
<table cellspacing="0" cellpadding="0" border="1" style="font-size: 9px;">
    <thead>
       <tr style="background-color: #06416e; color: #fff;">
        <th>Nro</th>
        <? for($i=1; $i<=$cant;$i++) {
            $tituloColumn = 'tituloColumn'.$i;
            ?>
        <th><?=${$tituloColumn}?></th>
        
        <?}?>
    </tr>  
    </thead>
   
    <tbody>
         <? $count = 1; foreach($filas as $fila):?>
            <? if($count%2==0) $color = '#f1eded';
            else $color = '#ffffff'
            ?>
            <tr style="background-color:<?=$color?>">
                  <td><?= $count; $count++;?></td>
                  <? for($j=1; $j<=$cant;$j++) {
                    $campoMostrar = 'campoMostrar'.$j;
                    ?>
                  <td>
                    <?=$fila->${$campoMostrar};                  
                    ?>
                 </td>
                  

                  <?}?>
                  
            </tr>
            <?endforeach?>
            
    </tbody>
<?//=${$campoMostrar} ?>
    
</table>
</body>
</html>
