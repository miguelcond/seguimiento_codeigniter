<?

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=reporte_seguimiento.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<!DOCTYPE html>
<html>
<head>
    <title></title>

    <meta charset="UTF-8">
</head>
<body>
<img style="width: 80%;" src="<?php echo base_url('assets/images/baner2.png') ?>" alt="..." >
<? if($tiporeporte == 1):?>
<br><br><br><br><br>
<?endif; ?>    
<h2>LISTADO GENERAL DE PROYECTOS DEL FDI</h2>
<table cellspacing="0" cellpadding="0" border="1" style="font-size: 9px;">
    <thead>
       <tr style="background-color: #06416e; color: #fff;">
        <th>Nro</th>
        <? for($i=1; $i<=$cant;$i++) {
            $tituloColumn = 'tituloColumn'.$i;
            ?>
        <th><?=${$tituloColumn}?></th>
        
        <?}?>
    </tr>  
    </thead>
   
    <tbody>
         <? $count = 1; foreach($filas as $fila):?>
            <? if($count%2==0) $color = '#f1eded';
            else $color = '#ffffff'
            ?>
            <tr style="background-color:<?=$color?>">
                  <td><?= $count; $count++;?></td>
                  <? for($j=1; $j<=$cant;$j++) {
                    $campoMostrar = 'campoMostrar'.$j;
                    ?>
                  <td>
                    <?
                      if(${$campoMostrar} == 'financiamiento_fdi' or ${$campoMostrar} == 'financiamiento_gam' or ${$campoMostrar} == 'financiamiento_benef' or ${$campoMostrar} == 'primer_desembolso' or ${$campoMostrar} == 'segundo_desembolso' or ${$campoMostrar} == 'tercer_desembolso' or ${$campoMostrar} == 'costo_total' or ${$campoMostrar} == 'desembolso_acumulado_fdi' or ${$campoMostrar} == 'saldo_fdi')
                      echo number_format($fila->${$campoMostrar}, 2, ",", ".");
                      elseif (${$campoMostrar} == 'fecha_primer_des' or ${$campoMostrar} == 'fecha_segudno_des' or ${$campoMostrar} == 'fecha_tercer_des' or ${$campoMostrar} == 'fecha_inicio' or ${$campoMostrar} == 'fecha_tercer_des' or ${$campoMostrar} == 'fecha_tercer_des' or ${$campoMostrar} == 'fecha_tercer_des' or ${$campoMostrar} == 'fecha_tercer_des' or ${$campoMostrar} == 'fecha_tercer_des' or ${$campoMostrar} == 'fecha_tercer_des') 
                           echo date("d/m/Y",strtotime($fila->${$campoMostrar}));
                           else
                           echo  $fila->${$campoMostrar};  
                    ?>
                 </td>
                  

                  <?}?>
                  
            </tr>
            <?endforeach?>
            
    </tbody>
<?//=${$campoMostrar} ?>
    
</table>
</body>
</html>
