<!-- Import Javascript -->
		<script src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
		<!-- Datatables -->
		<script src="<?php echo base_url('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') ?>"></script>
		
		<script src="<?php echo base_url('vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') ?>"></script>
		<script src="<?php echo base_url('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') ?>"></script>
		<script src="<?php echo base_url('vendors/jszip/dist/jszip.min.js') ?>"></script>
		<!-- Custom JS -->
		<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
		<script type="text/javascript" src="<?php echo  base_url() ?>jdseg/proyecto.js"></script>
		<!-- /Import Javascript -->
<style type="text/css">
	
div.dataTables_wrapper div.dataTables_filter label{
	text-align: right!Important;
	background-color: #f3f8fd;
}
</style>


<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size: 8px;">
					<thead>
						<tr>
							<th>Nro</th>
							<th>CARTERA</th>
							<th>DEPARTAMENTO</th>
							<th>MUNICIPIO</th>
							<th>NOMBRE PROY</th>
							<th>TIPO DE INVERSION</th>
							<th>FINANCIAMIENTO FDI</th>
							<th>1ER DESEMBOLSO</th>
							<th>FECHA 1ER DESEMBOLSO</th>
							<th>2DO DESEMBOLSO</th>
							<th>FECHA 2DO DESEMBOLSO</th>
							<th>3ER DESEMBOLSO</th>
							<th>FECHA 3ER DESEMBOLSO</th>
							<th>DESEMBOLSO ACUMULADO FDI</th>
							<th>%AVANCE FISICO</th>
							<th>%AVANCE FINANCIERO</th>
							
                           
					    </tr>
					</thead>
					<tbody>
						 <? $count = 1; foreach($filas as $fila):?>
						 	<tr>
			                      <td><?= $count; $count++;?></td>
			                      <td><?= $fila->cartera?></td>
			                      <td><?= $fila->departamento?></td>
			                      <td><?= $fila->municipio?></td>
			                      <td><?= $fila->nombre_proy?></td>
			                      <td><?= $fila->tipo_inversion?></td>
			                      <td><?= $fila->financiamiento_fdi?></td>
			                      <td><?= $fila->primer_desembolso?></td>
			                      <td><?= $fila->fecha_primer_des?></td>
			                      <td><?= $fila->segundo_desembolso?></td>
			                      <td><?= $fila->fecha_segudno_des?></td>
			                      <td><?= $fila->tercer_desembolso?></td>
			                      <td><?= $fila->fecha_tercer_des?></td>
			                      <td><?= $fila->desembolso_acumulado_fdi?></td>
			                      <td><?= $fila->porcentaje_ejec_fisica?></td>
			                      <td><?= $fila->porcentaje_ejec_financiera?></td>
                                  
                                  
                            </tr>
			                <?endforeach?>
					</tbody>
				</table>



		
		
		