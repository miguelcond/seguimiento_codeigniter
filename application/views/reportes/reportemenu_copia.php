<link href="<?php echo base_url('assets/css/reportes.css') ?>" rel="stylesheet">
<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Reportes generales: </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i style="color:#f72705" class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" id="" class=""><i class="fa fa-clock-o"></i> opciones</a>
                            </li>
                            
                        </ul>
                    </li>
                    <!--
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                    -->
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content ">
<!--inicio select-->
<div class="col-md-12 col-sm-12  ">
                <div class="x_panel borderpanel">
                  <div class="x_title">
                    <h2><i class="fa fa-align-left"></i> Seleccione....<small> condiciones del reporte</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!--inicio contenido select-->
                <?= form_open('#', array('name'=>'formReporte', 'id'=>'formReporte', 'class'=>'forlog')); ?>
                <div class="col-md-3 col-sm-3  form-group has-feedback">
                <label class="label2" for="RepDepartamento">Departamento</label>
                    <select class="form-control" id="RepDepartamento" name="RepDepartamento">

                    </select>
               </div> 
               <div class="col-md-3 col-sm-3  form-group has-feedback">
                <label  class="label2" for="RepProvincia">Provincia</label>
                    <select class="form-control" id="RepProvincia" name="RepProvincia">

                    </select>
               </div> 
               <div class="col-md-3 col-sm-3  form-group has-feedback">
                <label class="label2" for="RepMunicipio">Município</label>
                    <select class="form-control" id="RepMunicipio" name="RepMunicipio">

                    </select>
               </div> 
               
               <br/>
               <!--fin contenido select-->
              
     </div>
  </div>
</div>               

<!--fin select-->

            <!--inicio acordeon-->
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel borderpanel">
                  <div class="x_title">
                    <h2><i class="fa fa-align-left"></i> Seleccione....<small> reporte por campos</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      


                      <div class="panel">
                        <a class="acordeonreporte panel-heading">
                          <span style="display: none;">0</span><span style="display: none;">1</span><h4 class="panel-title">Situacion Financiera</h4>
                        </a>
                        <div id="panel1" class="estyleacordeon">
                          
                            <!---->
                             <div class="col-md-2 col-sm-2  form-group has-feedback">        
                                   <fieldset>
                                    <legend>Situacion Financiera de proyectos</legend>
                                      
                                        <input type="checkbox" id="seleccionar-todos_1" class="seleccionar-todos"> <label class="label1" for="seleccionar-todos_1">Seleccionar todo</label>
                                        <div id="listado1" onclick="seleccheck(1)">  
                                        <input type="checkbox" name="chk[]" id="opc1" value="cartera" class="" /><label for="opc1">Cartera</label>

                                        <input type="checkbox" name="chk[]" id="opc2" value="nombre_proy" class="" /> <label for="opc2">Nombre del proyecto</label>

                                        <input type="checkbox" name="chk[]" id="opc3" value="num_convenio" class="" /> <label for="opc3">Nro Convenio</label>

                                        <input type="checkbox" name="chk[]" id="opc4" value="costo_total" class="" /> <label for="opc4">Costo  Total</label>
                                        
                                       </div> 
                                   </fieldset>
                               </div>       
                            <!---->
                         
                        </div>
                      </div>

                       <div class="panel">
                        <a class="acordeonreporte panel-heading">
                          <span style="display: none;">0</span><span style="display: none;">2</span><h4 class="panel-title">Plazos Ejecusión y Estado del Proyecto</h4>
                        </a>
                        <div id="panel2" class="estyleacordeon">
                          
                            <!---->
                            <div class="col-md-2 col-sm-2  form-group has-feedback">        
                                   <fieldset>
                                            <legend>Plazos ejecusion y estado del proyecto</legend>
                                      
                                        <input type="checkbox" id="seleccionar-todos_2" class="seleccionar-todos"><label class="label1" for="seleccionar-todos_2">Seleccionar todo</label>
                                        <div id="listado2" onclick="seleccheck(2)">  
                                        <input type="checkbox" name="chkkuhkjhk" id="hobby12" value="ski" class="" /><label for="hobby12">uno</label>

                                        <input type="checkbox" name="chk2" id="hobby22" value="run" class="" /> <label for="hobby22">dos</label>

                                        <input type="checkbox" name="chk3" id="hobby32" value="eat" class="" /> <label for="hobby32">tres</label>

                                        <input type="checkbox" name="chk4" id="hobby42" value="sleep" class="" /> <label for="hobby42">cuatro</label>
                                        
                                       </div> 
                                   </fieldset>
                              </div>       
                            <!---->
                         
                        </div>
                      </div>

                       <div class="panel">
                        <a class="acordeonreporte panel-heading">
                          <span style="display: none;">0</span><span style="display: none;">3</span><h4 class="panel-title">Acuerdo Conciliatorio / Ejecución</h4>
                        </a>
                        <div id="panel3" class="estyleacordeon">
                          
                            <!---->
                             <div class="col-md-2 col-sm-2  form-group has-feedback">        
                                   <fieldset>
                                            <legend>Acuerdo conciliatorio - inpecciones</legend>
                                      
                                        <input type="checkbox" id="seleccionar-todos_3" class="seleccionar-todos"> <label class="label1" for="seleccionar-todos_3">Seleccionar todo</label>
                                        <div id="listado3" onclick="seleccheck(3)">  
                                        <input type="checkbox" name="chkkuhkjhk" id="hobby123" value="ski" class="" /><label for="hobby123">uno</label>

                                        <input type="checkbox" name="chk2" id="hobby223" value="run" class="" /> <label for="hobby223">dos</label>

                                        <input type="checkbox" name="chk3" id="hobby323" value="eat" class="" /> <label for="hobby323">tres</label>

                                        <input type="checkbox" name="chk4" id="hobby423" value="sleep" class="" /> <label for="hobby423">cuatro</label>
                                        
                                       </div> 
                                   </fieldset>
                              </div>  
                            <!---->
                         
                        </div>
                      </div>

                      <div class="panel">
                        <a class="acordeonreporte panel-heading">
                          <span style="display: none;">0</span><span style="display: none;">4</span><h4 class="panel-title">Situacion General del Proyecto / inspecciones</h4>
                        </a>
                        <div id="panel4" class="estyleacordeon">
                          
                            <!---->
                             <div class="col-md-2 col-sm-2  form-group has-feedback">        
                                   <fieldset>
                                            <legend>Acuerdo conciliatorio - inpecciones</legend>
                                      
                                        <input type="checkbox" id="seleccionar-todos_4" class="seleccionar-todos"> <label class="label1" for="seleccionar-todos_4">Seleccionar todo</label>
                                        <div id="listado4" onclick="seleccheck(4)">  
                                        <input type="checkbox" name="chkkuhkjhk" id="hobby124" value="ski" class="" /><label for="hobby124">uno</label>

                                        <input type="checkbox" name="chk2" id="hobby224" value="run" class="" /> <label for="hobby224">dos</label>

                                        <input type="checkbox" name="chk3" id="hobby324" value="eat" class="" /> <label for="hobby324">tres</label>

                                        <input type="checkbox" name="chk4" id="hobby424" value="sleep" class="" /> <label for="hobby424">cuatro</label>
                                        
                                       </div> 
                                   </fieldset>
                              </div>  
                            <!---->
                         
                        </div>
                      </div>
                     
                     

                    </div>
                    <!-- end of accordion -->
                    <div class="col-md-12 col-sm-12  form-group has-feedback">  
                        <!--
                        <button onClick="actionForm(this.form.id, 'Reportes/reportegralExcel'); return false;"> Exportar</button> solo con button agarra el id del formulario al hacer click
                        <button onClick="actionForm(this.form.id, 'Reportes/reportegralPdf'); return false;"> Exportar</button>
                        -->
                        <a data-title="Exportar" href="#" onClick="actionForm('formReporte', 'Reportes/reportegralPdf',0); return false;"><img style="width: 1.5%;" src="<?php echo base_url('assets/images/pdf.ico') ?>" alt="..." > </a>&nbsp;&nbsp;&nbsp;
                        <a data-title="Exportar" href="#" onClick="actionForm('formReporte', 'Reportes/reportegralExcel',1); return false;"><img style="width: 1.5%;" src="<?php echo base_url('assets/images/excel.png') ?>" alt="..." ></a>

                        </div>

                  </div>
                </div>
              </div>
<!--fin acordeon-->


           <?= form_close() ?>
            </div>
        </div>
    </div>
</div>




