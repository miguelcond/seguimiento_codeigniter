function activo(id)
    {
      $.ajax({
            url: base_url+'Administrador/activoUsuario',
            type: 'POST',
            data: 'id=' + id,
            success: function(data) {
   //                                            alert(data);
            },
            error: function() {
                alert('Error!');
            }
        });
        
    }

   $("#addUs").click(function() {//tico
     $('#regUsuario').modal('show');
     $('#editarUs').hide(); //volvemos a ocultar el boton editar (1)
     $('#guardarUs').show();
     $('#accion_form_usuario').text('Registrar');
   });

   $.ajax({//
            url: enlace+'Administrador/getTipoUsuario',
            method: 'POST',
            success: function(data) {
                $('#tipoUs').html(data);  
            }
            
    }); 

$('#guardarUs').on('click',function()
      {
       $("#formUsuario").submit();  
    });
$('#editarUs').on('click',function()
      {
       $("#formUsuario").submit();  
    });

//inicio validar form
jQuery.validator.addMethod("cirepetido", function() {
var result;
if($('#validador').val() == $('#ci').val()){//entra solo cuando se introduce un codigo diferente al que esta en la bd
    result = true;                          //si por accidente se coloca el curso en el campo de texto, al levantarlo detecta como ci repetido 
   
} else{
      var num = $('#ci').val();
      var urlBase = enlace + "Administrador/verificaCi";
                   $.ajax({
                          url: urlBase,
                          method: 'GET',
                          async: false,
                          data: {numci:num},
                         
                          success: function(data) {
                               if(data == 1){
                               
                               result = false;
                               
                               } else {
                                  
                                  result = true;
                               }
                               
                                   
                          }            
                  }); 

 }
return result;
}, "Este número de ci ya ha sido registrado !!");
var validatorUs = $("#formUsuario").validate({   /* variable validatorUs solo para resetear el form de las alertas de validacion*/
        ignore: "input[type=hidden], .select2-search__field",
        errorClass: "validation-error-label",
        successClass: "validation-valid-label",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },        
        rules: {
            nombre: {
                required: true,
                minlength: 3
            },
            apellidoPat: {
                required: true,
                minlength: 3
               
            },
            apellidoMat: {
                required: true,
                minlength: 3
             },
            tipoUs: {
                required: true
            },
            ci: {
                required: true,
                minlength: 6,
                cirepetido: true
            },
            org: {
                required: true
            }
            
            
        },
        submitHandler: function() {
            enviardatosUs();           
        }


    });

function enviardatosUs()
{
 //alert("entro"+enlace);

 if($('#accion_form_usuario').text() == 'Registrar'){
      var urlBase = enlace + "Administrador/guardardatosUs";
      var datos = $('#formUsuario').serialize(); 
        $.ajax({
            type: "GET",
            url: urlBase,
            data: datos, 
            success: function(data)  
             {
                
                if (data == 1) 
                {
                  swal("Correcto", "Usuario Registrado correctamente.", "success")
                    .then((value) => {
                        location.reload();
                    });    
                }
                else
                {
                  swal("Error", "Porfavor, intente nuevamente.", "error")
                    .then((value) => {
                        location.reload();
                    });
                }
             }
        });

}
 
if($('#accion_form_usuario').text() == 'Editar'){
      var urlBase = enlace + "Administrador/editardatosUs";
      var datos = $('#formUsuario').serialize(); 
        $.ajax({
            type: "GET",
            url: urlBase,
            data: datos, 
            success: function(data)  
             {
                
                if (data == 1) 
                {
                  swal("Correcto", "Informacion del Servidor Público Editada correctamente.", "success")
                    .then((value) => {
                        location.reload();
                    });    
                }
                else
                {
                  swal("Error", "Porfavor, intente nuevamente.", "error")
                    .then((value) => {
                        location.reload();
                    });
                }
             }
        });

}


}
//fin validar form

function editarUs(idUs)
{
  //alert("entro:"+idUs);
    $('#regUsuario').modal('show');
    var urlBase = enlace + "Administrador/getUsuario";
        $.ajax({
            type: "GET",
            url: urlBase,
            data: {id: idUs},
            success: function(data){
                     //alert(data);
                     var result = JSON.parse(data);
                    //console.log(result.nombre);
                    //$.each(result, function(i, val){
                     $('#validador').val(result.ci);
                     $('#idUsc').val(idUs);
                     $('#accion_form_usuario').text('Editar'); 
                     $('#nombre').val(result.nombre);  
                     $('#apellidoPat').val(result.apellido_pat);
                     $('#apellidoMat').val(result.apellido_mat);
                     $('#ci').val(result.ci);
                     $('#org').val(result.unidad_org);
                     $('#tipoUs').val(result.tipo_usu);
                     $('#editarUs').show();
                     $('#guardarUs').hide();
                    //});
            }

        }); 
}
$('.resetformUs').click(function() { //limpiar el formulario (2)
        
        validatorUs.resetForm();
        $('#formUsuario')[0].reset();
        $("select").val(['']);
        $('.form-control').removeClass('error');
        $('.form-control').removeClass('required');    
        $('.error').remove();

 });

function asignarUsuarioProy(valor){
    
    $("#asign_"+valor).submit();  
   
  
}
function asignProyectoUs(idproy,idus){
   swal({
          title: "Esta seguro de asignar proyecto?",
          text: "No podras deshacer este paso...!",
          icon: "warning",
          buttons: [
            'No, no estoy seguro(a)',
            'Si, estoy seguro(a)!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            swal({
              title: 'Enviando informacion ....',
              icon: 'success'
            });
           
                 setTimeout(function(){
                ///inicio
                    var urlBase = enlace + "Administrador/asignProyectoUsuario";
                    var parametros = {
                                        'idproyecto' : idproy,
                                        'idusuario' : idus
                                     }
                     $.ajax({
                        type: "GET",
                        url: urlBase,
                        data: parametros, 
                        success: function(data)  
                         {
                            
                            if (data == 1) 
                            {
                              swal("Correcto", "Se ha asignado el proyetco correctamente.", "success")
                                .then((value) => {
                                    $("#recargarUsuario").submit(); //enviamos el id al controlador a travez del formulario para evitar que muestre el explorador mensaje de confirmacion
                                    //location.reload();
                                });    
                            }
                            else
                            {
                              swal("Error", "Porfavor, intente nuevamente.", "error")
                                .then((value) => {
                                    $("#recargarUsuario").submit();
                                    //location.reload(true);
                                });
                            }
                         }
                    });
            ///fin
               },700);
           
          } else {
            swal("Cancelado", "no se guardo la accion", "error");
          }
        }); 



  
}

function desasignar(idasign){
   swal({
          title: "Esta seguro de desasignar proyecto?",
          text: "No podras deshacer este paso...!",
          icon: "warning",
          buttons: [
            'No, no estoy seguro(a)',
            'Si, estoy seguro(a)!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            swal({
              title: 'Enviando informacion ....',
              icon: 'success'
            });
            setTimeout(function(){
            //inicio
                var urlBase = enlace + "Administrador/desasignarProy";
                    var parametros = {
                                        'idasignacion' : idasign
                                    }
                     $.ajax({
                        type: "GET",
                        url: urlBase,
                        data: parametros, 
                        success: function(data)  
                         {
                            
                            if (data == 1) 
                            {
                              swal("Correcto", "Se ha des asignado el proyetco correctamente.", "success")
                                .then((value) => {
                                    //window.location.reload();
                                    $("#recargarUsuario").submit();
                                });    
                            }
                            else
                            {
                              swal("Error", "Porfavor, intente nuevamente.", "error")
                                .then((value) => {
                                    $("#recargarUsuario").submit();
                                    //location.reload();
                                });
                            }
                         }
                    });
            //fin    
          },700); 
          } else {
            swal("Cancelado", "no se guardo la accion", "error");
          }
        });

}