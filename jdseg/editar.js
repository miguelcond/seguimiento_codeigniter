 var message_status = $("#status");

    $(".editable").change(function(){
      console.log("entro input");
      var rownumber = $(this).attr("id");
      var value = $(this).val().trim();
      var validationField = $(this).parent().find('.validation');//parent agarra el que esta encima de campo de texto .parent().parente() seria dos encima
                $.post(enlace + 'Proyecto/proceso' , {row:rownumber, valuei:value}, function(data){
                  if(data == 1)
                            {
                               validationField.html("<img src='../assets/images/loader.gif' height='15px'>");
                                //validationField.hide().empty();
                                setTimeout(function(){
                                  swal("Editado correctamente !!", ".....presione OK para contimuar", "success");
                                  validationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#f70c0c'></i>");
                                  validationField.show();
                                 },250);
                                setTimeout(function(){
                                  validationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#13fd0b'></i>");
                                  validationField.show();
                               },4000);



                            } else {
                                swal("NO se ha editado correctamente !!", ".....presione OK para contimuar","error");
                             }

              });
    });

    $(".editablePlazos").change(function(){
      console.log("entro plazos");
      var formacuerdo = $('#acuerdoOculto').val();
      //console.log(formacuerdo);
      var plazosrownumber = $(this).attr("id");
      var plazosvalue = $(this).val().trim();
      var cadena = plazosrownumber.split(':');
      var plazosvalidationField = $(this).parent().find('.validation');//parent agarra el que esta encima de campo de texto .parent().parente() seria dos encima
                $.post(enlace + 'Proyecto/procesoEditProyecto' , {row:plazosrownumber, valuei:plazosvalue, tabl:'datos_proyecto.plazos_ejecucion'}, function(data){
                  if(data == 1)
                            {
                               
                                    $.ajax({//para mostrar actualizacion de apliaciones totales y fecha plazo de ejecucion final
                                        dataType  : 'html',
                                        url: enlace +"Proyecto/actualizarPlazosAjax",
                                        //async: false,
                                        method: 'POST',
                                        data: {id_val:cadena[1]},
                                        success: function(data){
                                            //cadena2 = data.split('*');
                                            var resultP = JSON.parse(data);
                                            $('#editAmpliacionesT_'+cadena[1]).html("<img src='../assets/images/loader.gif' height='15px'>");
                                            $('#editFechaPlazoE_'+cadena[1]).html("<img src='../assets/images/loader.gif' height='15px'>");
                                            $('#editAlertaPlazos_'+cadena[1]).html("<img src='../assets/images/loader.gif' height='15px'>");
                                            setTimeout(function(){
                                               $('#editAmpliacionesT_'+cadena[1]).html(resultP.ampliaciones_t);
                                               $('#editFechaPlazoE_'+cadena[1]).html(resultP.plazo_ejec_fin);
                                                $('#editAlertaPlazos_'+cadena[1]).html(resultP.alertaplazos);

                                           },250);
                                           
                                       }
                                    });

                               cargarHistoricos('tableHistoricoPlazos','datos_proyecto.plazos_ejecucion');
                               cargarAlertasplazos();
                               plazosvalidationField.html("<img src='../assets/images/loader.gif' height='15px'>");

                                //validationField.hide().empty();
                                setTimeout(function(){
                                  swal("Editado correctamente !!", ".....presione OK para contimuar", "success");
                                  plazosvalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#f70c0c'></i>");
                                  plazosvalidationField.show();
                                  if(formacuerdo == 0 && cadena[0] == 'vigencia_plazo_ejecucion' && plazosvalue == 'NO VIGENTE') location.reload();
                                  if(formacuerdo == 1 && cadena[0] == 'vigencia_plazo_ejecucion' && plazosvalue != 'NO VIGENTE') location.reload();

                                 },250);
                                setTimeout(function(){
                                  plazosvalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#13fd0b'></i>");
                                  plazosvalidationField.show();
                               },4000);



                            } else {
                                swal("NO se ha editado correctamente !!", ".....presione OK para contimuar","error");
                             }

              });
    });
    $(".editableAcuerdo").change(function(){
      console.log("entro a acuerdo");
      var acuerdorownumber = $(this).attr("id");
      var acuerdovalue = $(this).val().trim();
      var cadena = acuerdorownumber.split(':');
      var acuerdovalidationField = $(this).parent().find('.validation');//parent agarra el que esta encima de campo de texto .parent().parente() seria dos encima
                $.post(enlace + 'Proyecto/procesoEditProyecto' , {row:acuerdorownumber, valuei:acuerdovalue, tabl:'datos_proyecto.acuerdo_conciliatorio'}, function(data){
                  if(data == 1)
                            {
                               $.ajax({//para mostrar actualizacion de apliaciones totales y fecha plazo de ejecucion final
                                        dataType  : 'html',
                                        url: enlace +"Proyecto/actualizarAcuerdo",
                                        //async: false,
                                        method: 'POST',
                                        data: {id_val:cadena[1]},
                                        success: function(data){
                                            //alert(data);
                                            var result = JSON.parse(data);
                                            //cadena2 = data.split('*');
                                            console.log(result);
                                            $('#editFechaLimite_'+cadena[1]).html("<img src='../assets/images/loader.gif' height='15px'>");
                                            $('#editAlertaPlazos_'+cadena[1]).html("<img src='../assets/images/loader.gif' height='15px'>");
                                            setTimeout(function(){
                                               $('#editFechaLimite_'+cadena[1]).html(result.fecha_lim);
                                               $('#editAlertaPlazos_'+cadena[1]).html(result.alertaA);
                                          },250);
                                           
                                       }
                                    });

                              cargarHistoricos('tableHistoricoAcuerdo','datos_proyecto.acuerdo_conciliatorio');
                               acuerdovalidationField.html("<img src='../assets/images/loader.gif' height='15px'>");
                                //validationField.hide().empty();
                                setTimeout(function(){
                                  swal("Editado correctamente !!", ".....presione OK para contimuar", "success");
                                  acuerdovalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#f70c0c'></i>");
                                  acuerdovalidationField.show();
                                 },250);
                                setTimeout(function(){
                                  acuerdovalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#13fd0b'></i>");
                                  acuerdovalidationField.show();
                               },4000);



                            } else {
                                swal("NO se ha editado correctamente !!", ".....presione OK para contimuar","error");
                             }

              });
    });
$(".editableSituaciones").change(function(){
      console.log("entro situaciones");
      var sitrownumber = $(this).attr("id");
      var sitvalue = $(this).val().trim();
      var sitvalidationField = $(this).parent().find('.validation');//parent agarra el que esta encima de campo de texto .parent().parente() seria dos encima
                $.post(enlace + 'Proyecto/procesoEditProyecto' , {row:sitrownumber, valuei:sitvalue, tabl:'datos_proyecto.situacion_general_inspeccion'}, function(data){
                  if(data == 1)
                            {
                               cargarHistoricos('tableHistoricoSituaciones','datos_proyecto.situacion_general_inspeccion');
                               sitvalidationField.html("<img src='../assets/images/loader.gif' height='15px'>");
                                //validationField.hide().empty();
                                setTimeout(function(){
                                  swal("Editado correctamente !!", ".....presione OK para contimuar", "success");
                                  sitvalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#f70c0c'></i>");
                                  sitvalidationField.show();
                                 },250);
                                setTimeout(function(){
                                  sitvalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#13fd0b'></i>");
                                  sitvalidationField.show();
                               },4000);



                            } else {
                                swal("NO se ha editado correctamente !!", ".....presione OK para contimuar","error");
                             }

              });
    });
$(".editableDesembolsos").change(function(){
      console.log("entro desembolsos");
      var desembolsosrownumber = '';
      var desembolsosvalue = '';
      elementosig = $(this).next();
      if (elementosig.attr("id")){
          desembolsosrownumber = elementosig.attr("id");
          desembolsosvalue = elementosig.val().trim();
      }
        else{
          desembolsosrownumber = $(this).attr("id");
          desembolsosvalue = $(this).val().trim();
        } 
      
      //console.log('valor de id:'+desembolsosrownumber);
      //console.log('valor de valor:'+desembolsosvalue);
      
      var cadena = desembolsosrownumber.split(':'); //para recargar valores
      var desembolsosvalidationField = $(this).parent().find('.validation');//parent agarra el que esta encima de campo de texto .parent().parente() seria dos encima
                $.post(enlace + 'Proyecto/procesoEditProyecto' , {row:desembolsosrownumber, valuei:desembolsosvalue, tabl:'datos_financiera.desembolsos'}, function(data){
                  if(data == 1)
                            {
                               
                                    
                                    
                                    $.ajax({//para mostrar actualizacion de apliaciones totales y fecha plazo de ejecucion final
                                        dataType  : 'html',
                                        url: enlace +"Proyecto/actualizardesembolsosAjax",
                                        //async: false,
                                        method: 'POST',
                                        data: {id_val:cadena[1]},
                                        success: function(data){
                                            //alert(data);
                                            cadena2 = data.split(':');
                                            $('#editDesemAcum_'+cadena[1]).html("<img src='../assets/images/loader.gif' height='15px'>");
                                            $('#editSaldoFdi_'+cadena[1]).html("<img src='../assets/images/loader.gif' height='15px'>");
                                            setTimeout(function(){
                                               $('#editDesemAcum_'+cadena[1]).html(cadena2[0]);
                                               $('#editSaldoFdi_'+cadena[1]).html(cadena2[1]);
                                           },250);
                                           
                                       }
                                    });
                                    
                               cargarHistoricos('tableHistoricoDesembolsos','datos_financiera.desembolsos');
                               desembolsosvalidationField.html("<img src='../assets/images/loader.gif' height='15px'>");

                                //validationField.hide().empty();
                                setTimeout(function(){
                                  swal("Editado correctamente !!", ".....presione OK para contimuar", "success");
                                  desembolsosvalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#f70c0c'></i>");
                                  desembolsosvalidationField.show();
                                 },250);
                                setTimeout(function(){
                                  desembolsosvalidationField.html("<i class='fa fa-check-square-o fa-1x' style='color:#13fd0b'></i>");
                                  desembolsosvalidationField.show();
                               },4000);



                            } else {
                                swal("NO se ha editado correctamente !!", ".....presione OK para contimuar","error");
                             }

              });
    });
function Numeros(string){//Solo numeros
  var out = '';
    var filtro = '1234567890';//Caracteres validos
  
  //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<string.length; i++)
       if (filtro.indexOf(string.charAt(i)) != -1) 
       //Se añaden a la salida los caracteres validos
       out += string.charAt(i);
  
  //Retornar valor filtrado
    return out;
}

function NumerosMonto(string, idtext){//Solo numeros
  var input = document.querySelector("#"+idtext);
  idtextnext = input.nextElementSibling.id;
  console.log(idtextnext);  

  var out = '';
  var outMonto = '';
    var filtro = '1234567890,.';//Caracteres validos
  
  //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<string.length; i++){
       if (filtro.indexOf(string.charAt(i)) != -1) //charat retorna el caracter i de la cadena
       //Se añaden a la salida los caracteres validos
       out += string.charAt(i);
     }  
    //inicio
    if(out.includes(',')){//verificamos si existe separador de comas
       cad = out.split(',');//si existe almacemanos los valores en un vector separado por esa coma
       cad1 = cad[0];
       cad2 = ','+cad[1];
      }else{
       cad1 = out;
       cad2='';
     }
      out1 = '';
    cad1 =  cad1.replace(/[.]/gi, '');//eliminamos tosos los puntos par ala cadena nueva
    //console.log(cad1);  
    k = 1;
    l = 1;  
        for (var j = cad1.length-1; j >= 0; j--) {
          if (k == l*4 )
          {
            out1 = cad1.charAt(j)+'.' + out1;
            l++;
            k = k+2;
          }else{
            out1 = cad1.charAt(j) + out1;
            k++;

          }
            
        }
       outMonto = out1+cad2.replace(/[.]/gi, '');;
       montopostgresql = outMonto.replace(/[.]/gi, '');
       montopostgresql = montopostgresql.replace(/[,]/gi, '.');
       document.getElementById(idtextnext).value = montopostgresql;
       
    return outMonto;
}  
function NumerosPuntoComa(string){//Solo numeros
  var out = '';
    var filtro = '1234567890.,';//Caracteres validos
  
  //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    count = 0;
    for (var i=0; i<string.length; i++) {
      if (filtro.indexOf(string.charAt(i)) != -1) {
        if(string.charAt(i) == ',') count++; //para validar que solo se intruduzca una coma
        if(string.charAt(i) == ',' && count > 1) out = out + '';// si se quiere introducir mas de  una coma, se reemplaza p or ''
        else out += string.charAt(i);
      }
      
       
    }
       
  
  //Retornar valor filtrado
    return out;
} 

