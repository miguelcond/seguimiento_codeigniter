
var base_url;
var hlr2 = 0;
//var alertaValidacion = '';
 
function baseurl1(enlace)
{
   base_url = enlace;
   init_echarts();//
   cargarHistoricos('tableHistoricoPlazos','datos_proyecto.plazos_ejecucion');
   cargarHistoricos('tableHistoricoDesembolsos','datos_financiera.desembolsos');
   cargarHistoricos('tableHistoricoAcuerdo','datos_proyecto.acuerdo_conciliatorio');
   cargarHistoricos('tableHistoricoSituaciones','datos_proyecto.situacion_general_inspeccion');
   cargarAlertasplazos();
   //setajax();
}

function modalalerta(){
$('#modalAlertas').modal('show');
setTimeout(function(){
    $("#clickauto1").trigger("click"); //se cliquea de forma automatica para adaptar el datatable con el contenido (trigger obliga a que se realice el click)
    $("#clickauto1").trigger("click");//el primero ordena descendentemente y el segundo clic ascendentememte
    
},200);

}
  

function cargarAlertasplazos(){
$("#tablealertaPlazos").DataTable( {
        ajax: {
            url: enlace+'Proyecto/listaAlertas',
            async: false,
            type: "POST",
             
        },
        "fixedColumns": true,
        fixedColumns: {
            leftColumns : 3
        },
        "columns": [
           { "data": "nro"},
           { "data": "alerta"},
           { "data": "dias"},
           { "data": "cart"},
           { "data": "nombre"},
           { "data": "depa"},
           { "data": "munc"}
        ],
            
       "autoWidth": false, //
       "scrollY": "450px",
       "scrollX": true,
       //"responsive": true,
        "bDestroy": true,//
        "bProcessing": true,
        "aLengthMenu": [[10,20, -1], [10,20, "Todo"]],
        "iDisplayLength": 20,
        
    }); 
}

$("#tabledesembolsos").DataTable( {
        ajax: {
            url: enlace+'Proyecto/listaDesembolsos',
            async: false,
            type: "POST",
            data: function ( d ) {
                 //d.func = id;
                 //$('#javasc').html('<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>');
                
            }
        },
        "fixedColumns": true,
        fixedColumns: {
            leftColumns : 3
        },
        "columns": [
           { "data": "nro"},
           { "data": "edit"},
           { "data": "cart"},
           { "data": "depa"},
           { "data": "munc"},
           { "data": "nombre"},
           { "data": "nConv"},
           { "data": "costoTotal"},
           { "data": "financFdi"},
           { "data": "financGam"},
           { "data": "financBenef"},
           { "data": "desem1"},
           { "data": "fechaDesem1"},
           { "data": "desem2"},
           { "data": "fechaDesem2"},
           { "data": "desem3"},
           { "data": "fechaDesem3"},
           { "data": "desemAcum"},
           { "data": "saldoFdi"},
           { "data": "opc"}
           ],
            
       "autoWidth": false, //
       "scrollY": "450px",
       "scrollX": true,
       //"responsive": true,
        "bDestroy": true,//
        "bProcessing": true,
        "aLengthMenu": [[10,20, -1], [10,20, "Todo"]],
        "iDisplayLength": 10,
        
    });
$("#tablesituaciones").DataTable( {
        ajax: {
            url: enlace+'Proyecto/listaSituaciones',
            async: false,
            type: "POST",
            data: function ( d ) {
                 //d.func = id;
                 //$('#javasc').html('<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>');
                
            }
        },
        //"fixedColumns": true,
        "fixedColumns": {
            leftColumns : 3
        },
        "columns": [
           { "data": "nro"},
           { "data": "edit"},
           { "data": "cart"},
           { "data": "depa"},
           { "data": "munc"},
           { "data": "nombre"},
           { "data": "nConv"},
          
           { "data": "otrassituaciones"},
           { "data": "inspeccion1"},
           { "data": "tecnicoins1"},
           { "data": "inspeccion2"},
           { "data": "tecnicoins2"},
           { "data": "inspeccion3"},
           { "data": "tecnicoins3"},
           { "data": "opc"}
           ],
            
       "autoWidth": false, //
       "scrollY": "450px",
       "scrollX": true,
      //"responsive": true,
        "bDestroy": true,//
        "bProcessing": true,
        "aLengthMenu": [[10,20, -1], [10,20, "Todo"]],
        "iDisplayLength": 10,
        
    });
$(window).scroll(function(){
    //alert('hola');
    $('#header').css({
        'left': $(this).scrollLeft() + 15 //Why this 15, because in the CSS, we have set left 15, so as we scroll, we would want this to remain at 15px left
    });
});
 $("#tableacuerdo").DataTable( {
        ajax: {
            url: enlace+'Proyecto/listaacuerdo',
            async: false,
            type: "POST",
            data: function ( d ) {
                 //d.func = id;
                 //$('#javasc').html('<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>');
                
            }
        },
        "fixedColumns": true,
        fixedColumns: {
            leftColumns : 3
        },
        "columns": [
           { "data": "nro"},
           { "data": "edit"},
           { "data": "cart"},
           { "data": "depa"},
           { "data": "munc"},
           { "data": "nombre"},
           { "data": "nConv"},

           { "data": "acuerdoconc"},
           { "data": "acuerdoconcno"},
           { "data": "plazoconciliatorio"},
           { "data": "fechalimite"},
           { "data": "vigenciaplazos"},
           { "data": "alertaPlazos"},

           { "data": "opc"}
           ],
            
       "autoWidth": false, //
       "scrollY": "450px",
       "scrollX": true,
       //"responsive": true,
        "bDestroy": true,//
        "bProcessing": true,
        "aLengthMenu": [[10,20, -1], [10,20, "Todo"]],
        "iDisplayLength": 10,
        
    });

function cargarHistoricos(idtabla,campo){
$("#"+idtabla).DataTable( {
        ajax: {
            url: enlace+'Proyecto/listaHist',
            async: false,
            type: "POST",
            data: {datotab:campo}
        },
        "fixedColumns": true,
        fixedColumns: {
            leftColumns : 5
        },
        "columns": [
           { "data": "nro"},
           { "data": "proyectoh"},
           { "data": "campoh"},
           { "data": "datoH"},
           { "data": "datoAntH"},
           { "data": "fechaH"}
           
           ],
            
       "autoWidth": false, //
       "scrollY": "250px",
       "scrollX": true,
       //"responsive": true,
        "bDestroy": true,//
        "bProcessing": true,
        "aLengthMenu": [[10,20, -1], [10,20, "Todo"]],
        "iDisplayLength": 10,
        
    }); 

}


$("#tableplazos").DataTable( {
        ajax: {
            url: enlace+'Proyecto/listaPlazosEjecucion',
            async: false,
            type: "POST",
            data: function ( d ) {
                 //d.func = id;
                 //$('#javasc').html('<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>');
                
            }
        },
        "fixedColumns": true,
        fixedColumns: {
            left : 1,
            right: 1
        },
        "columns": [
           { "data": "nro"},
           { "data": "edit"},
           { "data": "cart"},
           { "data": "depa"},
           { "data": "munc"},
           { "data": "nombre"},
           { "data": "nConv"},
           { "data": "fechai"},
           { "data": "plazoe"},
           { "data": "adenda1"},
           { "data": "fechaadenda1"},
           { "data": "adenda2"},
           { "data": "fechaadenda2"},
           { "data": "adenda3"},
           { "data": "fechaadenda3"},
           { "data": "adenda4"},
           { "data": "fechaadenda4"},
           { "data": "adenda5"},
           { "data": "fechaadenda5"},
           { "data": "ampliacionresfdi"},
           { "data": "ampliacionresTotal"},
           { "data": "fechaPlazoEejecF"},
           { "data": "porcenFinanciera"},
           { "data": "porcenFisica"},

           ///
            { "data": "estado"},
           { "data": "actaprov"},
           { "data": "actadef"},
           { "data": "vigenciaplazos"},
           { "data": "alertaPlazos"},
           ///
           { "data": "opc"}
           ],
            
       "autoWidth": false, //
       "scrollY": "450px",
       "scrollX": true,
       //"responsive": true,
        "bDestroy": true,//
        "bProcessing": true,
        "aLengthMenu": [[10,20, -1], [10,20, "Todo"]],
        "iDisplayLength": 10,
        
    });
   
///marcar fila

function marcarFiladbl(){
$('.BorradorTable1 tbody tr').on("dblclick", marcarFila).off('dblclick');//deshabilitar eventos clic ejecutados anteriormente
$('.BorradorTable1 tbody tr').on("dblclick", marcarFila);
function marcarFila(){
    //alert("entro");
    $('.BorradorTable1 tr').each(function(i){
        if($("td:eq(1)", this).text()==0)
        $(this).removeAttr("style");
    });
    $("#dialogoProyecto").hide("slow");
    
    $("#dialogoProyecto").show("fast");
    $(this).css("background-color","#d0e1d7");
    var datosfijos = 'Cartera: '+$("td:eq(2)", this).text()+'<br>══════©═════<br>'
                    +'Departamento: '+$("td:eq(3)", this).text()+'<br>══════©═════<br>'
                    +'Municipio: '+$("td:eq(4)", this).text()+'<br>══════©═════<br>'
                    +'Nombre proyecto: '+$("td:eq(5)", this).text()+'<br>══════©═════<br>'
                    +'Nro. convenio: '+$("td:eq(6)", this).text();
    $("#ProyectoAeditar").html(datosfijos);
}
}
///fin marcar fila

//////inicio tabla habilitar/deshabilitar

var i=0;
var count = 0;
var sw = 0;


/*

*/
function cerrar_div(){
    $("#dialogoProyecto").hide("slow");
}
function habilitarTr(){
    $('.BorradorTable1 tbody tr').on("click", filaProyecto);//llama a la funcioion para marcar toda una fila 
}

function editarProyectoAjax(){
count = 0; 
i=1;
desvincular_evento = $('.BorradorTable1 tbody tr').on("click", filaProyecto);
}
function filaProyecto(){
 count ++;
 if (count != 2){
        //$("#dialogoProyecto:ui-dialog").dialog("destroy");
             $("#dialogoProyecto").hide("slow");
            var c = 'Cartera: '+$("td:eq(2)", this).text()+'<br>══════©═════<br>'
                    +'Departamento: '+$("td:eq(3)", this).text()+'<br>══════©═════<br>'
                    +'Municipio: '+$("td:eq(4)", this).text()+'<br>══════©═════<br>'
                    +'Nombre proyecto: '+$("td:eq(5)", this).text()+'<br>══════©═════<br>'
                    +'Nro. convenio: '+$("td:eq(6)", this).text();
            var c1 = $("td:eq(5)", this).text()        
            var b = $("td:eq(1)", this).text();
         if(i == 1){//si hace clic en editar
                  if (b==0){
                            $(this).css("background-color","#2a3f54");
                            $(this).css("color","#1abb9c");
                            swal("Edicion habilitada", "Proyecto: "+c1);
                            $("td:eq(1)", this).html("<a href='#' onclick='editarProyectoAjax();'><i class='fa fa-toggle-on fa-2x' style='color:#1abb9c'></i><span style='display:none'>1</span></a>");
                            $("td:first", this).parent().children().children().each(function () {
                                $(this).prop('disabled', false);
                            });
                        
                        }
                    else{
                            $(this).removeAttr("style");
                            swal("Edicion deshabilitada", "Proyecto: "+c1);       
                            $("td:eq(1)", this).html("<a href='#' onclick='editarProyectoAjax();'><i class='fa fa-toggle-off fa-2x' style='color:#1abb9c'></i><span style='display:none'>0</span></a>");
                            $("td:first", this).parent().children().children().each(function () {
                               $(this).prop('disabled', true);
                            });

                    }
                 i=0;
                $(desvincular_evento).off('click'); //como se llama al evento dentro de un clic, de esta forma desvinculamos el evento click del tr
                habilitarTr();

          }
          
          b = $("td:eq(1)", this).text();
          if (b == 1){
                $("#dialogoProyecto").show("fast");
                /*
                $("#dialogoProyecto").dialog({
                      modal: false,
                      title: "Caja con opciones",
                      width: 550,
                      minWidth: 400,
                      maxWidth: 650,
                      show: "fold",
                      hide: "scale"
                     
                  });
                  */
                $("#ProyectoAeditar").html(c);
                } 
 }
   //console.log(count);
}
    
//////fin tabla habilitar/deshabilitar


function mostrar_ficha(valor){
    $.ajax({
            dataType  : 'html',
            url: base_url+"Proyecto/detailsficha",
            async: false,
            method: 'POST',
            data: {id1:valor},
            success: function(data){
                //alert(data);

                $('#datosFichaModal').html(data);
                $('#miModalFicha').modal('show');
                $('#opcionesModal').prepend('<button id="botonPdf" type="button" class="btn btn-warning"><i class="fa fa-file-pdf-o"></i><a href="'+base_url+'Proyecto/pdfdetails/'+valor+'"> Generar PDF</a>');

            }
        });
    /*
    setTimeout(function(){
    //console.log("Hola Mundo");
    
}, 1000);
    */
}
function guardar_proyecto(){ 
        //alert ("guardar");
           
        var parametro = new FormData($('#formProyecto')[0]);
        if($('#accion_form_mueble').text() == 'Registrar'){
            $.ajax({
                url: base_url+"Proyecto/registrar",
                data: parametro,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function(data) {
                    alert(data);
                    //location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                },
                error: function() {
                    alert(data);
                    //location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                }
            });
        } 
     /* 
    if($('#accion_form_mueble').text() == 'Editar'){
            //$('#accion_form_mueble').text('Registrar');
            $.ajax({
                url: "{{ url('muebles/editar')}}",
                data: parametro,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function(data) {                
                    //alert(data);
                    console.log(data);
                    location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                },
                error: function() {
                    location.assign("{{ url("bienes/index/")}}{{entidad.id_entidad}}|4");
                }
            });
        }

        $('#modal_large_mueble').modal('toggle'); 
        */
    }

 /////inicio google map
var marker;          
var coords = {};
var address = 'Bolivia';
var pais = 'Bolivia';
var docval = '';

function geocodeResult(results, status) {
    
    if (status == 'OK') {
        
        var mapOptions = {
            center: results[0].geometry.location,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map($("#map").get(0), mapOptions);
        map.fitBounds(results[0].geometry.viewport);
        
        var markerOptions = { 
            map: map,
            draggable: true, 
            animation: google.maps.Animation.DROP,
            position: results[0].geometry.location 
        }
        var marker = new google.maps.Marker(markerOptions);

        marker.addListener('click', toggleBounce);      
        marker.addListener( 'dragend', function (event)
        {
            document.getElementById("txtLatitud").value = this.getPosition().lat();
            document.getElementById("txtLongitud").value = this.getPosition().lng();
        });
        
        console.log(marker.getPosition().lat());
        document.getElementById("txtLatitud").value = marker.getPosition().lat();
        document.getElementById("txtLongitud").value = marker.getPosition().lng();
        
    } else {
        alert("Geocoding no tuvo éxito debido a: " + status);
    }
}

function setMapa (coords)
{   
    var nzoom = 15;
    if(!coords || coords.lat==''){
        nzoom = 5;
        coords =  {
            lng: -65.36572949062497,
            lat: -16.949146464111088
        };        
    }
    var map = new google.maps.Map(document.getElementById('map'),
    {
        zoom: nzoom,
        center:new google.maps.LatLng(coords.lat,coords.lng),   
    });
    map.panBy(-400, -200);
    marker = new google.maps.Marker({
        map: map,
        draggable: true, 
        animation: google.maps.Animation.DROP,
        title: 'Ubicacion',
        position: new google.maps.LatLng(coords.lat,coords.lng), 
    });
    marker.addListener('click', toggleBounce);      
    marker.addListener( 'dragend', function (event)
    {
        document.getElementById("txtLatitud").value = this.getPosition().lat();
        document.getElementById("txtLongitud").value = this.getPosition().lng();
    });
/////inicio
const contentString =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
    '<div id="bodyContent">' +
    "<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large " +
    "sandstone rock formation in the southern part of the " +
    "Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) " +
    "south west of the nearest large town, Alice Springs; 450&#160;km " +
    "(280&#160;mi) by road. Kata Tjuta and Uluru are the two major " +
    "features of the Uluru - Kata Tjuta National Park. Uluru is " +
    "sacred to the Pitjantjatjara and Yankunytjatjara, the " +
    "Aboriginal people of the area. It has many springs, waterholes, " +
    "rock caves and ancient paintings. Uluru is listed as a World " +
    "Heritage Site.</p>" +
    '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
    "https://en.wikipedia.org/w/index.php?title=Uluru</a> " +
    "(last visited June 22, 2009).</p>" +
    "</div>" +
    "</div>";
  const infowindow = new google.maps.InfoWindow({
    content: contentString,
  });


marker.addListener("click", () => {
    infowindow.open({
      anchor: marker,
      map,
      shouldFocus: false,
    });
});







//////fin


}
function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
    //alert("entro1");
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
    //alert("entro 2");
  }
}  

////fin google map 
//inicio adicionar municipio
function cuotas(){
    var cuotas = $('#nro_comunidades').val();
    $('#fila_comunidades').empty();
    for (var i = 1; i <= cuotas; i++){
        $('#fila_comunidades').append('<tr><td>'+i+'</td><td><input type="text" name="monto_plan'+i+'" id="monto_plan'+i+'" class="form-control monto_plan" onchange="monto_suma()" required></td></tr>');    
    }
    
}
//fin adicionar municipio

function Calcular(){
    
    if ($('#inversionFdi').val()=='') $('#inversionFdi').val(0);
    if ($('#inversionGam').val()=='') $('#inversionGam').val(0); 
    if ($('#inversionBeneficiarios').val()=='') $('#inversionBeneficiarios').val(0);
    if ($('#inversionOtros').val()=='') $('#inversionOtros').val(0);
    
    sum1 = $('#inversionFdi').val();
    sum2 = $('#inversionGam').val();
    sum3 = $('#inversionBeneficiarios').val(); 
    sum4 = $('#inversionOtros').val();
    total = parseInt(sum1)+parseInt(sum2)+parseInt(sum3)+parseInt(sum4);
    $('#inversionTotal').val(total);
}

function setMapa1 (coords,num) //tico1
{   
    

for(var i=0; i<=3; i++) {
    window['p'+i] = "hello " + i;
}

console.log(p0);
console.log(p1);

    var nzoom = 15;
    if(!coords || coords.lat==''){
        nzoom = 5;
        coords =  {
            lng: -65.36572949062497,
            lat: -16.949146464111088
        };        
    }

    var map = new google.maps.Map(document.getElementById('map1'),
    {
        zoom: nzoom,
        center:new google.maps.LatLng(coords.lat,coords.lng),   
    });
    map.panBy(-400, -200);


    for (var i = 1; i <= num; i++) {
               //marker = window['marker'+i]; 
                window['marker'+i] = new google.maps.Marker({
                map: map,
                draggable: true, 
                animation: google.maps.Animation.DROP,
                title: 'numero_'+i,
                position: new google.maps.LatLng(coords.lat,coords.lng), 
            });
            window['marker'+i].addListener('click', toggleBounce);      
            window['marker'+i].addListener( 'dragend', function (event)
            {
                var mark=this.title;
                var res = mark.split("_")[1];
                document.getElementById("txtLatitud"+res).value = this.getPosition().lat();
                document.getElementById("txtLongitud"+res).value = this.getPosition().lng();


            });

////////iniico
const contentString = 'UBICACION COMUNIDAD '+i;
  const infowindow = new google.maps.InfoWindow({
    content: contentString,
    name: 'mark_'+i,
  });

//varia = window['marker'+i];
window['marker'+i].addListener("click", () => {
    var markeri=infowindow.name;
    var res1 = markeri.split("_")[1];
    console.log(marker1);
    infowindow.open({
      anchor: window['marker'+res1], //aqui se pone la variable de marker
      map,
      shouldFocus: true,
    });
});

////////////////fin

    }

}

function nuevoproyecto()
{
        //inicioabrir mmodal geolocalizacion
        
        $("#addCoord").click(function() {//tico
            num = $('#nro_comunidades').val();
            coords =  {
                    lng: $('#txtLongitud').val(),
                    lat: $('#txtLatitud').val()
                };

            if ($('#nro_comunidades').val() >= 1 && $('#txtLongitud').val() != ''){
                 $('#geolocalizacion').modal('show');
                 setMapa1(coords,num);
                 $('.dialog-message1').html('');
            }
              else
             $('.dialog-message1').html('Debe seleccionar la Ubicacion y el Núumero de comunidades');  
        //$("#txt_geolocalizacion").val("");
        });
        $("#cerrar_geolocalizacion").click(function(){
            $('#geolocalizacion').modal('hide');
            //$("geolocalizacion").modal("toggle");
            $('.modal').css('overflow-y', 'auto');
            
        });
        //fin modal geolocalizacion   

$('#gmimap0').click(function(){
    alert("entro al marcador");
});
$('#addproy').click(function() {
$('#miModal').modal('show');
//$('#editarfun').hide();
//$('#guardarfun').show();
//$('#accion_form_mueble').text('Registrar');
});
$('#habilitarC').click(function(){
    //valor=$('#habilitarC').val();
    valchk = $('input:checkbox[name=habilitarC]:checked').val(); 
    if(valchk=='on'){
        $('#txtLatitud').removeAttr('readonly', false);
        $('#txtLongitud').removeAttr('readonly', false);
    }
    else
       {
        $('#txtLatitud').attr('readonly', true);
        $('#txtLongitud').attr('readonly', true);
       } 
        
});


//abrir entidad ejecutora
$("#addEntEj").click(function() {//tico
    
$('#EntidadEjecutoraM').modal('show');
$("#txt_EntidadEj").val("");
});

$("#cerrar_EntidadEj").click(function(){
    $('#EntidadEjecutoraM').modal('hide');
    $("#txt_EntidadEj").val("");
    $('.modal').css('overflow-y', 'auto');
});
//fin abrir modal entidad ejecutora


$("#add_EntidadEj").click(function(){
    var enlaceTipo = base_url + "Proyecto/saveTipo";
    valt= $("#txt_EntidadEj").val();
    sigla= $("#txt_Sigla").val();

    //alert("gusrdar: "+valt);
    $.ajax({
                        dataType  : "html",
                        type: "POST",
                        url: enlaceTipo,
                        data: {ent:valt,
                               sigl: sigla},
                        // Mostramos un mensaje con la respuesta de PHP
                              success: function(data) {


                                //alert(data);
                                  
                                  //$( "#dialog-carga" ).dialog( "close" )
                                 
                                  //alert("data de ajax"+data);
                                  if (data == 0 || data == 1){ 
                                          if(data==0) msg="no existe el nombre";
                                                else msg="El nombre ya se encuentra registrado..!!" 
                                          $(".dialog-message1").html(msg);
                                          setTimeout( function() {
                                            //$("#dialog_tipoentidad").dialog("close");
                                            $('#EntidadEjecutoraM').modal('hide');
                                            $(".dialog-message1").html("");
                                          }, 2000 );
                                    }
                                    else{
                                          $("select#entidadEjecutora").html(data);
                                          $(".dialog-message1").html("Se ha guardado la información con exito");
                                          setTimeout( function() {
                                            $('#EntidadEjecutoraM').modal('hide');
                                            $(".dialog-message1").html("");
                                            //$("#dialog_tipoentidad").dialog("close");
                                          }, 2000 );

                                    }
                                    
                                  
                        }
                 });
    
});


/*
$('#tipo_inversion').change(function(){

    var val = $('#tipo_inversion option:selected').text();
    if(val=='Otro')
      _dialogo1(250, 150, 'Cerrar', 'REGISTRO TIPO ENTIDAD');
    //alert("agarro el valor de data"+val);
  });
*/
//fin dialog


//////////////////////////////seleccionar unidad de medida de acuerdo a tipo proyecto

$('#tipoProyecto').change(function(){
    var opcion_seleccionada = $('#tipoProyecto option:selected').val();
    if(opcion_seleccionada == 2){
       $('#unidadMedida').val(1);
       $('#unidadMedida').mouseup(function(e) {
           $('#unidadMedida').blur();  
            });
        }
        
        
    if(opcion_seleccionada == 3){
       $('#unidadMedida').val(3);
       $('#unidadMedida').mouseup(function(e) {
           $('#unidadMedida').blur();  
            });
        }
    if(opcion_seleccionada == 1 || opcion_seleccionada == 4){
           $('#unidadMedida').off('mouseup');
           $('#unidadMedida').val('');
        }
       
});
/////////////////fin 
//////inicio generar codigo
$('#tipoProyecto').change(function(){
    var opcion_selec = $('#tipoProyecto option:selected').val();
    //alert("valor" + opcion_selec);
    
    switch (opcion_selec){
        case '1' :
            $('#codigoProyecto').val('PR + correlativo');
        break;
        case '2' :
            $('#codigoProyecto').val('SR + correlativo');
        break;
        case '3' :
            $('#codigoProyecto').val('PU + correlativo');
        break;
        case '4' :
            $('#codigoProyecto').val('ME + correlativo');
        break;
        default :
            $('#codigoProyecto').val('');
        
    }
    
});
//////fin generar codigo
///inicio cartera - fecha inicio
$('#carteraProyecto').change(function(){
   var opcion_seleccionada1 = $('#carteraProyecto option:selected').val();
   //alert(opcion_seleccionada1);
   if(opcion_seleccionada1 == 1)//tico
    $('#fechaInicConvenio').hide();
   else
    $('#fechaInicConvenio').show();
});
//inicio autocompletado
modalautocompletar2 = "#miModal";
console.log(modalautocompletar2);  
var enlace2 = base_url + "Proyecto/getDepartamento2";
$('.auto-entidad2').autocomplete({
                        
    source: function( request, response ) {
        $.ajax({
            type: "POST",
            url: enlace2,
            dataType: "json",
            data: {
                term: request.term
            },
            success: function( data ) {
                //console.log(data);
                response(data);             
                //alert(data);
            }
        });
    },
    appendTo: modalautocompletar2
});
//fin autocompletado


//cargar datos de select





var enlace = base_url + "Proyecto/getDepartamento";
$.ajax({//
            url: enlace,
            method: 'POST',
            success: function(data) {
                $('#departamento').html(data);  
            }
            
    }); 

$('#departamento').change(function(){
    var valor = $('#departamento').val();
    //alert(valor);
        $.ajax({
            dataType  : 'html',
            url: base_url+"Proyecto/getProv",
            method: 'POST',
            data: {departam:valor},
            success: function(data){
                $('#cbprovincia').html(data);
            }
        });
        address = pais +', '+ $('#departamento option:selected').text();
        var geocoder = new google.maps.Geocoder();
        //alert("hola: "+address);
        geocoder.geocode({ 'address': address}, geocodeResult);
});

$('#cbprovincia').change(function(){
    var valor1 = $('#cbprovincia').val();
    //alert(valor1);
        $.ajax({
            dataType  : 'html',
            url: base_url+"Proyecto/getMun",
            method: 'POST',
            data: {prov:valor1},
            success: function(data){
                $('#cbmunicipio').html(data);
            }
        });
        address = pais +', '+ $('#departamento option:selected').text()+' '+$('#cbprovincia option:selected').text();
        //alert(address);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address}, geocodeResult);
});

$('#cbmunicipio').change(function(){
        address = pais +', '+ $('#departamento option:selected').text()+' '+$('#cbprovincia option:selected').text()+' '+$('#cbmunicipio option:selected').text();
        //alert(address);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address}, geocodeResult);
});
   

$.ajax({//
            url: base_url+"Proyecto/getTipoProyecto",
            method: 'POST',
            success: function(data) {
                $('#tipoProyecto').html(data);  
            }
            
    }); 
$.ajax({//
            url: base_url+"Proyecto/getCartera",
            method: 'POST',
            success: function(data) {
                $('#carteraProyecto').html(data);  
            }
            
    });
$.ajax({//
            url: base_url+"Proyecto/getTipoInversion",
            method: 'POST',
            success: function(data) {
                $('#tipoInversion').html(data);  
            }
            
    });

$.ajax({//
            url: base_url+"Proyecto/getUmedida",
            method: 'POST',
            success: function(data) {
                $('#unidadMedida').html(data);  
            }
            
    }); 
$.ajax({//
            url: base_url+"Proyecto/getIndicador",
            method: 'POST',
            success: function(data) {
                $('#indicadorDesglosado').html(data); 
                 $('#indicadorDesglosado').selectpicker('refresh');  //con la opcion refresh se vuelve a cargar el datepicker

            }
            
    });
$.ajax({//
            url: base_url+"Proyecto/getEjecutora",
            method: 'POST',
            success: function(data) {
                $('#entidadEjecutora').html(data); 
            }
            
    });











//fin cargar datos de select

jQuery.validator.addMethod("conRepetido", function() {
var result;
var cod = $('#numeroConvenio').val();
if($('#validador').val() == cod){//entra solo cuando se introduce un codigo diferente al que esta en la bd
    result = true;

} else{

            //var cod = $('#codactual_mueble').val();
             $.ajax({
                    url: base_url+"Proyecto/validaConv",
                    async: false,
                    method: 'POST',
                    data: {conv: cod},
                   
                    success: function(data) {
                         if(data == 1){
                         
                         result = false;
                         //$('#codactual_mueble').val('');
                         } else {
                            
                            result = true;
                         }
                         
                             
                    }            
            }); 
  }           
return result;
}, "Este número de convenio ha sido registrado !!");

//
jQuery.validator.addMethod("numero", function(value,  element) {
return this.optional(element) || /^([0-9])*$/.test(value);
}, "Debes introducir solo numeros positivos");
valido_mueble = 0;
    contador = 1;
    
    var validatorProy = $("#formProyecto").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",

        rules: {
           
            familiasBeneficiadas: {
                required: true,
                numero: true,
                minlength: 1,
                maxlength: 3
            },
            tipoProyecto: {
                required: true
            },
            
            carteraProyecto: { 
                required: true
            },
            tipoInversion: {
                required: true
                
            },

            nombreProyecto: {
                required: true,
                minlength: 5
            },
            /////

            departamento: {
                required: true
            },
            cbprovincia: {
                required: true
            },
            cbmunicipio: {
                required: true
            },
            /////////

            numeroConvenio: {
                required: true,
                minlength: 2,
                conRepetido: true
            },
            fechaConvenio: {
                required: true
            },
            plazoInicial: {
                required: true,
                numero: true,
                minlength:1
            },
            montoFinanciamiento: {
                required: true
            },


            //fechaInicConvenio: {
              //  required: true
            //},
            //subirConvenio: {
              //  required: true
            //},

            inversionFdi: {
                required: true
            },
            inversionGam: {
                required: true
            },
            entidadEjecutora: {
                required: true
            },
            ////////
            indicadorDesglosado: {
                required: true
            },
            unidadMedida: {
                required: true
            },
            metaProyecto: {
                required: true
            }
            

        },


    });
    
$('#smartwizard2').smartWizard({
        selected: 0,  
        keyNavigation:false, 
        autoAdjustHeight:true, 
        cycleSteps: false, 
        backButtonSupport: true, 
        useURLhash: true, 
        lang: {  
            next: 'Siguiente', 
            previous: 'Anterior'
        },
        toolbarSettings: {
            toolbarPosition: 'bottom', 
            toolbarButtonPosition: 'right', 
            showNextButton: true, 
            showPreviousButton: true, 
            toolbarExtraButtons: [
           $('<button></button>').text('Guardar')
                    .addClass('btn btn-info hide')
                    .on('click', function(){
                
                        
                //inicio para que valide el wisard en caso de editar, solo valida cuando se hace siguiente por eso es necesario volveer a validar desde el principio el formulario
                if($('#accion_form_mueble').text() == 'Editar'){
                    contador = 3;
                    var valid = true;
                    valid = $("#formueble").valid() && valid; //valida el formulario
                     
                     if(!valid){
                        wizard.find(".stepContainer").removeAttr("style");
                        //validatorProy.focusInvalid(); //2121
                        valido_mueble = 0;
                        
                    }else {
                        valido_mueble = 1;
                    }
                    
                }
                //fin valida en caso de editar
                        //contador++;
                        //alert(valido_mueble);
                        //alert('contador:'+contador);
                        if(contador < 3) {
                        alert("Falta introducir datos al formulario..");
                        return false;  
                        //event.preventDefault();
                        
                        }
                        if(valido_mueble == 1 && contador >= 3){
                            guardar_proyecto();
                            return false;
                        }
                console.log('sdasd');

                    }),
                ],                
        },             
        theme: 'arrows',
        transitionEffect: 'fade', 
        transitionSpeed: '400'
    });
    
    $("#smartwizard2").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
        //alert(stepNumber);
        //wizard para que vaya a siguiente wisard, empieza a validar cuando se va  al siguiente
        var wizard = $('#smartwizard2');
        //console.log(wizard);
        //alert(wizard.hasClass("myForm_mueble"));
        if(wizard.hasClass("myForm_mueble")){
            

            var valid = true;
            //con each recorremos todos los tags
             $('input,textarea,select',$(anchorObject.attr("href"))).each(function(i,v){
                valid = validatorProy.element(v) && valid; //2021 1/2 para validar
                //valid es verdadero si cumple validacion en cada iteracion
                //alert("valor de valid:"+valid);
                valido_mueble = 1;
                
                                
            });
             //si alguna condicion no se cumpke entra 
            if(!valid){
                wizard.find(".stepContainer").removeAttr("style");
                validatorProy.focusInvalid(); //2021 2/2 para validar
                valido_mueble = 0;
                
                //alert("entro por falso");
                return false;
            }
            else{
                if (stepDirection!="backward") contador++;//cuando retrocede resta
                else 
                contador--;
                if(contador >= 4) $('.btn-info').removeClass('hide');
                alert(contador);
            }
        }
        
        return valid;
    });
//
 $('.resetform').click(function() {
        $('#smartwizard2').smartWizard('reset');
        contador=1;
        validatorProy.resetForm();
        $('#formProyecto')[0].reset();
        $("select").val(['']);
        $('.form-control').removeClass('error');
        $('.form-control').removeClass('required');    
        $('.error').remove();

 });
 $('.resetform1').click(function() {
    //alert("entro");
        $('#botonPdf').remove();

 });


}

