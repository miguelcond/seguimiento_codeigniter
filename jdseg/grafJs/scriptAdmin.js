document.addEventListener("DOMContentLoaded", () => {
    // Escuchamos el click del botón
    const $boton3 = document.querySelector("#btnCrearPdfbarraAdmin");
    $boton3.addEventListener("click", () => {
        //const $elementoParaConvertir = document.body; // <-- Aquí puedes elegir cualquier elemento del DOM
        const $elementoParaConvertir = document.getElementById('graficoBarraAdmin');
        $('.x_panel').css("width","600px");
        //document.getElementById("margenTorta").style.padding = "0px 0px 0px 16px";
        $('.x_title').css({'width':'550px'});
        $('canvas').parent().css({'width':'550px'});
        //$('canvas').css({'width':'450px'});
        $('.margenTabla').css("width","550px");
        html2pdf()
            .set({
                margin: 1,
                filename: 'documento.pdf',
                image: {
                    type: 'jpeg',
                    quality: 0.98
                },
                html2canvas: {
                    scale: 3, // A mayor escala, mejores gráficos, pero más peso
                    letterRendering: true,
                },
                jsPDF: {
                    unit: "in",
                    format: "a3",
                    orientation: 'portrait' // landscape o portrait
                }
            })
            .from($elementoParaConvertir)
            .save()
            .catch(err => console.log(err));
             setTimeout(function(){
                $('.x_panel').css("width","");
                $('.x_title').css({'width':''});
                $('canvas').parent().css({'width':''});
                //$('canvas').css({'width':''});
                $('.margenTabla').css("width","");
         },1000);
    });

    const $boton4 = document.querySelector("#btnCrearPdfTortaAdmin");
    $boton4.addEventListener("click", () => {
        //const $elementoParaConvertir = document.body; // <-- Aquí puedes elegir cualquier elemento del DOM
        const $elementoParaConvertir = document.getElementById('graficoTortaAdmin');
        $('.x_panel').css("width","600px");
        //document.getElementById("margenTorta").style.padding = "0px 0px 0px 16px";
        $('.x_title').css({'width':'550px'});
        $('canvas').parent().css({'width':'550px'});
        //$('canvas').css({'width':'450px'});
        $('.margenTabla').css("width","550px");
        html2pdf()
            .set({
                margin: 1,
                filename: 'documento.pdf',
                image: {
                    type: 'jpeg',
                    quality: 0.98
                },
                html2canvas: {
                    scale: 3, // A mayor escala, mejores gráficos, pero más peso
                    letterRendering: true,
                },
                jsPDF: {
                    unit: "in",
                    format: "a3",
                    orientation: 'portrait' // landscape o portrait
                }
            })
            .from($elementoParaConvertir)
            .save()
            .catch(err => console.log(err));
             setTimeout(function(){
                $('.x_panel').css("width","");
                $('.x_title').css({'width':''});
                $('canvas').parent().css({'width':''});
                //$('canvas').css({'width':''});
                $('.margenTabla').css("width","");
         },1000);
    });






}); 