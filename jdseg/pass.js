//inicio modificar pass
$("#modificarPass").click(function() {//tico
     $('#modPass').modal('show');
     
   });
function mostrarPassword(){
          var cambio = document.getElementById("contrasena");
          if(cambio.type == "password"){
               cambio.type = "text";
               $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
          }else{
               cambio.type = "password";
               $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
          }
     }
function mostrarPassword1(){
          var cambio = document.getElementById("contrasenaNueva");
          if(cambio.type == "password"){
               cambio.type = "text";
               $('.icon1').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
          }else{
               cambio.type = "password";
               $('.icon1').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
          }
     }
function mostrarPassword2(){
          var cambio = document.getElementById("contrasenaConfirm");
          if(cambio.type == "password"){
               cambio.type = "text";
               $('.icon2').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
          }else{
               cambio.type = "password";
               $('.icon2').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
          }
     }
$('#guardarPass').on('click',function()
      {
       valor=$('#contrasena').val();
       $("#formPass").submit();  
    });
//inicio validar form
jQuery.validator.addMethod("passRepetido", function() {
var result='';
     var pass = $('#contrasena').val();
     var urlBase = enlace + "Cambio/verificaPass";
                   $.ajax({
                          url: urlBase,
                          method: 'GET',
                          async: false,
                          data: {passsactual:pass},
                         
                          success: function(data) {
                            //alert(data);
                               if(data == 0){
                               
                               result = false;
                               
                               } else {
                                  
                                  result = true;
                               }
                         }            
                  }); 

return result;
}, "no coincide con la actual contraseña !!");
jQuery.validator.addMethod("passConfirm", function() {
var result='';
     var passnew = $('#contrasenaNueva').val();
     var passnewConf = $('#contrasenaConfirm').val();
     if (passnew == passnewConf) result = true;
          else result = false;

return result;
}, "las contraseña nueva no coincide con la de confirmación !!");
var validatorUPass = $("#formPass").validate({   /* variable validatorUs solo para resetear el form de las alertas de validacion*/
        ignore: "input[type=hidden], .select2-search__field",
        errorClass: "validation-error-label",
        successClass: "validation-valid-label",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },        
        rules: {
            contrasena: {
                required: true,
                minlength: 5,
                passRepetido:true
            },
            contrasenaNueva: {
                required: true,
                minlength: 5
             },
            contrasenaConfirm: {
                required: true,
                minlength: 5,
                passConfirm: true
             }
         },
        submitHandler: function() {
            enviardatosPass();           
        }


    });

function enviardatosPass()
{
 alert("entro"+enlace);

 //if($('#accion_form_usuario').text() == 'Registrar'){
      var urlBase = enlace + "Cambio/modifPAss";
      var datos = $('#formPass').serialize(); 
        $.ajax({
            type: "GET",
            url: urlBase,
            data: datos, 
            success: function(data)  
             {
                
                  
                if (data == 1)
                { 
                swal("Correcto", "Contraseña modificada con exito.", "success")
                    .then((value) => {
                        location.reload();
                    });    
                }
                else
                {
                  swal("Error", "Porfavor, intente nuevamente.", "error")
                    .then((value) => {
                        location.reload();
                    });
                }
             }
        });

//}

}
$('.resetformPass').click(function() { //limpiar el formulario (2)
        
        validatorUPass.resetForm();
        $('#formPass')[0].reset();
        $('.form-control').removeClass('error');
        $('.form-control').removeClass('required');    
        $('.error').remove();
 });        
//fin modificar pass